
#pragma once

#include "engine_base.h"

#include "cFx.h"

/*
加载静态X文件
*/

// 不支持动画
class cStaticModel
{
public:
	cStaticModel();
	~cStaticModel();

	// 加载前要调用该函数
	void	SetDevice(IDirect3DDevice9*	device);

	void	LoadXFile(char* x_file_name);

	// 用于T&L渲染。
	void	Render();
	ID3DXMesh*	getMesh();

	bool	isVisable();
	void	setVisable(bool	t);

	// 暂时只起存储矩阵作用
	void	setMatrix(D3DXMATRIX t);
	D3DXMATRIX*	getMatrix();
		
	void	Shutdown();

//--适应shader
	void	shader_draw(D3DLIGHT9* pLight, cFx* pFx);

private:
	IDirect3DDevice9* _device;
	IDirect3DTexture9** _tex; // 纹理指针数组
	D3DMATERIAL9*	_material;
	DWORD		_numMaterial;
	ID3DXMesh*	_mesh;

	bool	_isVisable;
	D3DXMATRIX	 _modulate; // 暂存矩阵，（思想是和对象绑定）
};


// 本来想重写该系统的，用纹理链以及别的。
// 但是，陡感时间急迫，压力巨大。
// 还是以大局考虑。细节以后再说
