#ifndef	_RENDER_
#define _RENDER_

#include "engine_base.h"
#include "engine_fvf.h"
#include "engine_GUI.h"
#include "engine_color.h"

//----------------------------------
// Primitive types.
enum PrimType
{
	NULL_TYPE,
	POINT_LIST,
	TRIANGLE_LIST,
	TRIANGLE_STRIP,
	TRIANGLE_FAN,
	LINE_LIST,
	LINE_STRIP
};

struct sD3DStaticBuffer
{
	sD3DStaticBuffer() : vbPtr(0), ibPtr(0), numVerts(0),
		numIndices(0), stride(0), fvf(0),
		primType(NULL_TYPE) {}
	
	LPDIRECT3DVERTEXBUFFER9 vbPtr; 
	LPDIRECT3DINDEXBUFFER9 ibPtr;
	UINT numVerts;
	UINT numFaces;
	UINT numIndices;
	UINT stride;  // 每顶点字节数
	unsigned long fvf;
	PrimType primType;
};

//-------------------------------------
struct sMaterial
{
	sMaterial(){}

	sMaterial(D3DXCOLOR _a, D3DXCOLOR _d, D3DXCOLOR _s, D3DXCOLOR _e, float _p)
	{
		a=_a;
		d=_d;
		s=_s;
		e=_e;
		p=_p;
	}

	D3DXCOLOR a; //ambient
	D3DXCOLOR d; //diffuse
	D3DXCOLOR s; //specular
	D3DXCOLOR e; //emissive
	float p;
};

const sMaterial WHITE_MATERIAL(WHITE, WHITE, WHITE, BLACK, 2);
const sMaterial BLACK_MATERIAL(BLACK, BLACK, BLACK, BLACK, 0);

// define lights
#define LIGHT_POINT        1
#define LIGHT_DIRECTIONAL  2
#define LIGHT_SPOT         3

struct sLight
{
	sLight()
	{

	}
	D3DXVECTOR3	pos;
	D3DXVECTOR3 dir;
	D3DXCOLOR	ambient_color;
	D3DXCOLOR   diffuse_color;
	D3DXCOLOR	specular_color;
	int type;
};

struct sTexture
{
	TCHAR*	name;
	int		width;
	int		height;
	IDirect3DTexture9*	image;
};

// Texture states.
enum TextureFilter
{
	MIN_FILTER,
	MAG_FILTER,
	MIP_FILTER
};

// Texture filtering.
enum FilterType
{
	POINT_TYPE,
	LINEAR_TYPE,
	ANISOTROPIC_TYPE
};



//  
//  
class cRender
{
public:
	cRender();
	~cRender();

	// D9设备系列
	IDirect3DDevice9* init_d3d9_device(HWND window_handle, int width, int height, BOOL windowed =TRUE);

	// 渲染系列
	void setClearColor(D3DCOLOR	temp);
	void startRender(bool bColor, bool bDepth, bool bStencil);
	void clearBuff(bool bColor, bool bDepth, bool bStencil);
	void endRender();
	void init_render_state();
	void drawGeometry(sD3DStaticBuffer&	 buffer_dsc);

	// 材质系列
	void setMaterial(const sMaterial& m);
	void disableMaterial();

	// 灯光系列
	void setLight(sLight& l, int index);
	void disableLight();

	// 纹理系列
	void addTexture(TCHAR* name, int& texID);
	void setTextureFilter(int index, int filter, int type);
	void setMultiTexture();
	void setDetailMapping();
	void applyTexture(int index, int texID);
	void saveScreenShot();
	IDirect3DTexture9* getTexture(int TexID);

	// 返回结构体-_-，好吧。。人家是补丁啦。。能用就行
	D3DSURFACE_DESC getTextureInfo(int TexID);
	
	int addTexture(TCHAR* Name); // Update!

	// 字体系列
	void createFont(TCHAR* name, int width, int height, int& ID, BOOL italic =FALSE);
	void displayText(int id, long x, long y, D3DCOLOR color, TCHAR* text,...);


	// 矩阵系列
	void setWorldMatrix(D3DXMATRIX* temp);
	void setViewMatrix(D3DXMATRIX* temp);
	void calculateProjMatrix(float fov, float n, float f);

	// 释放系列
	void ReleaseTex(int TexID);
	void ReleaseFont(int FontID);
	void ReleaseAllTex();
	void ReleaseAllFont();
	void shutDownALLXmodels();

	void shutDown();
	
// 增强
//	void debug_text(int FontID, char* Message, ...);

private:
	void generate_matrial(D3DMATERIAL9& mtrl, const sMaterial& m);

	void generate_direction_light(D3DLIGHT9& dir_light, D3DVECTOR&	dir, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

	void generate_point_light(D3DLIGHT9& point_light, D3DVECTOR& pos, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

	void generate_spot_ligth(D3DLIGHT9&	spot_light, D3DVECTOR&	pos, D3DVECTOR& dir, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

private:
	D3DCOLOR			m_clearColor;
	HWND				m_handle;
	BOOL				m_windowed;
	bool				m_rendering;
	int					m_windowWidth;
	int					m_windowHeight;
	IDirect3DDevice9*	m_pDevice;

	sTexture*			m_pTexList;
	int					m_totalTextures;

	LPD3DXFONT*			m_ppFont;
	int					m_totalFonts;


	D3DXMATRIX			m_projection;
};


bool	createRender(cRender** _render);

#endif


