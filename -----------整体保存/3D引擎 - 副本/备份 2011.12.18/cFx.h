#pragma once
#include "engine_base.h"

class cFx
{
public:
	cFx(IDirect3DDevice9* pDevice)
	{
		_pDevice =pDevice;
	}
	~cFx()
	{
		SAFE_RELEASE(_pEffect);
	}

	void load_fx(char* FileName)
	{
		// 加载编译SHADE 
		int result =0;
		ID3DXBuffer*	pErrorBuff =0;

		result = D3DXCreateEffectFromFile(_pDevice, FileName, NULL, NULL, \
			D3DXSHADER_DEBUG, NULL, &_pEffect, &pErrorBuff);
		Failed_Msg_Box("D3DXCreateEffectFromFile", result);

		if( pErrorBuff )
		{
			MessageBox(0, (char*)pErrorBuff->GetBufferPointer(), 0, 0);
			exit(0);
		}

		SAFE_RELEASE(pErrorBuff);
	}

	void apply(char* TechniqueName, void (*fuc)())
	{
		D3DXHANDLE hTechnique = _pEffect->GetTechniqueByName( TechniqueName );
		_pEffect->SetTechnique( hTechnique );

		UINT nPasses;
		UINT iPass;
		
		_pEffect->Begin( &nPasses, 0 );
			for( iPass = 0; iPass < nPasses; iPass ++ )
			{
				_pEffect->BeginPass( iPass );
				fuc();
				_pEffect->EndPass();
			}
		_pEffect->End();
	}


	ID3DXEffect* get_effect(){
		return _pEffect;
	}

	void modify_matrix(char* Name, D3DXMATRIX& t)
	{// 由于VS设置不是用总接口设置的保持一致，就写了该函数
		_pEffect->SetMatrix(Name, &t);
	}

	

protected:
	IDirect3DDevice9* _pDevice;
	ID3DXEffect* _pEffect;
};