
//用D9控制板设置的话，这里d3dx9d.lib和d3dx9.lib没什么输出区别呀。。
#ifdef _DEBUG 
#define D3D_DEBUG_INFO	
#pragma comment(lib,"d3dx9.lib")
//#pragma comment(lib,"d3dx9d.lib")

#else
#pragma comment(lib,"d3dx9.lib")

#endif

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dsound.lib")
#pragma comment(lib,"dxguid.lib") 

#pragma comment(lib, "dxerr9.lib")

// #pragma comment(lib,"d3dx9dt.lib")
// #pragma comment(lib,"d3dxof.lib")
// #pragma comment(lib,"comctl32.lib ")

#ifndef	my
#define my

#include "engine_fvf.h"
#include "engine_color.h"
#include "engine_terrain.h"
#include "engine_wav.h"
#include "bound.h"
#include "engine_load_x.h"
#include "engine_billBoard.h"
#include "engine_gui.h"
#include "window_main.h"


// for input
#include "input_check.h"

#include "camera.h"

#include "NoxRender.h"


#include "cMatrixProctect.h"
#include "cRenderStateProctect.h"

#include "system_log.h"

#include "cHighTimer.h"

//基本地板
#include "cGround.h"

//基本顶点缓存,索引缓存
#include "cVertexBuffer.h"
#include "cIndexBuffer.h"

// 读取并编译特效
#include "cFx.h"

// 顶点格式声明
#include "cVertexDecl.h"


// 地形
#include "cTerrain.h"

//
//---------------------------------
// 数据区
//


//
//---------------------------------
// 数据结构区
//


//
//-----------------------------------------------------
//函数区




#endif