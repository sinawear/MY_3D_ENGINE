// GetRandFloat 工作正常。可生成正负数

#include <iostream>
using namespace std;
#include "system_log.h"
#include "random_factory.h"

cSystemLog Log;
int main()
{
	my_srand();

	char Buff[80];

	Log.clearLog("1.txt");
	for(int i =0; i<100; i++)
	{
		sprintf(Buff, "%f %f\n", GetRandomFloat(-5, 5), GetRandomFloat(-5, 5));
		Log.appendLog("1.txt", Buff);
	}
}