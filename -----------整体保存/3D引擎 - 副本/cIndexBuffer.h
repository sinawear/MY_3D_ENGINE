#pragma once
#include "engine_base.h"

class cIndexBuffer
{
public:
	cIndexBuffer(IDirect3DDevice9* pDevice)
	{
		_pDevice =pDevice;
	}

	// 注意Length的步长，和Format匹配。
	HRESULT create_index_buffer(DWORD Length, D3DFORMAT Format =D3DFMT_INDEX16,
		DWORD Useage =D3DUSAGE_WRITEONLY, D3DPOOL Pool =D3DPOOL_MANAGED)
	{
		return _pDevice->CreateIndexBuffer(Length, Useage, Format, Pool,
			&_pIndexBuffer, NULL);  //最后参数ShareHandle有什么用
	}

	void lock(void** pI, DWORD Flags =0)
	{
		_pIndexBuffer->Lock(0, 0, pI, Flags);
	}

	void unlock()
	{
		_pIndexBuffer->Unlock();
	}

	IDirect3DIndexBuffer9* get_index_buffer()
	{
		return _pIndexBuffer;
	}

protected:
	IDirect3DDevice9* _pDevice;

	IDirect3DIndexBuffer9* _pIndexBuffer;
};