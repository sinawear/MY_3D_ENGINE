
#if _MSC_VER <= 1200
// VC6专用,让for在另一个作用域中。
	#define for if(false){}else for
#endif


#define DIRECTINPUT_VERSION	0x0800


#include <windows.h>
#include <windowsx.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dX9math.h>
#include <d3dx9core.h>
#include <d3dx9mesh.h>

#include <dxerr9.h>

#include<dmusicc.h>
#include<dmusici.h>
#include<cguid.h>


#include <dinput.h>
#include <dsound.h>

#include "MAC_CHANGE.h" //模仿微软ANSI和UNICODE函数的转换
#include "comm_base.h"
#include "comm_macro.h"
#include "comm_Error.h"




#include "utility_F.inl"
#include "DX9_ERROR.INL"
#include "D9_macro.INL"

#include "D9_comm.h"



// 这两者都会引起重定义。
//const string NoSay("I don't want to say sth.");
//cosnt char* NoSay ="I don't want to say sth.";

#define NoSay "I don't wanna to say."


//................GLOBAL.OBJECT..........................
//用于调试
#include "system_log.h"


// 全局对象
class cCameraUVNV1;
extern cCameraUVNV1 g_Camera;
//...........................................