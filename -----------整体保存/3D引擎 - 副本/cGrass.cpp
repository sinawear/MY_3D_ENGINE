#include "cGrass.h"
#include "random_factory.h"
#include "cTerrainV1.h"


const int NUM_GRASS_BLOCK =400;
IDirect3DVertexDeclaration9* cGrass::GrassVertex::Decl =0;


D3DVERTEXELEMENT9 GrassVertexElements[] = 
{
	{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
	{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
	{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
	{0, 32, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 2},
	{0, 36, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
	D3DDECL_END()
};	


cGrass::cGrass(IDirect3DDevice9* pDevice)
{
	_pDevice =pDevice;
	_pDevice->CreateVertexDeclaration(GrassVertexElements, &GrassVertex::Decl);
}

void cGrass::init(cTerrainV1* pTerrain, char* TexName)
{

	// 得到顶点声明
	D3DVERTEXELEMENT9 TempVertexDecl[MAX_FVF_DECL_SIZE];
	UINT NumElems =0;
	GrassVertex::Decl->GetDeclaration(&TempVertexDecl[0], &NumElems);

/*实验用FVF。。失败。开来要翻译D9文档了。
#define GRASS_FVF D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX0 | D3DFVF_PSIZE |D3DFVF_DIFFUSE
	D3DXDeclaratorFromFVF(GRASS_FVF, TempVertexDecl);
*/	
	// 创建网格
	// 一个BLOCK就是一个正方形
	D3DXCreateMesh(NUM_GRASS_BLOCK*2, NUM_GRASS_BLOCK*4, D3DXMESH_MANAGED,
		&TempVertexDecl[0], _pDevice, &_pGrassMesh);

	// 填充网格的顶点和索引
	GrassVertex* pV;
	WORD* pI;

	_pGrassMesh->LockVertexBuffer(NULL, (void**)&pV);
	_pGrassMesh->LockIndexBuffer(NULL, (void**)&pI);

	// 处理索引偏移的变量
	int OffsetIndex =0;

	// 缩小我们要生成草地形区域的面积
	int w =g_Terrain.width * 0.15f;
	int d =g_Terrain.depth * 0.15f;

	// 以块为单位
	for(int i =0; i<NUM_GRASS_BLOCK; i++)
	{
		// 生成区域内的随机位置
		float x =GetRandomInt(-w/2, w/2);
		float z =GetRandomInt(-d/2, d/2);

		float y =pTerrain->getY(x,z);

		if(y < 37.0f || y > 40.0f)
		{
			--i; // We are trying again, so decrement back the index.
			continue;
		}

		// 得出位置和缩放
		float sx = GetRandomFloat(0.75f, 1.25f); 
		float sy = GetRandomFloat(0.75f, 1.25f);
		float sz = GetRandomFloat(0.75f, 1.25f);
		D3DXVECTOR3 pos(x, y, z);
		D3DXVECTOR3 scale(sx, sy, sz);

	
		_build(pV, pI, OffsetIndex, pos, scale);

		pV +=4;
		pI +=6;
	}

	_pGrassMesh->UnlockIndexBuffer();
	_pGrassMesh->UnlockVertexBuffer();



	// 创建属性缓存
	DWORD* attributeBufferPtr = 0;
	_pGrassMesh->LockAttributeBuffer(0, &attributeBufferPtr);
	for(UINT i = 0; i < _pGrassMesh->GetNumFaces(); ++i)
		attributeBufferPtr[i] = 0;
	_pGrassMesh->UnlockAttributeBuffer();

	// 创建邻接信息并优化
	DWORD* adj = new DWORD[_pGrassMesh->GetNumFaces()*3];
	_pGrassMesh->GenerateAdjacency(0.001f, adj);
	_pGrassMesh->OptimizeInplace(D3DXMESHOPT_ATTRSORT|D3DXMESHOPT_VERTEXCACHE,
		adj, 0, 0, 0);
	
	delete [] adj;	


//////////////////////////////////////////////////////////////////////////
	// 创建特效
	_pGrassFx =new cFx(_pDevice);
	_pGrassFx->load_fx("grass.fx");

	//加载纹理
	D3DXCreateTextureFromFile(_pDevice, TexName, &_pGrassTex);

}

void cGrass::_build(GrassVertex* v, WORD* k, int& indexOffset, 
				   D3DXVECTOR3& worldPos, D3DXVECTOR3& scale)
{
	// Only top vertices have non-zero amplitudes: 
	// The bottom vertices are fixed to the ground.
	float amp = GetRandomFloat(0.5f, 1.0f);
	v[0] = GrassVertex(D3DXVECTOR3(-1.0f,-0.5f, 0.0f), D3DXVECTOR2(0.0f, 1.0f), 0.0f);
	v[1] = GrassVertex(D3DXVECTOR3(-1.0f, 0.5f, 0.0f), D3DXVECTOR2(0.0f, 0.0f), amp);
	v[2] = GrassVertex(D3DXVECTOR3( 1.0f, 0.5f, 0.0f), D3DXVECTOR2(1.0f, 0.0f), amp);
	v[3] = GrassVertex(D3DXVECTOR3( 1.0f,-0.5f, 0.0f), D3DXVECTOR2(1.0f, 1.0f), 0.0f);
	
	// 这种写法，又敢大，又漂亮
	// Set indices of fin.
	k[0] = 0 + indexOffset;
	k[1] = 1 + indexOffset;
	k[2] = 2 + indexOffset;
	k[3] = 0 + indexOffset;
	k[4] = 2 + indexOffset;
	k[5] = 3 + indexOffset;
	
	// Offset the indices by four to have the indices index into
	// the next four elements of the vertex buffer for the next fin.
	indexOffset += 4;
	
	// Scale the fins and randomize green color intensity.
	for(int i = 0; i < 4; ++i)
	{
		v[i].pos.x *= scale.x;
		v[i].pos.y *= scale.y;
		v[i].pos.z *= scale.z;
		
		// Generate random offset color (mostly green).
		v[i].colorOffset = D3DXCOLOR(
			GetRandomFloat(0.0f, 0.1f),
			GetRandomFloat(0.0f, 0.2f),
			GetRandomFloat(0.0f, 0.1f),
			0.0f);
	}
	
	// 在底部添加一些偏移到地面，另外，中心点将接触地面，
	// 只显示一半。
	// Add offset so that the bottom of fin touches the ground
	// when placed on terrain.  Otherwise, the fin's center point
	// will touch the ground and only half of the fin will show.
	float heightOver2 = (v[1].pos.y - v[0].pos.y) / 2;
	worldPos.y += heightOver2;
	
	// Set world center position for the quad.
	v[0].quadPos = worldPos;
	v[1].quadPos = worldPos;
	v[2].quadPos = worldPos;
	v[3].quadPos = worldPos;
}

// uniform extern float4x4 gViewProj;
// uniform extern texture  gTex;
// uniform extern float    gTime;
// uniform extern float3   gEyePosW;
void cGrass::draw(D3DXMATRIX& view, D3DXMATRIX& proj, float Time,
				  D3DXVECTOR3 EyePos)
{
	_pGrassFx->modify_matrix("gViewProj", view*proj);
	_pGrassFx->modify_float("gTime", Time);
	_pGrassFx->set_float_array("gEyePosW", EyePos, 3);
	_pGrassFx->set_texture("gTex", _pGrassTex);

	// 看来cFx只有用宏，才能解决问题。
	ID3DXEffect* pEffect =_pGrassFx->get_effect();

	D3DXHANDLE hTechnique = pEffect->GetTechniqueByName( "GrassTech" );
	pEffect->SetTechnique( hTechnique );
	
	UINT nPasses;
	UINT iPass;
	
	pEffect->Begin( &nPasses, 0 );
	for( iPass = 0; iPass < nPasses; iPass ++ )
	{
		pEffect->BeginPass( iPass );
			_paint();
		pEffect->EndPass();
	}
	pEffect->End();
}

void cGrass::_paint()
{
	_pGrassMesh->DrawSubset(0);
}