
/*
δͨ�����ַ�����.
*/

#pragma	once

#include "engine_base.h"


class cSystemLog
{
public:
	void	clearLog(char*	fileName);
	void	writeLog(char*	fileName, char* message,...);
	void	appendLog(char* fileName, char* message,...);
	
	void	writeMatrix(char* fileName, D3DXMATRIX&	t);
	void	writeVector(char* fileName, D3DXVECTOR3&	t);

private:
	void	write(char* message, va_list start);

	ofstream	m_file;
};


