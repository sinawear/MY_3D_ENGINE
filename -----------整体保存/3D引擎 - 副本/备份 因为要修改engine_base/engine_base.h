#define VC6_COMPILER

#ifdef		VC6_COMPILER
	// VC6专用,让for在另一个作用域中。
	#define for if(false){}else for

#endif

//#ifndef _ENGINE_BASE_
//#define _ENGINE_BASE_


//#define INITGUID	VC6下不注释掉将导致大量的重定义类接口ID；VC2005下却不会
#define DIRECTINPUT_VERSION	0x0800


#include <windows.h>
#include <windowsx.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dX9math.h>
#include <d3dx9core.h>
#include <d3dx9mesh.h>

#include <dxerr9.h>

#include<dmusicc.h>
#include<dmusici.h>
#include<cguid.h>

#include <math.h>
#include <tchar.h>
#include <memory.h>
#include <time.h>
#include <assert.h>

#include <stdio.h>


	#include <iostream>
	#include <fstream>
	#include <list>
	using namespace std;
	using std::ifstream;
	using std::ofstream;
	using std::wifstream;
	using std::wofstream;



#include <dinput.h>
#include <dsound.h>



//-------------------------------------------------------
// 宏区

#define SAFE_RELEASE(p)			if(p) { (p)->Release(); (p)=NULL; } 
#define SAFE_DELETE(p)			if(p) { delete(p); p=NULL;}
#define SAFE_DELETE_ARRAY(p)	if(p) { delete[] (p); p=NULL;}

//百分比宏(percentage)
#define	PERCENTAGE(v, p)	(int)v*p


//////////////////////////////////////////////////////////////////////////
//




//#endif