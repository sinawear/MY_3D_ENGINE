

#include "Render.h"

cRender::cRender()
{
	m_pDevice =0;

	m_totalTextures =0;
	m_totalFonts =0;
	m_totalModels =0;

	m_clearColor =0;

//	m_pGUIList =0;
	m_pTexList =0;
	m_ppFont =0;
	m_pModel =0;

}

cRender::~cRender()
{

}

IDirect3DDevice9*	cRender::init_d3d9_device(HWND window_handle, int width, int height, BOOL windowed)
{
	m_handle =window_handle;
	m_windowed =windowed;
	m_windowWidth =width;
	m_windowHeight =height;

	D3DCAPS9	caps;
	D3DPRESENT_PARAMETERS	d3dParam;
	int vp =0, result;

	//得到D3D接口
	IDirect3D9*	d3d9 = Direct3DCreate9(D3D_SDK_VERSION);

	//得到设备状态.
	d3d9->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps);

	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
	{
		vp = D3DCREATE_HARDWARE_VERTEXPROCESSING; 
	}
	else 
	{
		MessageBox(NULL,_T("程序不支持软件运行"),_T("!"),0);
	}

	ZeroMemory(&d3dParam,sizeof(d3dParam));

	d3dParam.Windowed = windowed;
	d3dParam.hDeviceWindow = window_handle;
	d3dParam.BackBufferCount = 1;
	d3dParam.BackBufferFormat = D3DFMT_A8R8G8B8;
	d3dParam.EnableAutoDepthStencil = TRUE;
	d3dParam.Flags = 0;
	d3dParam.BackBufferWidth = width;
	d3dParam.BackBufferHeight = height;
	d3dParam.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dParam.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3dParam.MultiSampleQuality = 0;
	d3dParam.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dParam.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	d3dParam.AutoDepthStencilFormat = D3DFMT_D24S8;

	//得到设备接口.
	result = d3d9->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,window_handle, \
		vp, &d3dParam, &m_pDevice);

	//realse d3d9  
	SAFE_RELEASE(d3d9);

//	init_render_state();
	
	return m_pDevice;
}

void	cRender::init_render_state()
{
	m_pDevice->SetRenderState(D3DRS_ZENABLE, TRUE); //开启深度缓存
	m_pDevice->SetRenderState(D3DRS_LIGHTING , false);//关闭灯光
	m_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); //关闭背面消隐
	
	// 纹理过滤器
	m_pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
	m_pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
	m_pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC);
}

void	cRender::setClearColor(D3DCOLOR temp)
{
	m_clearColor =temp;
}

void	cRender::startRender(bool bColor, bool bDepth, bool bStencil)
{
	if(!m_pDevice) return;

	unsigned int buffers = 0;
	if(bColor) buffers |= D3DCLEAR_TARGET;
	if(bDepth) buffers |= D3DCLEAR_ZBUFFER;
	if(bStencil) buffers |= D3DCLEAR_STENCIL;

	m_pDevice->Clear(NULL, NULL, buffers, m_clearColor, 1, 0);

	m_pDevice->BeginScene();

	m_rendering =true;
}

void	cRender::clearBuff(bool bColor, bool bDepth, bool bStencil)
{
	if(m_pDevice) return;

	unsigned int buffers = 0;
	if(bColor) buffers |= D3DCLEAR_TARGET;
	if(bDepth) buffers |= D3DCLEAR_ZBUFFER;
	if(bStencil) buffers |= D3DCLEAR_STENCIL;

	m_pDevice->Clear(NULL, NULL, buffers, m_clearColor, 1, 0);

	m_pDevice->BeginScene();
}

void	cRender::endRender()
{
	if(!m_pDevice) return;

	m_pDevice->EndScene();
	m_pDevice->Present(NULL, NULL, NULL, NULL);

	m_rendering =false;
}

void	cRender::setMaterial(sMaterial& m)
{
	D3DMATERIAL9 mtrl;
	generate_matrial(mtrl ,m);
	m_pDevice->SetMaterial(&mtrl);
}

void	cRender::setLight(sLight& l, int index)
{
	D3DLIGHT9			light;

	if(l.type == LIGHT_POINT)
		generate_point_light(light, l.pos, l.ambient_color, l.diffuse_color, l.specular_color);

	if(l.type == LIGHT_DIRECTIONAL)
		generate_direction_light(light, l.dir, l.ambient_color, l.diffuse_color, l.specular_color);

	if(l.type == LIGHT_SPOT)
		generate_spot_ligth(light, l.pos, l.dir, l.ambient_color, l.diffuse_color, l.specular_color);

	
	m_pDevice->SetLight(index, &light);
	m_pDevice->LightEnable(index, TRUE);
	m_pDevice->SetRenderState( D3DRS_LIGHTING, TRUE );			/// 打开光源设置

    m_pDevice->SetRenderState( D3DRS_AMBIENT, 0x00909090 );
}


void	cRender::disableLight(int index)
{
	m_pDevice->LightEnable(index, FALSE);
}

void	cRender::addTexture(TCHAR* name, int& texID)
{
	if(!name || !m_pDevice) return;

	int index = m_totalTextures;

	if(!m_pTexList)
		m_pTexList =new sTexture[1];
	else
	{
		sTexture *temp;
		temp = new sTexture[m_totalTextures + 1];

		memcpy(temp, m_pTexList, sizeof(sTexture) * m_totalTextures);

		delete[] m_pTexList;
		m_pTexList = temp;
	}

	int length =_tcslen(name) +1;
	m_pTexList->name =new TCHAR[length];

	_tcscpy(m_pTexList->name, name);

	D3DCOLOR colorkey = 0xff000000;
	D3DXIMAGE_INFO info;

	D3DXCreateTextureFromFileEx(m_pDevice, name, 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT,
		D3DX_DEFAULT,  colorkey, &info, NULL,
		&m_pTexList->image);

	m_pTexList[index].image->SetAutoGenFilterType(D3DTEXF_LINEAR);
	m_pTexList[index].image->GenerateMipSubLevels();

	m_pTexList[index].width = info.Width;
	m_pTexList[index].height = info.Height;

	texID =index;
	m_totalTextures++;
}

void	cRender::setTextureFilter(int index, int filter, int type)
{
	D3DSAMPLERSTATETYPE fil = D3DSAMP_MINFILTER;
	int v = D3DTEXF_LINEAR;

	if(filter == MIN_FILTER) fil = D3DSAMP_MINFILTER;
	if(filter == MAG_FILTER) fil = D3DSAMP_MAGFILTER;
	if(filter == MIP_FILTER) fil = D3DSAMP_MIPFILTER;

	if(type == POINT_TYPE) v = D3DTEXF_POINT;
	if(type == LINEAR_TYPE) v = D3DTEXF_LINEAR;
	if(type == ANISOTROPIC_TYPE) v = D3DTEXF_ANISOTROPIC;

	m_pDevice->SetSamplerState(index, fil, v);
}

void	cRender::setMultiTexture()
{
	m_pDevice->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
	m_pDevice->SetTextureStageState(0, D3DTSS_COLOROP,
		D3DTOP_MODULATE);
	m_pDevice->SetTextureStageState(0, D3DTSS_COLORARG1,
		D3DTA_TEXTURE);
	m_pDevice->SetTextureStageState(0, D3DTSS_COLORARG2,
		D3DTA_DIFFUSE);

	m_pDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1);
	m_pDevice->SetTextureStageState(1, D3DTSS_COLOROP,
		D3DTOP_MODULATE);
	m_pDevice->SetTextureStageState(1, D3DTSS_COLORARG1,
		D3DTA_TEXTURE);
	m_pDevice->SetTextureStageState(1, D3DTSS_COLORARG2,
		D3DTA_CURRENT);

}

void	cRender::setDetailMapping()
{
	m_pDevice->SetTextureStageState(0, D3DTSS_TEXCOORDINDEX, 0);
	m_pDevice->SetTextureStageState(0, D3DTSS_COLOROP,
		D3DTOP_SELECTARG1);
	m_pDevice->SetTextureStageState(0, D3DTSS_COLORARG1,
		D3DTA_TEXTURE);
	m_pDevice->SetTextureStageState(0, D3DTSS_COLORARG2,
		D3DTA_DIFFUSE);

	m_pDevice->SetTextureStageState(1, D3DTSS_TEXCOORDINDEX, 1);
	m_pDevice->SetTextureStageState(1, D3DTSS_COLOROP,
		D3DTOP_ADDSIGNED);
	m_pDevice->SetTextureStageState(1, D3DTSS_COLORARG1,
		D3DTA_TEXTURE);
	m_pDevice->SetTextureStageState(1, D3DTSS_COLORARG2,
		D3DTA_CURRENT);
}

void	cRender::applyTexture(int index, int texID)
{
	m_pDevice->SetTexture(index, &m_pTexList->image[texID]);
}

void	cRender::saveScreenShot()
{
	D3DDISPLAYMODE			display;
	IDirect3DSurface9*		surface;
	static	int  i =0;
	TCHAR	buff[20];
	SYSTEMTIME systemTime;

	// 得到当前时间
	GetLocalTime(&systemTime);
	_stprintf(buff, _T("%d-%d-%d_%d.bmp"), systemTime.wYear,
		systemTime.wMonth, systemTime.wDay,	++i);

	// 得到窗口信息
	m_pDevice->GetDisplayMode(NULL, &display);
	// 创建离屏表面
	m_pDevice->CreateOffscreenPlainSurface(display.Width, display.Height,
		display.Format, D3DPOOL_DEFAULT, &surface, NULL);
	// 得到后备缓冲的内容放到离屏表面上.
	m_pDevice->GetBackBuffer(NULL, NULL, D3DBACKBUFFER_TYPE_MONO, &surface);
	// 保存到磁盘
	D3DXSaveSurfaceToFile(buff, D3DXIFF_BMP, surface, NULL, NULL);

	SAFE_RELEASE(surface);
}

void	cRender::createFont(TCHAR* name, int width, int height, int& ID, BOOL italic /*=FALSE */)
{
	if(!name)	return ;
	if(!m_ppFont)
	{// 初始化
		m_ppFont =new LPD3DXFONT[1];
	}
	else
	{	
		LPD3DXFONT*	temp;
		temp =new LPD3DXFONT[m_totalFonts + 1];
		memcpy(temp, m_ppFont, sizeof(LPD3DXFONT)*m_totalFonts);
		delete[] m_ppFont;
		m_ppFont =temp;

	}
	// 创建字体
	D3DXFONT_DESC	font_desc;

	ZeroMemory(&font_desc,sizeof(font_desc));
	font_desc.Height =25;
	font_desc.Width =12;
	font_desc.Italic =FALSE;
	_tcscpy(font_desc.FaceName,name);

	D3DXCreateFontIndirect(m_pDevice, &font_desc, &m_ppFont[m_totalFonts]);

	ID =m_totalFonts;
	m_totalFonts++;
}

void	cRender::displayText(int id, long x, long y, D3DCOLOR color, TCHAR* text,...)
{
	if(id > m_totalFonts)
		return ;

	TCHAR	buff[1024];
	va_list	argList;

	va_start(argList, text);
	_vstprintf(buff, text, argList);
	va_end(argList);

	RECT	rect ={x, y, m_windowWidth, m_windowHeight};
	m_ppFont[id]->DrawText(NULL, buff, _tcslen(buff), &rect, DT_LEFT, color);
}

void	cRender::loadXModel(TCHAR* file, int& xModelId)
{
	if(!file) return;

	CHAR	buff[1024];

	if(!m_pModel)
	{
		m_pModel =new CXModel[1];
	}
	else
	{
		CXModel*	temp;
		temp = new CXModel[m_totalModels + 1];

		memcpy(temp, m_pModel, sizeof(CXModel) * m_totalModels);

		delete[] m_pModel;
		m_pModel = temp;
	}

	m_pModel[m_totalModels].SetDevice(m_pDevice);	

#ifdef UNICODE
//	DXUtil_ConvertWideStringToAnsiCch(buff, file, _tcslen(file));
#else
	_tcscpy(buff, file);
#endif

	bool b =m_pModel[m_totalModels].LoadXFile(buff);

	xModelId =m_totalModels;
	m_totalModels++;
}

void	cRender::GetXModelBoundingSphere(int xModelId, D3DXVECTOR3* origin, float* radius)
{
	if(xModelId < 0 || xModelId >= m_totalModels || !m_pModel)
		return;

	D3DXVECTOR3 center;
	float r;

	m_pModel[xModelId].GetBoundingSphere(&center, &r);

	if(origin)
		*origin =center;

	if(radius)
		*radius = r;
}

void	cRender::UpdateXAnimation(int xModelId, float time, D3DXMATRIX* mat)
{
	m_pModel[xModelId].Update(time, mat);
}

void	cRender::RenderXModel(int xModelId)
{
	m_pModel[xModelId].Render();
}

void	cRender::ReleaseTex(int TexID)
{
	SAFE_RELEASE(m_pTexList[TexID].image);
}

void	cRender::ReleaseFont(int FontID)
{
	SAFE_RELEASE(m_ppFont[FontID]);
}

void	cRender::ReleaseAllFont()
{
	for(int i =0; i<m_totalFonts; i++)
	{
		SAFE_RELEASE(m_ppFont[i]);
	}
}

void	cRender::ReleaseAllTex()
{
	for(int i =0; i<m_totalTextures; i++)
	{
		SAFE_RELEASE(m_pTexList[i].image);
	}
}

void	cRender::shutDownALLXmodels()
{
	for(int i =0;i<m_totalModels; i++)
	{
		m_pModel->Shutdown();
	}
}

void	cRender::shutDown()
{

	SAFE_RELEASE(m_pDevice);

//	releaseAllGUI();
	ReleaseAllFont();
	ReleaseAllTex();
	shutDownALLXmodels();
}

// void	cRender::releaseAllGUI()
// {
// 	for(int i =0; i<m_totalGUIs; i++)
// 	{	
// 		m_pGUIList[i].shut_down()
// 	}
// 
// 	SAFE_DELETE_ARRAY(m_pGUIList);
// }

// void	cRender::createGUI(int& ID)
// {
// 	if(!m_pGUIList)
// 	{
// 		m_pGUIList =new cGUIsystem[1];
// 		m_pGUIList->setGUIsystem(m_pDevice, 800, 600);
// 	}
// 	else
// 	{
// 		cGUIsystem* temp =new cGUIsystem[m_totalGUIs + 1];
// 		memcpy(m_pGUIList,temp, sizeof(cGUIsystem) * m_totalGUIs);
// 		delete[] m_pGUIList;
// 		m_pGUIList =temp;
// 
// 		m_pGUIList[m_totalGUIs].setGUIsystem(m_pDevice, 800, 600);
// 	}
// 
// 	ID =m_totalGUIs;
// 	m_totalGUIs++;
// }
// 
// bool	cRender::addGUIBackDrop(int guiID, TCHAR* fileName)
// {
// 	if(guiId >= m_totalGUIs) return false;
// 
// 	int texID = -1, staticID = -1;
// 
// 	if(!addTexture(fileName, &texID)) return false;
// 
// 	RhwTextureVertex obj[] =
// 	{
// 		{(float)m_windowWidth, 0, 0, 1, 1, 0},
// 		{(float)m_windowWidth, (float)m_windowHeight,0, 1,  1, 1},
// 		{0, 0, 0, 1,  0, 0},
// 		{0, (float)m_windowHeight, 0, 1, 0, 1},
// 	};
// 
// 	if(!CreateStaticBuffer(RHW_TEXTURE_FVF, TRIANGLE_STRIP, 4, 0,
// 		sizeof(stGUIVertex), (void**)&obj, NULL,
// 		&staticID)) return false;
// 
// 	return m_pGUIList[guiId].AddBackdrop(texID, staticID);
// }


//------------------------------------
//protect

void cRender::generate_matrial(D3DMATERIAL9& mtrl, sMaterial& m )
{
	mtrl.Ambient  = m.a;
	mtrl.Diffuse  = m.d;
	mtrl.Specular = m.s;
	mtrl.Emissive = m.e;
	mtrl.Power    = m.p;
}


void cRender::generate_direction_light(D3DLIGHT9&	dir_light, D3DVECTOR&	dir, 
									   D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color
									   )
{//方向光没有位置。无穷远的,range无效
	ZeroMemory(&dir_light, sizeof(dir_light));

	//	dir_light.Position =pos;
	dir_light.Direction =dir;
	dir_light.Type =D3DLIGHT_DIRECTIONAL;	
	dir_light.Ambient   = ambient_color;
	dir_light.Diffuse   = diffuse_color;
	dir_light.Specular  = specular_color;   //镜面高光的值

}

void cRender::generate_point_light(D3DLIGHT9& point_light, D3DVECTOR& pos,
								   D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color
								   )
{//点光源没有位置
	ZeroMemory(&point_light, sizeof(point_light));

	point_light.Position =pos;
	point_light.Type =D3DLIGHT_POINT;	
	point_light.Ambient   = ambient_color;
	point_light.Diffuse   = diffuse_color;
	point_light.Specular  = specular_color; //镜面高光的值
	point_light.Range        = 1000.0f;
	point_light.Falloff      = 1.0f;
	point_light.Attenuation0 = 1.0f;
	point_light.Attenuation1 = 0.0f;
	point_light.Attenuation2 = 0.0f;
}

void cRender::generate_spot_ligth(D3DLIGHT9&	spot_light, D3DVECTOR&	pos, D3DVECTOR& dir, 
								  D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color
								  )
{//聚光灯有位置也有方向
	ZeroMemory(&spot_light, sizeof(spot_light));

	spot_light.Position =pos;
	spot_light.Direction =dir;
	spot_light.Type =D3DLIGHT_SPOT;	
	spot_light.Ambient   = ambient_color ;
	spot_light.Diffuse   = diffuse_color;
	spot_light.Specular  = specular_color;  //镜面高光的值
	spot_light.Range        = 1000.0f;
	spot_light.Falloff      = 1.0f;
	spot_light.Attenuation0 = 1.0f;
	spot_light.Attenuation1 = 0.0f;
	spot_light.Attenuation2 = 0.0f;
	spot_light.Phi =0.9f;
	spot_light.Theta =0.4f;	
}

void	cRender::calculateProjMatrix(float fov, float n, float f)
{
	if(!m_pDevice) return;

	D3DXMatrixPerspectiveFovLH(&m_projection, fov,
		(float)m_windowWidth/(float)m_windowHeight, n, f);

	m_pDevice->SetTransform(D3DTS_PROJECTION, &m_projection);
}

void	cRender::setWorldMatrix(D3DXMATRIX* temp)
{
	m_pDevice->SetTransform(D3DTS_WORLD, temp);
}

void	cRender::setViewMatrix(D3DXMATRIX* temp)
{
	m_pDevice->SetTransform(D3DTS_VIEW, temp);
}

void	cRender::drawGeometry(sD3DStaticBuffer& buffer_dsc)
{
	D3DPRIMITIVETYPE	temp_type =D3DPT_FORCE_DWORD;

	switch(buffer_dsc.primType)
	{
	case TRIANGLE_LIST:
		temp_type =D3DPT_TRIANGLELIST;
		break;

	case TRIANGLE_STRIP:
		temp_type =D3DPT_TRIANGLESTRIP;
		break;

	case TRIANGLE_FAN:
		temp_type =D3DPT_TRIANGLEFAN;
		break;
		
	}

	m_pDevice->SetFVF(buffer_dsc.fvf);  // LIGHT_TEXTURE_FVF
	m_pDevice->SetStreamSource(0, buffer_dsc.vbPtr, 0, buffer_dsc.stride);  // LightTextureVertex
	m_pDevice->SetIndices(buffer_dsc.ibPtr);	 // ib
	m_pDevice->DrawIndexedPrimitive(temp_type, 0, 0, 
		buffer_dsc.numVerts, 0, buffer_dsc.numFaces);
}





bool	createRender(cRender** _render)
{
	*_render =new cRender;

	if(*_render)
		return true;
	else
		return false;
}



