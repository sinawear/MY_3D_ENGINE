

#include "engine_light.h"


void	generate_direction_light(D3DLIGHT9&	dir_light, D3DVECTOR&	dir, 
								 D3DXCOLOR&	color)
{//方向光没有位置。
	ZeroMemory(&dir_light, sizeof(dir_light));
	
	//	dir_light.Position =pos;
	dir_light.Direction =dir;
	dir_light.Type =D3DLIGHT_DIRECTIONAL;	
	dir_light.Ambient   = color * 0.6f ;
	dir_light.Diffuse   = color * 0.6f;
	dir_light.Specular  = color * 0.6f;   //镜面高光的值
}

void	generate_point_light(D3DLIGHT9& point_light, D3DVECTOR& pos,
							 D3DXCOLOR& color)
{//点光源没有位置
	ZeroMemory(&point_light, sizeof(point_light));
	
	point_light.Position =pos;
	point_light.Type =D3DLIGHT_POINT;	
	point_light.Ambient   = color * 0.6f;
	point_light.Diffuse   = color;
	point_light.Specular  = color * 0.6f; //镜面高光的值
	point_light.Range        = 1000.0f;
	point_light.Falloff      = 1.0f;
	point_light.Attenuation0 = 1.0f;
	point_light.Attenuation1 = 0.0f;
	point_light.Attenuation2 = 0.0f;
}

void	generate_spot_ligth(D3DLIGHT9&	spot_light, D3DVECTOR&	pos,
							D3DVECTOR& dir, D3DXCOLOR& color)
{//聚光灯有位置也有方向
	ZeroMemory(&spot_light, sizeof(spot_light));

	spot_light.Position =pos;
	spot_light.Direction =dir;
	spot_light.Type =D3DLIGHT_SPOT;	
	spot_light.Ambient   = color ;
	spot_light.Diffuse   = color;
	spot_light.Specular  = color * 0.6f;  //镜面高光的值
	spot_light.Range        = 1000.0f;
	spot_light.Falloff      = 1.0f;
	spot_light.Attenuation0 = 1.0f;
	spot_light.Attenuation1 = 0.0f;
	spot_light.Attenuation2 = 0.0f;
	spot_light.Phi =0.9f;
	spot_light.Theta =0.4f;	
}