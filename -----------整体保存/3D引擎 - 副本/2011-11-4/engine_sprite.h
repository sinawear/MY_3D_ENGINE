#pragma once
#include "engine_base.h"

typedef	struct Sprite 
{
	// 物体的要旋转的角度
	float	rotation_angle;

//	D3DXVECTOR3	postion;
//	D3DXVECTOR3	deep;
//	D3DXVECTOR3	up;
//	D3DXVECTOR3	right;

	D3DXMATRIX	view_matrix;
	TCHAR*	sprite_file_name;
}Sprite;