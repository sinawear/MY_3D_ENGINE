#include "engine_font.h"


// 字体输出,以各个矩形框为单位..
void	draw_text(IDirect3DDevice9* pDevice, HWND handle,TCHAR* string, RECT* fontRect)
{
	D3DXFONT_DESC	font_desc;
	ID3DXFont*		pFont;

	ZeroMemory(&font_desc,sizeof(font_desc));
	font_desc.Height =25;
	font_desc.Width =12;
	_tcscpy(font_desc.FaceName,_T("Fixedsys"));

	D3DXCreateFontIndirect(pDevice, &font_desc, &pFont); // indirect :间接的.
	
/*	GetClientRect(handle,&fontRect);*/
	pFont->DrawText(NULL, string,_tcslen(string), fontRect, DT_LEFT |DT_WORDBREAK , 0xffff0000);
	
	SAFE_RELEASE(pFont);
}	