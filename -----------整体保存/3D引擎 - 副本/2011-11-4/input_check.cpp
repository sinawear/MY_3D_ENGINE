

#include "input_check.h"

cMouseDevice::cMouseDevice(IDirectInput8* pDI, HWND	handle, bool exclusive)
{
	m_xPos =0;
	m_yPos =0;
	m_zPos =0;
	ZeroMemory(&m_mouseState, sizeof(DIMOUSESTATE));

	//create mouse
	pDI->CreateDevice(GUID_SysMouse,&m_pMouseDevice,NULL);

	// ~.~
	DWORD	t;
	if(exclusive)
		t =DISCL_FOREGROUND | DISCL_EXCLUSIVE;
	else
		t =DISCL_FOREGROUND | DISCL_NONEXCLUSIVE;

	m_pMouseDevice->SetCooperativeLevel(handle, t);

	m_pMouseDevice->SetDataFormat(&c_dfDIMouse);
	m_pMouseDevice->Acquire();
}

cMouseDevice::~cMouseDevice()
{

}

void	cMouseDevice::updateDevice()
{

	memcpy(&m_mouseOldState, &m_mouseState, sizeof(DIMOUSESTATE));
	// 对付设备丢失
	if(DIERR_INPUTLOST==(m_pMouseDevice->GetDeviceState(sizeof(DIMOUSESTATE),&m_mouseState)))
		m_pMouseDevice->Acquire();

	m_xPos +=m_mouseState.lX;
	m_yPos +=m_mouseState.lY;
	m_zPos +=m_mouseState.lZ;
}

bool	cMouseDevice::isDown(DWORD t)
{
	return m_mouseState.rgbButtons[t] & 0x80;
}

bool	cMouseDevice::isUp(DWORD t)
{//状态为按下，并且和上次的状态不同。
	return (~m_mouseState.rgbButtons[t] & 0x80) && 
		(m_mouseState.rgbButtons[t]!=m_mouseOldState.rgbButtons[t]);
}

POINT	cMouseDevice::getPos()
{//得到位置..初始化位置怎么处理呢?初始化
	POINT	point;
	point.x =m_xPos;
	point.y =m_yPos;

	return point;
}

LONG	cMouseDevice::getWheelPos()
{
	return	m_zPos; 
}

void	cMouseDevice::shutDown()
{
	m_pMouseDevice->Unacquire();
	SAFE_RELEASE(m_pMouseDevice);
}

LONG	cMouseDevice::getState(int t)
{
	if(t == 0)
		return m_mouseState.lX;
	if(t == 1)
		return m_mouseState.lY;
	if(t ==2 )
		return m_mouseState.lZ;
	else
		return NULL;
}

//----------------------------------------------------------
cKeyboardDevice::cKeyboardDevice(IDirectInput8* pDI, HWND	handle)
{
	ZeroMemory(&m_key,sizeof(m_key));

	pDI->CreateDevice(GUID_SysKeyboard,&m_pKeyDevice,NULL);
	// 不独占.
	m_pKeyDevice->SetCooperativeLevel(handle,DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	m_pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
	m_pKeyDevice->Acquire();
}

cKeyboardDevice::~cKeyboardDevice()
{

}

void	cKeyboardDevice::updateDevice()
{

	memcpy(&m_oldKey, &m_key, sizeof(m_key));
	if(DIERR_INPUTLOST==(m_pKeyDevice->GetDeviceState(sizeof(m_key), m_key)))
		m_pKeyDevice->Acquire();
	
}

POINT	cKeyboardDevice::getPos()
{
	POINT	t ={0,0};

	return t;
}

LONG	cKeyboardDevice::getWheelPos()
{
	return 0L;
}

bool	cKeyboardDevice::isDown(DWORD t)
{
	return m_key[t] & 0x80;
}

bool	cKeyboardDevice::isUp(DWORD t)
{
	return (~m_key[t] & 0x80) && (m_key[t]!=m_oldKey[t]);
}

void	cKeyboardDevice::shutDown()
{
	m_pKeyDevice->Unacquire();
	SAFE_RELEASE(m_pKeyDevice);
}

LONG	cKeyboardDevice::getState(int t)
{
	return NULL;
}

//----------------------------------------------------------
cInputController::cInputController(HINSTANCE hinstance, HWND hwnd, bool exclusive)
{
	m_hinstance =hinstance;
	m_hwnd =hwnd;

	DirectInput8Create(hinstance,DIRECTINPUT_VERSION,IID_IDirectInput8, (void **)&m_pDI, NULL);


	m_pKeyDevice =new cKeyboardDevice(m_pDI, hwnd);
	m_pMouseDevice =new cMouseDevice(m_pDI, hwnd, false);
}

cInputController::~cInputController()
{
	shutDown();
}

void	cInputController::init()
{

	
}

POINT	cInputController::getMousePos()
{
	return m_pKeyDevice->getPos();
}

LONG	cInputController::getMouseWheelPos()
{
	return m_pKeyDevice->getWheelPos();
}

bool	cInputController::isKeyDown(DWORD t)
{
	return m_pKeyDevice->isDown(t);
}

bool	cInputController::isKeyUp(DWORD t)
{
	return m_pKeyDevice->isUp(t);
}

bool	cInputController::isMouseDown(DWORD t)
{
	return m_pMouseDevice->isDown(t);
}

bool	cInputController::isMouseUp(DWORD t)
{
	return m_pMouseDevice->isUp(t);
}

void	cInputController::updateDevice()
{
	if(m_hwnd ==GetFocus())
	{
		m_pMouseDevice->updateDevice();
		m_pKeyDevice->updateDevice();
	}

}

void	cInputController::shutDown()
{
	m_pKeyDevice->shutDown();
	m_pMouseDevice->shutDown();

	SAFE_DELETE(m_pKeyDevice);
	SAFE_DELETE(m_pMouseDevice);

	SAFE_RELEASE(m_pDI);
}

LONG	cInputController::getMouseState(int t)
{
	LONG	temp =m_pMouseDevice->getState(t);

	return temp;

	// return m_pMouseDevice->getState(t);
	// 这样会失败.getMouseState().内存地址变成了0
	
}