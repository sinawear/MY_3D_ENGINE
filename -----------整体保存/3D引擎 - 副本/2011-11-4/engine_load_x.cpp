
#include "engine_load_x.h"


cStaticModel::cStaticModel()
{
	_isVisable =true;
}

cStaticModel::~cStaticModel()
{
	
}

void	cStaticModel::SetDevice(IDirect3DDevice9*	device)
{
	_device =device;
}

void	cStaticModel::LoadXFile( char* x_file_name)
{

	ID3DXBuffer*	pAdja_buff,*pMaterial_buff;
	
	// 得到X文件的材质指针,材质个数,网格
	D3DXLoadMeshFromX(x_file_name, D3DXMESH_MANAGED, _device, &pAdja_buff, 
		&pMaterial_buff, NULL, &_numMaterial, &_mesh);
	
	// 材质数和纹理数是匹配的.
	// 开辟材质缓冲,纹理缓冲.
	D3DXMATERIAL*	pD3DX_Material =(D3DXMATERIAL*)pMaterial_buff->GetBufferPointer();	
	_material = new	D3DMATERIAL9[_numMaterial];
	_tex = new	LPDIRECT3DTEXTURE9[_numMaterial];
	
	int hr;
	for(DWORD i=0;i<_numMaterial;i++)
	{
		
		_material[i] =pD3DX_Material[i].MatD3D;
		// 从X文件加载的材质没有AMBIENT;
		// 当我们有范围光源时,可注释掉.
		_material[i].Ambient = _material[i].Diffuse ;
		
		hr =D3DXCreateTextureFromFile(_device, pD3DX_Material[i].pTextureFilename, &_tex[i]);
		
		if(FAILED(hr))
		{
			_tex[i] =0;
		}
	}
	
	
	SAFE_RELEASE(pAdja_buff);
	SAFE_RELEASE(pMaterial_buff);
	
}

void	cStaticModel::Render()
{
	for(int i=0; i<_numMaterial; i++)
	{
		_device->SetMaterial(&_material[i]);
		_device->SetTexture(0,_tex[i]);
		_mesh->DrawSubset(i);
	}
}

ID3DXMesh*	cStaticModel::getMesh()
{
	return _mesh;
}

bool	cStaticModel::isVisable()
{
	return _isVisable;
}

void	cStaticModel::setVisable(bool t)
{
	_isVisable =t;
}

void	cStaticModel::Shutdown()
{
	SAFE_RELEASE(_mesh);
	
	for(int i =0; i<_numMaterial; i++)
	{
		SAFE_RELEASE(_tex[i]);
	}

	SAFE_DELETE_ARRAY(_material);
}


D3DXMATRIX*	cStaticModel::getMatrix()
{
	return &_modulate;
}

void	cStaticModel::setMatrix(D3DXMATRIX t)
{
	_modulate =t;
}