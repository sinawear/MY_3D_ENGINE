
#pragma once

#include "engine_base.h"


/*
加载静态X文件
*/

// 不支持动画
class cStaticModel
{
public:
	cStaticModel();
	~cStaticModel();

	void	LoadXFile(char* x_file_name);
	void	SetDevice(IDirect3DDevice9*	device);
	void	Render();
	ID3DXMesh*	getMesh();

	bool	isVisable();
	void	setVisable(bool	t);

	void	setMatrix(D3DXMATRIX t);
	D3DXMATRIX*	getMatrix();
	
	void	Shutdown();

private:
	IDirect3DDevice9* _device;
	IDirect3DTexture9** _tex;
	D3DMATERIAL9*	_material;
	DWORD		_numMaterial;
	ID3DXMesh*	_mesh;

	bool	_isVisable;
	D3DXMATRIX	 _modulate;
};

