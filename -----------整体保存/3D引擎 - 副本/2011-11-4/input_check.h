
#ifndef	_INPUT_CHECK_
#define _INPUT_CHECK_

#define	KEYSIZE	256

#include "engine_base.h"


class cDeviceInterface
{
public:
	virtual	void	updateDevice() =0;
	virtual	bool	isUp(DWORD t) =0;
	virtual	bool	isDown(DWORD t) =0;
	virtual POINT	getPos() =0;
	virtual LONG	getWheelPos() =0;
	virtual void	shutDown() =0;
	virtual LONG    getState(int t) =0;

};

class cMouseDevice: public cDeviceInterface
{
public:
	cMouseDevice(IDirectInput8* pDI, HWND	handle, bool exclusive);
	~cMouseDevice();

	void	updateDevice() ;
	bool	isUp(DWORD t) ;
	bool	isDown(DWORD t) ;
	POINT	getPos() ;
	LONG	getWheelPos() ;
	void	shutDown() ;
	LONG    getState(int t) ; //得到鼠标的移动状态，X方向，Y方向。滚轮的移动状态Z方向。

//	bool	isDefault(DWORD t);	//用于得到鼠标的默认状态

protected:		//该类的成员函数或派生类的成员函数才能访问
	IDirectInputDevice8*	m_pMouseDevice;
	DIMOUSESTATE	m_mouseState;
	DIMOUSESTATE	m_mouseOldState;
	LONG	m_xPos, m_yPos, m_zPos;
};


class cKeyboardDevice: public cDeviceInterface
{
public:
	cKeyboardDevice(IDirectInput8* pDI, HWND	handle);
	~cKeyboardDevice();

	void	updateDevice() ;
	bool	isUp(DWORD t) ;
	bool	isDown(DWORD t) ;
	POINT	getPos() ;
	LONG	getWheelPos() ;
	void	shutDown() ;
	LONG    getState(int t) ;

protected:
	IDirectInputDevice8*	m_pKeyDevice;
	BYTE	m_key[KEYSIZE];
	BYTE	m_oldKey[KEYSIZE];
};


class cInputInterface
{
public:
	virtual	void		init() =0;
	virtual	void		updateDevice() =0;
	virtual	bool		isKeyDown(DWORD	t) =0;
	virtual	bool		isKeyUp(DWORD t) =0;
	virtual	bool		isMouseDown(DWORD t) =0;
	virtual bool		isMouseUp(DWORD t) =0;
	virtual POINT		getMousePos() =0;
	virtual LONG		getMouseWheelPos() =0;
	virtual	void		shutDown() =0;
	virtual LONG		getMouseState(int t) =0;
};

class cInputController: public cInputInterface
{
public:
	cInputController(HINSTANCE hinstance, HWND	hwnd, bool exclusive);
	~cInputController();

	void	init();
	void	updateDevice();
	bool	isKeyDown(DWORD t);
	bool	isKeyUp(DWORD t) ;
	bool	isMouseDown(DWORD t) ;
	bool	isMouseUp(DWORD t) ;
	POINT	getMousePos() ;
	LONG	getMouseWheelPos() ;
	void	shutDown() ;
	LONG	getMouseState(int t) ;
private:
	IDirectInput8*		m_pDI;
	cMouseDevice*		m_pMouseDevice;
	cKeyboardDevice*	m_pKeyDevice;
	HINSTANCE	m_hinstance;
	HWND		m_hwnd;
};


#endif