
/*
δͨ�����ַ�����.
*/

#pragma	once

#include "engine_base.h"


class cSystemLog
{
public:
	void	clearLog(TCHAR*	fileName);
	void	writeLog(TCHAR*	fileName, TCHAR* message,...);
	void	appendLog(TCHAR* fileName, TCHAR* message,...);
	
	void	writeMatrix(TCHAR* fileName, D3DXMATRIX&	t);
	void	writeVector(TCHAR* fileName, D3DXVECTOR3&	t);

private:
	void	write(TCHAR* message, va_list start);

	ofstream	m_file;
};


