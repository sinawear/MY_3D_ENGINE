

#include	"PSystem.h"


cParticleSystem::cParticleSystem()
{
	
}

cParticleSystem::~cParticleSystem()
{
	SAFE_RELEASE(_tex);
	SAFE_RELEASE(_vb);
}

bool	cParticleSystem::init(IDirect3DDevice9* device, char* FileName)
{
	_device =device;

	// 加载纹理
	D3DXCreateTextureFromFile(_device, FileName, &_tex);

	// 创建整个顶点缓冲
	_device->CreateVertexBuffer(_vb_size*sizeof(ColorVertex), 
		D3DUSAGE_DYNAMIC | D3DUSAGE_POINTS | D3DUSAGE_WRITEONLY,
		COLOR_FVF, D3DPOOL_DEFAULT, &_vb, NULL);

	return true;
}

void	cParticleSystem::reset()
{
	std::list<sAttribute>::iterator  i;
	
	for( i =_particles.begin() ; i != _particles.end(); i++)
	{
		resetParticles(&(*i));
	}	
}

bool	cParticleSystem::isDead()
{
	std::list<sAttribute>::iterator  i;

	for( i =_particles.begin() ; i != _particles.end(); i++)
	{
		if(i->is_alive)
			return false;
	}

	return true;
}

bool	cParticleSystem::isEmpty()
{
	return _particles.empty();
}

void	cParticleSystem::removeDeadParticles()
{
	std::list<sAttribute>::iterator  i =_particles.begin();

	while( i != _particles.end() )
	{
		if( i->is_alive == false )
		{
			// erase returns the next iterator, so no need to
		    // incrememnt to the next one ourselves.
			i = _particles.erase(i); 
		}
		else
		{
			i++; // next in list
		}
	}
}

void	cParticleSystem::addParticle()
{
	sAttribute		attr;
	resetParticles(&attr);//调用子类的实现，也可以看成逻辑与实现向分离
	_particles.push_back(attr);
}

void	cParticleSystem::preRender()
{	
	// use alpha from texture
	_device->SetRenderState(D3DRS_LIGHTING, false);
	_device->SetRenderState(D3DRS_POINTSPRITEENABLE, true);
	_device->SetRenderState(D3DRS_POINTSCALEENABLE, true); 
	_device->SetRenderState(D3DRS_POINTSIZE, FtoDw(_size));
	_device->SetRenderState(D3DRS_POINTSIZE_MIN, FtoDw(0.0f));

	// control the size of the particle relative to distance
	_device->SetRenderState(D3DRS_POINTSCALE_A, FtoDw(0.0f));
	_device->SetRenderState(D3DRS_POINTSCALE_B, FtoDw(0.0f));
	_device->SetRenderState(D3DRS_POINTSCALE_C, FtoDw(1.0f));

	_device->SetRenderState(D3DRS_ALPHABLENDENABLE, true); //开启alpha混合
}

void	cParticleSystem::postRender()
{
	_device->SetRenderState(D3DRS_LIGHTING,          false);  //暂时不支持灯光
	_device->SetRenderState(D3DRS_POINTSPRITEENABLE, false);
	_device->SetRenderState(D3DRS_POINTSCALEENABLE,  false);
	_device->SetRenderState(D3DRS_ALPHABLENDENABLE,  false);//关闭alpha混合

}

void	cParticleSystem::render()
{
	// 判断是否为空
	if(_particles.empty())
		return;

	// 设置渲染前的状态
	preRender();

	// 设置纹理，FVF,顶点流
	_device->SetTexture(0, _tex);
	_device->SetFVF(COLOR_FVF);
	_device->SetStreamSource(0, _vb, 0, sizeof(ColorVertex));

	// 判断偏移是否到达缓冲的最末端，是：移动的最开始
	if(_vb_offset >= _vb_size)
		_vb_offset =0;

	ColorVertex*		v =NULL;
	// 如果偏移在最开始，追加写入
	_vb->Lock(_vb_offset * sizeof(ColorVertex), _vb_batch_size * sizeof(ColorVertex), 
		(void**)&v, _vb_offset? D3DLOCK_NOOVERWRITE:D3DLOCK_DISCARD);

	DWORD	numBatch =0;
	// 迭代粒子
	for(std::list<sAttribute>::iterator i =_particles.begin(); i!=_particles.end(); i++)
	{
		if(i->is_alive)
		{
			v->pos =i->position;
			v->color =i->color;
			v++;
			
			numBatch++;		
			
			// 如果，粒子数等于一批的大小
			if(numBatch == _vb_batch_size)
			{
				_vb->Unlock();
				
				// 渲染一批粒子
				_device->DrawPrimitive(D3DPT_POINTLIST, _vb_offset, _vb_batch_size);
				
				_vb_offset +=_vb_batch_size;
				
				// 缓冲最末端
				if(_vb_offset >= _vb_size)
					_vb_offset =0;
				
				_vb->Lock(_vb_offset * sizeof(ColorVertex), _vb_batch_size * sizeof(ColorVertex), 
					(void**)&v, _vb_offset? D3DLOCK_NOOVERWRITE:D3DLOCK_DISCARD);
				
				numBatch =0;
			}
			
		}
	}
	_vb->Unlock();
	
	// 可能并不能达到一批的数量
	if(numBatch >0)
	{
		_device->DrawPrimitive(D3DPT_POINTLIST, _vb_offset, numBatch);

		// 加上一批的大小
		_vb_offset +=_vb_batch_size;	
	}


	// 设置渲染后状态
	postRender();


}


//-----------

cParticleGun::cParticleGun(cCameraUVN* camera)
{
	_camera          = camera;
	_size            = 0.8f;//粒子大小
	_vb_size          = 2048;//顶点缓冲大小
	_vb_offset        = 0;  //顶点缓冲的偏移
	_vb_batch_size     = 512; //一批的粒子数
}

void	cParticleGun::resetParticles(sAttribute* attr)
{
	// 设置属性为存活
	attr->is_alive =true;

	D3DXVECTOR3	 pos, dir;
	//得到摄像机位置，方向
	_camera->getPos(pos);
	_camera->getDeep(dir);

//	pos.y =0;
//	D3DXVec3Normalize(&dir, &dir);
	//赋值位置给属性
	attr->position =pos;
	attr->velocity =dir * 200;

	//设置颜色
	attr->color =GREEN;

	//设置年龄，设置生命
	attr->age =0;
	attr->life_time =1;//秒
}

void	cParticleGun::update(float EplasedTime)
{
	for(std::list<sAttribute>::iterator i =_particles.begin(); i!=_particles.end(); i++)
	{	
		// 位置=位置+速度×时间
		i->position +=i->velocity * EplasedTime;
		// 年龄=年龄+时间
		i->age +=EplasedTime;

		// 年龄大于生命，将死亡
		if(i->age > i->life_time)
			i->is_alive =false;
		
	}

	removeDeadParticles();
}

void	cParticleGun::preRender()
{
	cParticleSystem::preRender();

	_device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
}

void	cParticleGun::postRender()
{
	cParticleSystem::postRender();

}

void	cParticleGun::collision(void*	 model_pointer)
{
	CXModel*	p
	for(std::list<sAttribute>::iterator i =_particles.begin(); i!=_particles.end(); i++)
	{	
		
	}
}
//
//----------
//

cParticleSnow::cParticleSnow(cObjectBound* bound_box, int numParticles)
{
	_bound_box =bound_box;

	_size =0.25f;
	_vb_size =2048;//顶点缓冲的大小
	_vb_offset =0;
	_vb_batch_size =512;

	for(int i =0; i<numParticles; i++)
		addParticle();
}

void	cParticleSnow::resetParticles(sAttribute* attr)
{
	D3DXVECTOR3		min_pos =_bound_box->get_bound_box()->min_pos;
	D3DXVECTOR3		max_pos =_bound_box->get_bound_box()->max_pos;

	// 设置标记为存活
	attr->is_alive =true;

	// 得到范围内的随机向量
	GetRandomVector(&attr->position, &min_pos, &max_pos);

	// 设置为边界框的最顶端
	attr->position.y =max_pos.y;

	// 下落方向，稍稍偏左
	attr->velocity.x =GetRandomFloat(0, 1) * -10.0f;
	attr->velocity.y =GetRandomFloat(0, 1) * -3.0f;
	attr->velocity.z =0;

	attr->color =WHITE;
}

void	cParticleSnow::update(float EplasedTime)
{
	for(std::list<sAttribute>::iterator i =_particles.begin(); i!=_particles.end(); i++)
	{
		i->position += i->velocity * EplasedTime;

		if(_bound_box->isPointInsideBox(i->position))
			;
		else
			resetParticles(&(*i));
	}
}

void	cParticleSnow::preRender()
{
	cParticleSystem::preRender();

	// 开启alpha测试
	_device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE); 
	_device->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x00000001);
    _device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
}

void	cParticleSnow::postRender()
{
	cParticleSystem::postRender();

	_device->SetRenderState(D3DRS_ALPHATESTENABLE, false);  //关闭alpha测试
}

//
//--------------
//
cParticleFirework::cParticleFirework(D3DXVECTOR3* origin, int numParticles)
{
	_origin =*origin;
	_max_particles =numParticles;

	_size =0.9f;
	_vb_size =2048;//顶点缓冲的大小
	_vb_offset =0;
	_vb_batch_size =512;

	for(int i =0; i<numParticles; i++)
		addParticle();
}

void	cParticleFirework::resetParticles(sAttribute* attr)
{
	attr->is_alive =true;
	attr->position =_origin;

	// 设置速度
	GetRandomVector(&attr->velocity, &D3DXVECTOR3(-1,-1,-1), &D3DXVECTOR3(1,1,1));
	D3DXVec3Normalize(&attr->velocity, &attr->velocity);
	attr->velocity *=100.0f;

	// 随机颜色
	attr->color =D3DXCOLOR(GetRandomFloat(0,1), GetRandomFloat(0,1),GetRandomFloat(0,1), 1.0f);

	attr->age =0;
	attr->life_time =2;
}

void	cParticleFirework::update(float EplasedTime)
{
	for(std::list<sAttribute>::iterator i =_particles.begin(); i!=_particles.end(); i++)
	{	
		if(i->is_alive)
		{
			// 位置=位置+速度×时间
			i->position +=i->velocity * EplasedTime;
			// 年龄=年龄+时间
			i->age +=EplasedTime;
			
			// 年龄大于生命，将死亡
			if(i->age > i->life_time)
				i->is_alive =false;
		}

	}
}

void	cParticleFirework::preRender()
{
	cParticleSystem::preRender();

	_device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	_device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	_device->SetRenderState(D3DRS_ZWRITEENABLE, false);
}

void	cParticleFirework::postRender()
{
	cParticleSystem::postRender();
	
	_device->SetRenderState(D3DRS_ZWRITEENABLE, true);
}
