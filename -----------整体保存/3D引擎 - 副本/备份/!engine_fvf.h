#pragma once

#include "engine_base.h"

//
//---------------------------------
// 宏区

// 灵活顶点格式宏
//
#define COLOR_FVF		 D3DFVF_XYZ | D3DFVF_DIFFUSE
#define LIGHT_TEXTURE_FVF		 D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1
#define TEXTURE_FVF		 D3DFVF_XYZ | D3DFVF_TEX1
#define SPRITE_FVF		 D3DFVF_XYZW	| D3DFVF_TEX1
#define RHW_TEXTURE_FVF		D3DFVF_XYZRHW | D3DFVF_TEX1
#define PARTICLE_FVF	D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_PSIZE

//
//---------------------------------
// 数据结构区

// 坐标加颜色
typedef struct ColorVertex
{
	ColorVertex(){}
	ColorVertex(float _x,float _y,float _z,D3DCOLOR _rgba)
	{
		x=_x;
		y=_y;
		z=_z;
		rgba=_rgba;
	}
	
	float x,y,z;	
	D3DCOLOR	rgba;
	
}ColorVertex;


// 坐标加光照纹理
typedef struct LightTextureVertex
{
	LightTextureVertex(){}
	LightTextureVertex(float _x,float _y, float _z,float _nx,float _ny, \
		float _nz,float _u,float _v)
	{
		x=_x; y=_y; z=_z;  // vertex
		nx=_nx; ny=_ny; nz=_nz; // normal vertex
		u=_u; v=_v;  // UV;
	}

	float x,y,z;
	float nx,ny,nz;
	float u,v;

}LightTextureVertex;

// 坐标加纹理
typedef struct TextureVertex 
{
	TextureVertex(){}
	TextureVertex(float _x,float _y,float _z,float _u,float _v)
	{
		x=_x;y=_y;z=_z;
		u=_u;v=_v;
	}
	TextureVertex(D3DXVECTOR3 temp, D3DXVECTOR2 uv)
	{
		
	}

	float x,y,z;
	float u,v;
}TextureVertex;

// 屏幕坐标加纹理
typedef	struct RhwTextureVertex 
{
	RhwTextureVertex(){}
	RhwTextureVertex(float _x,float _y,float _z,float _w, float _u,float _v)
	{
		x=_x;y=_y;z=_z;w=_w;
		u=_u;v=_v;
	}
	float x,y,z,w;
	float u,v;
}RhwTextureVertex;


struct ParticleVertex 
{
	ParticleVertex(){}
	ParticleVertex(D3DXVECTOR3& _pos, D3DCOLOR& _color, float _size)
	{
		position =_pos;
		color =_color;
		size =_size;
	}
	D3DXVECTOR3		position;
	D3DCOLOR		color;
	float			size;
};
//=========================================================
//函数区

	