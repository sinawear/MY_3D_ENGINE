//2011-8-18 15:31

// 随机数产生工厂

#ifndef _RANDOM_FACTORY_
#define _RANDOM_FACTORY_

#include "engine_base.h"

// 0) 初始化随机数生成器种子

inline	void	my_srand()
{
	srand( (unsigned)time( NULL ) );
}

// 1) 控制在【0，0x7fff】整数范围内

inline int my_rand()
{
	return rand();
}


// 2）得到给定范围内的浮点数

inline float GetRandomFloat(float lowBound, float highBound)
{
	if( lowBound >= highBound ) // bad input
		return lowBound;
	
	// get random float in [0, 1] interval
	float f = (my_rand() % 10000) * 0.0001f; 
	
	// return float in [lowBound, highBound] interval. 
	return (f * (highBound - lowBound)) + lowBound; 
}

// 3)得到给定范围内的随机向量

inline void GetRandomVector( 
							D3DXVECTOR3* out,
							D3DXVECTOR3* min,
							D3DXVECTOR3* max)
{
	out->x = GetRandomFloat(min->x, max->x);
	out->y = GetRandomFloat(min->y, max->y);
	out->z = GetRandomFloat(min->z, max->z);
}

// 4）得到给定范围内的随机整形[x,y]
inline int	GetRandomInt(int low, int height)
{
	return (low + rand()%(height - low));
}

#endif