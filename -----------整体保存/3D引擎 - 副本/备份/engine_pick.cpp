
#include "engine_pick.h"

bool	is_ray_cross_bound_sphere(IDirect3DDevice9* device,
						   sBoundSphere& bound_sphere, POINT& point)
{
	//-------------------------------------
	// 把屏幕点转换到，视口坐标系中
	float x =(float)point.x;
	float y =(float)point.y;
	float px = 0.0f;
	float py = 0.0f;
	
	D3DVIEWPORT9 vp;
	device->GetViewport(&vp);
	
	D3DXMATRIX proj;
	device->GetTransform(D3DTS_PROJECTION, &proj);
	
	px = ((( 2.0f*x) / vp.Width)  - 1.0f) / proj(0, 0);
	py = (((-2.0f*y) / vp.Height) + 1.0f) / proj(1, 1);
	
	Ray ray;
	ray.origin    = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	ray.direction = D3DXVECTOR3(px, py, 1.0f);
	
	//---------------------------------------
	// 得出视口变化的逆矩阵，并把点转换到世界坐标系中
	
	D3DXMATRIX	temp, inverse_view_matrix;
	
	device->GetTransform(D3DTS_VIEW, &temp);
	
	D3DXMatrixInverse(&inverse_view_matrix, 0, &temp);
	
	// 变换点
	D3DXVec3TransformCoord(&ray.origin, &ray.origin, &inverse_view_matrix);
	
	// 变换向量
	D3DXVec3TransformNormal(&ray.direction, &ray.direction, &inverse_view_matrix);
	
	// 单位化射线向量的方向
	D3DXVec3Normalize(&ray.direction, &ray.direction);
	
	//-----------------------------------------------------
	// 测试是否相交
	
	D3DXVECTOR3 v = ray.origin - bound_sphere.center; 
	
	float a = 1.0f;
	float b = 2.0f * D3DXVec3Dot(&ray.direction, &v);
	float c = D3DXVec3Dot(&v, &v) - (bound_sphere.radius * bound_sphere.radius);
	
	// find the discriminant
	float discriminant = (b * b) - (4.0f * a * c);
	
	// test for imaginary number
	if( discriminant < 0.0f )
		return false;
	
	discriminant = sqrtf(discriminant);
	
	float s0 = (-b + discriminant) / 2.0f * a;
	float s1 = (-b - discriminant) / 2.0f * a;
	
	// if a solution is >= 0, then we intersected the sphere
	if( s0 >= 0.0f || s1 >= 0.0f )
		return true;
	
	return false;
}