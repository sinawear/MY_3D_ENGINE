
#include  "engine_init.h"

/*  
函数功能:创建设备
返回设备指针
*/
IDirect3DDevice9*	init_d3d9_device(HWND main_window_handle, BOOL windowed)
{
	IDirect3DDevice9*	pDevice;
	D3DCAPS9	caps;
	D3DPRESENT_PARAMETERS	d3dParam;
	int vp =0,result;
	
	//得到D3D接口
	IDirect3D9*	d3d9 = Direct3DCreate9(D3D_SDK_VERSION);
	
	//得到设备状态.
	d3d9->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps);
	
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
	{
		vp = D3DCREATE_HARDWARE_VERTEXPROCESSING; 
	}
	else 
	{
		MessageBox(NULL,_T("程序不支持软件运行"),_T("!"),0);
	}
	
	ZeroMemory(&d3dParam,sizeof(d3dParam));
	
	d3dParam.Windowed = windowed;
	d3dParam.hDeviceWindow = main_window_handle;
	d3dParam.BackBufferCount = 1;
	d3dParam.BackBufferFormat = D3DFMT_A8R8G8B8;
	d3dParam.EnableAutoDepthStencil = TRUE;
	d3dParam.Flags = 0;
	d3dParam.BackBufferWidth = 800;
	d3dParam.BackBufferHeight = 600;
	d3dParam.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dParam.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3dParam.MultiSampleQuality = 0;
	d3dParam.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dParam.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	d3dParam.AutoDepthStencilFormat = D3DFMT_D24S8;
	
	//得到设备接口.
	result = d3d9->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,main_window_handle, \
		vp,&d3dParam,&pDevice);
	
	
	//realse d3d9  
	SAFE_RELEASE(d3d9);
	
	return pDevice;
};

/*
初始化一个带颜色的三角形
*/
IDirect3DVertexBuffer9*	init_vertex_buffer(IDirect3DDevice9* pDevice)
{//create vertex buffer.[out]vertexBuff
	IDirect3DVertexBuffer9*	vb;
	
	pDevice->CreateVertexBuffer(sizeof(ColorVertex)*3,0,COLOR_FVF,D3DPOOL_MANAGED, \
		&vb,0);
	
	//copy data to buff.
	ColorVertex*		ver;
	vb->Lock(0,0,(void**)&ver,0);
	ver[0] = ColorVertex(D3DXVECTOR3(-1.0f, 0.0f, 2.0f),D3DCOLOR_ARGB(0,0,11*2,111*2));
	ver[1] = ColorVertex(D3DXVECTOR3(0.0f, 1.0f, 2.0f),D3DCOLOR_ARGB(0,111*2,11*2,0)); 
	ver[2] = ColorVertex(D3DXVECTOR3(1.0f, 0.0f, 2.0f),D3DCOLOR_ARGB(0,11*2,11*2,11*2));
	
//	ver[0] = ColorVertex(-315.0f, 43.0f, 315.0f,D3DCOLOR_ARGB(0,0,11*2,111*2));
//	ver[1] = ColorVertex(-295.0f, 30.0f, 315.0f,D3DCOLOR_ARGB(0,111*2,11*2,0)); 
//	ver[2] = ColorVertex( -295.0f,30.0f, 315.0f,D3DCOLOR_ARGB(0,11*2,11*2,11*2));
	vb->Unlock();
	
	return vb;
}

/*
初始化矩阵。
*/
void	init_draw_matrix(IDirect3DDevice9* pDevice)
{//D3D的矩阵变换是全局。
	
	D3DXVECTOR3 eye( 0,0,-3.0f);
	D3DXVECTOR3 lookAt(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	D3DXMATRIX view_mat;
	D3DXMatrixLookAtLH(&view_mat, &eye, &lookAt, &up);
	pDevice->SetTransform(D3DTS_VIEW, &view_mat);
	
	//set the projection matrix
	D3DXMATRIX	proj_matrix;
	D3DXMatrixPerspectiveFovLH(&proj_matrix,3.14f*0.25f,800.0f/600.0f,1.0f,1000.0f);
	pDevice->SetTransform(D3DTS_PROJECTION,&proj_matrix);
	
	// viewport translation
	D3DVIEWPORT9  viewport={0,0,800,600,0,1};
	pDevice->SetViewport(&viewport);
}
/*
初始化DX的状态机状态
*/
void	init_render_machine_state(IDirect3DDevice9* pDevice)
{
	//set draw model
	pDevice->SetRenderState(D3DRS_LIGHTING , false);
//	pDevice->SetRenderState(D3DRS_AMBIENT, 0x00f0f0f0);
//	pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
//	pDevice->SetRenderState(D3DRS_CULLMODE, false);
//	pDevice->SetRenderState(D3DRS_AMBIENT, 0xffffffff);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); //关闭背面消隐

}



