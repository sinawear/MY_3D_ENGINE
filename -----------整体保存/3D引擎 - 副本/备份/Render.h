#ifndef	_RENDER_
#define _RENDER_

#include "engine_base.h"
#include "engine_fvf.h"
#include "engine_GUI.h"
#include "XMesh.h"

//----------------------------------
// Primitive types.
enum PrimType
{
	NULL_TYPE,
	POINT_LIST,
	TRIANGLE_LIST,
	TRIANGLE_STRIP,
	TRIANGLE_FAN,
	LINE_LIST,
	LINE_STRIP
};

struct sD3DStaticBuffer
{
	sD3DStaticBuffer() : vbPtr(0), ibPtr(0), numVerts(0),
		numIndices(0), stride(0), fvf(0),
		primType(NULL_TYPE) {}
	
	LPDIRECT3DVERTEXBUFFER9 vbPtr; 
	LPDIRECT3DINDEXBUFFER9 ibPtr;
	UINT numVerts;
	UINT numFaces;
	UINT numIndices;
	UINT stride;  // 每顶点字节数
	unsigned long fvf;
	PrimType primType;
};

//-------------------------------------
struct sMaterial
{
	D3DXCOLOR a; //ambient
	D3DXCOLOR d; //diffuse
	D3DXCOLOR s; //specular
	D3DXCOLOR e; //emissive
	float p;
};

// define lights
#define LIGHT_POINT        1
#define LIGHT_DIRECTIONAL  2
#define LIGHT_SPOT         3

struct sLight
{
	sLight()
	{

	}
	D3DXVECTOR3	pos;
	D3DXVECTOR3 dir;
	D3DXCOLOR	ambient_color;
	D3DXCOLOR   diffuse_color;
	D3DXCOLOR	specular_color;
	int type;
};

struct sTexture
{
	TCHAR*	name;
	int		width;
	int		height;
	IDirect3DTexture9*	image;
};

// Texture states.
enum TextureFilter
{
	MIN_FILTER,
	MAG_FILTER,
	MIP_FILTER
};

// Texture filtering.
enum FilterType
{
	POINT_TYPE,
	LINEAR_TYPE,
	ANISOTROPIC_TYPE
};


//  
//  
class cRender
{
public:
	cRender();
	~cRender();

	// D9设备系列
	IDirect3DDevice9* init_d3d9_device(HWND window_handle, int width, int height, BOOL windowed =TRUE);

	// 渲染系列
	void setClearColor(D3DCOLOR	temp);
	void startRender(bool bColor, bool bDepth, bool bStencil);
	void clearBuff(bool bColor, bool bDepth, bool bStencil);
	void endRender();
	void init_render_state();
	void drawGeometry(sD3DStaticBuffer&	 buffer_dsc);

	// 材质系列
	void setMaterial(sMaterial& m);

	// 灯光系列
	void setLight(sLight& l, int index);
	void disableLight(int index);

	// 纹理系列
	void addTexture(TCHAR* name, int& texID);
	void setTextureFilter(int index, int filter, int type);
	void setMultiTexture();
	void setDetailMapping();
	void applyTexture(int index, int texID);
	void saveScreenShot();

	// 字体系列
	void createFont(TCHAR* name, int width, int height, int& ID, BOOL italic =FALSE);
	void displayText(int id, long x, long y, D3DCOLOR color, TCHAR* text,...);

	// X文件系列
	void loadXModel(TCHAR* file, int& xModelId);
	void GetXModelBoundingSphere(int xModelId, D3DXVECTOR3* origin, float* radius);
	void UpdateXAnimation(int xModelId, float time, D3DXMATRIX* mat);
	void RenderXModel(int xModelId);

//  	void createGUI(int& ID);
//  	void releaseAllGUI();
//  	bool addGUIBackDrop(int guiID, TCHAR* fileName);
//  	void addGUIStaticText(int guiID, int id, TCHAR* text,int x, int y,
//  		unsigned long color, int fontID);
//  	void addGUIButton(int guiId, int id, int x, int y,
//  		TCHAR* up, TCHAR* over, TCHAR* down);

	// 矩阵系列
	void setWorldMatrix(D3DXMATRIX* temp);
	void setViewMatrix(D3DXMATRIX* temp);
	void calculateProjMatrix(float fov, float n, float f);

	// 释放系列
	void ReleaseTex(int TexID);
	void ReleaseFont(int FontID);
	void ReleaseAllTex();
	void ReleaseAllFont();
	void shutDownALLXmodels();

	void shutDown();
	


private:
	void generate_matrial(D3DMATERIAL9& mtrl, sMaterial& m);

	void generate_direction_light(D3DLIGHT9& dir_light, D3DVECTOR&	dir, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

	void generate_point_light(D3DLIGHT9& point_light, D3DVECTOR& pos, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

	void generate_spot_ligth(D3DLIGHT9&	spot_light, D3DVECTOR&	pos, D3DVECTOR& dir, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

private:
	D3DCOLOR			m_clearColor;
	HWND				m_handle;
	BOOL				m_windowed;
	bool				m_rendering;
	int					m_windowWidth;
	int					m_windowHeight;
	IDirect3DDevice9*	m_pDevice;

	sTexture*			m_pTexList;
	int					m_totalTextures;

	LPD3DXFONT*			m_ppFont;
	int					m_totalFonts;

	CXModel*			m_pModel;
	int					m_totalModels;

//  	cGUIsystem*			m_pGUIList;
//  	int					m_totalGUIs;

	D3DXMATRIX			m_projection;
};


bool	createRender(cRender** _render);

#endif


