
#include "engine_load_x.h"

/*=======================*===============================
FUC:加载静态X文件
IN:	
OUT:
负面:
返回:
========================================================*/
void	load_x_file(IDirect3DDevice9* device, //[in]
					TCHAR*	x_file_name,	//[in]
					ID3DXMesh*&	pMesh,		//[in,out] 
					ID3DXPMesh*& pPmesh,	//[in,out]
					DWORD& num_material,	//[in,out]
					D3DMATERIAL9*&	pD3D_Material, //[in,out]
					IDirect3DTexture9**& ppTexture) //[in,out]
{
	ID3DXBuffer*	pAdja_buff,*pMaterial_buff;

	// 得到X文件的材质指针,材质个数,网格
	D3DXLoadMeshFromX(x_file_name,D3DXMESH_MANAGED,device,&pAdja_buff, &pMaterial_buff, \
		NULL,&num_material,&pMesh);
	
	// 材质数和纹理数是匹配的.
	// 开辟材质缓冲,纹理缓冲.
	D3DXMATERIAL*	pD3DX_Material =(D3DXMATERIAL*)pMaterial_buff->GetBufferPointer();	
	pD3D_Material = new	D3DMATERIAL9[num_material];
	ppTexture = new	LPDIRECT3DTEXTURE9[num_material];
	
	int hr;
	for(DWORD i=0;i<num_material;i++)
	{
		
		pD3D_Material[i] =pD3DX_Material[i].MatD3D;
		// 从X文件加载的材质没有AMBIENT;
		// 当我们有范围光源时,可注释掉.
		pD3D_Material[i].Ambient = pD3D_Material[i].Diffuse ;

		hr =D3DXCreateTextureFromFileA(device,pD3DX_Material[i].pTextureFilename,&ppTexture[i]);
		if(FAILED(hr))
		{
			ppTexture[i] =0;
		}
	}
	
	// 生成渐进网格
	D3DXGeneratePMesh(pMesh, (DWORD*)pAdja_buff->GetBufferPointer(), NULL, NULL, 1,
		D3DXMESHSIMP_FACE,&pPmesh);

	SAFE_RELEASE(pAdja_buff);
	SAFE_RELEASE(pMaterial_buff);

}