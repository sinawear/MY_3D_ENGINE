

#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dsound.lib")
#pragma comment(lib,"dxguid.lib") 

// #pragma comment(lib,"d3dx9dt.lib")
// #pragma comment(lib,"d3dxof.lib")
// #pragma comment(lib,"comctl32.lib ")

#ifndef	my
#define my

#include "engine_init.h"
#include "engine_fvf.h"
#include "engine_color.h"
#include "engine_font.h"
#include "engine_terrain.h"
#include "engine_material.h"
#include "engine_disk_io.h"
#include "engine_wav.h"
#include "engine_sprite.h"
#include "bound.h"
#include "engine_pick.h"
#include "engine_load_x.h"
#include "engine_light.h"
#include "engine_billBoard.h"
#include "engine_gui.h"
#include "window_main.h"

// for X animotion
#include "XStructures.h"
#include "XMesh.h"

// for input
#include "input_check.h"

#include "camera.h"

#include "Render.h"
//
//---------------------------------
// 数据结构区


//
//-----------------------------------------------------
//函数区

inline DWORD FtoDw(float val) { return *((DWORD*)&val); }


#endif