#pragma once

#include "engine_base.h"
#include "engine_terrain.h"
#include "cVertexDecl.h"
#include "cHeightMap.h"
#include "bound.h"


//////////////////////////////////////////////////////////////////////////
// 基础地板，用于给出个参照物
class cGroundV1
{
public:
	// 1这里由于，修改麻烦的原因没有用指针传递地形结构。
	// 2构造函数写成（cVertexDecl* pVertexDecl, Terrain t）多好
	cGroundV1(cVertexDecl* pVertexDecl, const Terrain& t):_Terrain(t)
	{
		_pVertexDecl =pVertexDecl;
		
	}
	
	~cGroundV1()
	{
		for(vector<SubMeshAndBox>::iterator i =_SubGrid.begin();
		i != _SubGrid.end(); i++)
		{
			SAFE_RELEASE(i->mesh);
		}
	}
		
	//--
	// 初始化整个网格
	void	init_light_tex_ground_sub_mesh(
		cHeightMap* pHeightMap, D3DXVECTOR3 Center =Zero);
	
	// 渲染子网格
	void	draw_light_tex_sub_mesh_ground();
	
	// 给出位置得到所处的高度
	float	get_light_texGround_sub_mesh_Y(cHeightMap* pHeightMap,
		float Row, float Col);
	
protected:
	// 将网格分解。
	void	sub_mesh(RECT& Rect, vector<LightTextureVertexV1>& GobalVertex
					   );
	
private:
	cVertexDecl* _pVertexDecl;
	const Terrain& _Terrain; // 地形的结构体描述

	vector<LightTextureVertexV1> _GroundData; //整个地形数据

	vector<SubMeshAndBox> _SubGrid; // 被分解后的地形数据

	
};



