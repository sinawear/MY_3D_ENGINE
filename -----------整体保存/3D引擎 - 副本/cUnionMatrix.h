#pragma once

#include "engine_base.h"

// 该类意义不大。

#define UNION_MATRIX 512
class cUnionMatrix 
{
public:
	// 有没有效率问题呢。
	cUnionMatrix()
	{
		for(int i =0; i<UNION_MATRIX; i++)
		{
			_List[i] =I_MATRIX;
		}

		_Index =0;
	}

	D3DXMATRIX& getMatrix()
	{	
		return _List[_Index++];
	}

protected:
	D3DXMATRIX _List[UNION_MATRIX];
	int _Index;
};