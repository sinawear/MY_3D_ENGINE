
// Author: Frank Luna (C) All Rights Reserved

#ifndef __cubeH__
#define __cubeH__

#include "engine_base.h"

class cCube
{
public:
	cCube(IDirect3DDevice9* device);
	~cCube();

	bool draw(D3DXMATRIX* world, D3DMATERIAL9* mtrl, IDirect3DTexture9* tex);
private:
	IDirect3DDevice9*       _device;
	IDirect3DVertexBuffer9* _vb;
	IDirect3DIndexBuffer9*  _ib;
};
#endif //__cubeH__