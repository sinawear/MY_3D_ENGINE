#pragma once

#include "engine_base.h"

#define MAX_NUM_VERTEX_BUFF 1024
class cVertexBuffers
{
public:
	cVertexBuffers(IDirect3DDevice9* pDevice)
	{
		_pDevice =pDevice;
		_TotalVertexBuff =0;
	}

	~cVertexBuffers()
	{
		release_Vertex_buffer_array();
	}

	IDirect3DVertexBuffer9* get_vertex_Buffer(DWORD ID)
	{
		return _pVertexBuff[ID];
	}

	DWORD create_vertex_buff(DWORD Length, DWORD FVF, D3DPOOL Pool);


protected:
	void release_Vertex_buffer_array();

private:
	IDirect3DDevice9* _pDevice;
	IDirect3DVertexBuffer9* _pVertexBuff[MAX_NUM_VERTEX_BUFF];
	DWORD _TotalVertexBuff; //���㻺�������
};


