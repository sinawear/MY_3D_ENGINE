#include "cHeightMap.h"





//////////////////////////////////////////////////////////////////////////


cHeightMap::cHeightMap(int NumRow, int NumCol, string FileName, 
		   float HeightScal, float HeightOffset)
{
	m_bIsLoad =false;
	
	load_row(NumRow, NumCol, FileName, HeightScal, HeightOffset);
	
	m_bIsLoad =true;
}

void cHeightMap::load_row(int NumRow, int NumCol, const string FileName, 
		float HeightScal, float HeightOffset)
	{
		if(m_bIsLoad == true)
			MyMsgBox(L" 高度图已经被加载。", true);
///*1 读取文件 c方法
		FILE* pFile;
		vector<UCHAR> FileMemory(NumRow * NumCol);
		pFile =fopen(FileName.c_str(), "rb");
		int ReadByte =fread((void*)&FileMemory[0], sizeof(UCHAR), NumRow*NumCol, pFile);
		fclose(pFile);
//1*/

/*2 读取文件 c++方法
		// 高度图文件和内存
		ifstream FileRead;
		vector<UCHAR> FileMemory(NumRow * NumCol);
		
// 打开文件读取到内存
		FileRead.open(FileName.c_str(), ios::binary);
			if(FileRead.is_open())
			{
				FileRead.read((char*)&FileMemory[0], FileMemory.size());
			//	FileRead>>(UCHAR*)&FileMemory[0]; 它是不是只能用于字符模式？
				// 不然读取的高度图怎么，那么，奇怪，肯定做手脚了。
				// 而却，也中看不中用，不知道读取多少呀。
			}
// 关闭文件
		FileRead.close();
	2*/
		
		// 调整高度图大小
		m_HeightMap.resize(NumRow, NumCol);

		for(int i =0; i< NumRow; i++) //行数
		{
			for(int j =0; j< m_HeightMap.getVertexesPerRow(); j++) //每行多少顶点
			{
				float t =FileMemory[i*m_HeightMap.getVertexesPerRow()+j] * HeightScal + HeightOffset;
				m_HeightMap(i, j) = t;
			}
		}

		// 平滑地图的高度值
		sampleHeight3X3();
				
}