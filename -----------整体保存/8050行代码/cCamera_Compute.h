#pragma once

#include "engine_base.h"

class cCamera_Compute
{
public:
	cCamera_Compute(){}
	~cCamera_Compute(){}

	D3DXMATRIX& compute_view_matrix(D3DXVECTOR3 Eye, D3DXVECTOR3 LookAt =Zero, 
		D3DXVECTOR3 Up =D3DXVECTOR3(0,1,0))
	{
		D3DXMatrixLookAtLH(&m_ViewMatrix, &Eye, &LookAt, &Up);
		return m_ViewMatrix;
	}

protected:
	D3DXMATRIX m_ViewMatrix;
};