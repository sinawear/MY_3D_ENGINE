// by sinawear [12/28/2011 O.O]
#pragma once
#include "engine_base.h"


template<class TYPE>
class cTable
{
public:
	cTable(){}

	// 重置向量的大小
	void resize(int NumRow, int NumCol)
	{
		m_NumRow =NumRow;
		m_VertexesPerRow =NumCol;
		
		m_Table.resize(m_VertexesPerRow * m_NumRow);
	}

	// 重置模仿D9的读取方法
	TYPE& operator()(int i, int j)
	{
		return m_Table[i*m_VertexesPerRow + j];
	}

	// 得到行数
	int getNumRow()
	{
		return m_NumRow;
	}

	// 得到每行多少顶点
	int getVertexesPerRow()
	{
		return m_VertexesPerRow;
	}

protected:
	vector<TYPE> m_Table;

	int m_NumRow;
	int m_VertexesPerRow; // 每行多少顶点
};



class cHeightMap
{
public:
	cHeightMap(){}
	
	//
	cHeightMap(int NumRow, int NumCol, string FileName, 
		float HeightScal, float HeightOffset);


	void load_row(int NumRow, int NumCol, const string FileName, 
		float HeightScal, float HeightOffset);


	// 得到行号，列号
	float getHeight(int i, int j)
	{	
		static float h;
		
		if(isInBounds(i,j))
			h =(float)m_HeightMap(i, j);
		return  h;
	}

	
protected:
	// 索引值是否有效
	bool isInBounds(int i, int j)
	{
		if(
			(j<0 || j > m_HeightMap.getVertexesPerRow()-1) || 
			(i<0 || i > m_HeightMap.getNumRow()-1)
			)
			return false;
		else
			return true;
	}

	// 过滤器主函数
	void sampleHeight3X3()
	{		
		int NumRow =m_HeightMap.getNumRow();
		int VertexesPerRow =m_HeightMap.getVertexesPerRow();

		// 生成一个暂存容器。这个省不掉。
		cTable<float> Temp;
		Temp.resize(NumRow, VertexesPerRow);

		for(int i =0; i<NumRow; i++)
		{
			for(int j =0; j<VertexesPerRow; j++)
			{
				Temp(i, j) =filter3X3(i, j);
			}
		}

		m_HeightMap =Temp;
	}

	// 过滤器单一实现
	float filter3X3(int i, int j)
	{
		// (i-1, j-1) (i-1, j) (i-1, j+1)
		// (i, j-1) (i , j) (i , j+1)
		// (i+1, j-1) (i+1, j) (i+1, j+1)
		float Sum =0;
		float Count =0;
		for(int k =-1; k<=1; k++)
		{
			for(int z =-1; z<=1; z++)
			{
				if(isInBounds(i+k, j+z))
				{
					Sum +=m_HeightMap(i+k, j+z);
					Count +=1.0f;

				}
			}
		}
		
		return Sum/Count;

	}

protected:
	bool m_bIsLoad;
	cTable<float> m_HeightMap;

};

