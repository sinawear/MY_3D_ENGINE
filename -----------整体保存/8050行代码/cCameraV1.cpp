
#include "cCameraV1.h"
//#include "cTerrainV1.h"

cCameraUVNV1::cCameraUVNV1(camera_type type)
{
	m_type =type;
}

cCameraUVNV1::~cCameraUVNV1()
{

}

void	cCameraUVNV1::reset()
{
	init();
}

void	cCameraUVNV1::init(int width , int height ,
						 D3DXVECTOR3 pos , D3DXVECTOR3 lookat, 
						 D3DXVECTOR3 up, float fov, float zn, float zf)
{

	m_width =width;
	m_height =height;

	m_pos =pos;
	m_lookat =lookat;
	
//	m_deep = m_lookat - m_pos; 摄像机会直冲面向地形（像，低着头）
	m_deep = m_lookat;
	m_right =D3DXVECTOR3(1.0f, 0, 0);

	m_up =up;
	m_fov =fov;

	m_zn =zn;
	m_zf =zf;

	compute();

	// 生成投影变换矩阵
	D3DXMatrixPerspectiveFovLH(&m_projMatrix, m_fov, (float)m_width/m_height, m_zn, m_zf);

	// viewport translation 窗口变换
	D3DVIEWPORT9  viewport={0,0,m_width, m_height, 0, 1};
	m_viewport =viewport;

//--
	m_Speed =0.07f;
}

void	cCameraUVNV1::compute()
{
/*1D9文档给的。
	N = normal(At - Eye) //z
	U = normal(cross(Up, N)) //x 
	V = cross(N, U) //y
		
		U.x           V.x           N.x          0
		U.y           V.y           N.y          0
		U.z           V.z           N.z          0
		-dot(U, eye)  -dot(V, eye)  -dot(N, eye)  1
1*/

// 根据文档整理一遍
	D3DXVECTOR3& U =m_right;
	D3DXVECTOR3& V =m_up;
	D3DXVECTOR3& N =m_deep;
	
	D3DXVec3Normalize(&N, &N); //单位化Z
	
	D3DXVec3Cross(&U, &D3DXVECTOR3(0,1,0), &N);
	D3DXVec3Normalize(&U, &U); // 单位化X
	
	D3DXVec3Cross(&V, &N, &U);
//	D3DXVec3Normalize(&V, &V); //单位化Y


	D3DXMATRIX	transform_move(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		-m_pos.x, -m_pos.y,-m_pos.z, 1.0f);

	D3DXMATRIX	transform_rotation(
		U.x, V.x, N.x, 0.0f,
		U.y, V.y, N.y, 0.0f,
		U.z, V.z, N.z, 0.0f,
		0,    0,   0,  1.0f);

	// 生成视口变换矩阵
	m_viewMatrix =transform_move * transform_rotation; 

}

// 我将compute修改N次后，已妥善，V1不太需要了
void cCameraUVNV1::computeV1()
{
//	m_deep =m_lookat - m_pos;

	// 生成视口变换矩阵
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_pos, &m_lookat, &m_up);
}

D3DXMATRIX&	cCameraUVNV1::getViewMatrix()
{
//	compute();

	return m_viewMatrix;

}

D3DXMATRIX&	cCameraUVNV1::getProjMatrix()
{
	return m_projMatrix;
}
void	cCameraUVNV1::getViewPort(D3DVIEWPORT9& t)
{
	t =m_viewport;
}

void	cCameraUVNV1::rotate_x_axis(float angle)
{
	D3DXMATRIX	temp;

	D3DXMatrixRotationAxis(&temp, &m_right, angle); //生成绕X轴旋转矩阵

	D3DXVec3TransformCoord(&m_deep, &m_deep, &temp);
	D3DXVec3TransformCoord(&m_up, &m_up, &temp);
}

void	cCameraUVNV1::rotate_y_axis(float angle)
{
	D3DXMATRIX	temp;

	if(m_type == LAND) // 我认为这样写没必要,因为,LAND不会改变up向量(未证实)
		D3DXMatrixRotationY(&temp, angle);

	if(m_type == AIR)
		D3DXMatrixRotationAxis(&temp, &m_up, angle); //生成绕Y轴旋转矩阵

	//  函数名字好怪。
	//  应设计：D3DXVec3TransfromMatrix();哈哈
	//	m_deep =m_deep * temp; 向量是3列的，矩阵是4行的。乘不了。
	//	m_right =m_right * temp;

	//Transforms a 3-D vector by a given matrix, projecting the result back into w = 1.
	D3DXVec3TransformCoord(&m_deep, &m_deep, &temp);
	D3DXVec3TransformCoord(&m_right, &m_right, &temp);
}

void	cCameraUVNV1::rotate_z_axis(float angle)
{
	D3DXMATRIX	temp;
	D3DXMatrixRotationAxis(&temp, &m_deep, angle);  //生成绕Z轴旋转矩阵

	D3DXVec3TransformCoord(&m_right, &m_right, &temp);
	D3DXVec3TransformCoord(&m_up, &m_up, &temp);
}

void	cCameraUVNV1::walk_x_axis(float units)
{
	if(m_type == LAND)
		m_pos +=D3DXVECTOR3(m_right.x, 0, m_right.z) * units;	

	if(m_type == AIR)
		m_pos +=m_right * units;
}

void	cCameraUVNV1::walk_y_axis(float units)
{
	if(m_type == LAND)
			m_pos.y +=units; 

	if(m_type == AIR)
		m_pos +=m_up * units;
}

void	cCameraUVNV1::walk_z_axis(float units)
{
	if(m_type == LAND)
		m_pos +=D3DXVECTOR3(m_deep.x, 0, m_deep.z) * units;	

	if(m_type == AIR)
		m_pos +=m_deep * units;
}

void	cCameraUVNV1::getPos(D3DXVECTOR3& t)
{
	t =m_pos;
}

void	cCameraUVNV1::setPos(D3DXVECTOR3 t)
{
	m_pos =t;

	m_deep =m_lookat - m_pos; // 位置的变化都要导致m_deep重新计算
}

void	cCameraUVNV1::getDeep(D3DXVECTOR3& t)
{
	t =m_deep;
}

void	cCameraUVNV1::computeV2(cInputController* pInput, float delt_time,
							  cTerrainV1* pTerrain, float OffsetY)
{
///*	
	D3DXVECTOR3 Dir =Zero;

	if(pInput->isKeyDown(DIK_W))
		Dir +=m_deep;
	if(pInput->isKeyDown(DIK_A))
		Dir -=m_right;

	if(pInput->isKeyDown(DIK_S))
		Dir -=m_deep;
	if(pInput->isKeyDown(DIK_D))
		Dir +=m_right;

	D3DXVec3Normalize(&Dir, &Dir);

	// 改变摄像机位置
	D3DXVECTOR3  NewPos= m_pos + Dir * m_Speed * delt_time ;
	
	if(pTerrain != NULL)
	{
		// 得到摄像机所在位置的地形高度
		NewPos.y = pTerrain->getY(NewPos.x, NewPos.z) + OffsetY;

		// 得到切线
		D3DXVECTOR3 tangent =NewPos - m_pos;
		D3DXVec3Normalize(&tangent, &tangent);

		m_pos +=tangent * m_Speed * delt_time;

		m_pos.y = pTerrain->getY(m_pos.x, m_pos.z) + OffsetY; 
		
	}
	else
		m_pos =NewPos;


	float RotaY =D3DXToRadian(pInput->getMouseState(0))/5;
	float RotaX =D3DXToRadian(pInput->getMouseState(1))/5;

	rotate_x_axis(RotaX);
	rotate_y_axis(RotaY);
	
//*/	
	compute(); 

	makeFrustum(); 
}	


void	cCameraUVNV1::makeFrustum()
{
	D3DXMATRIX VP = m_viewMatrix * m_projMatrix;

	D3DXVECTOR4 col0(VP(0,0), VP(1,0), VP(2,0), VP(3,0));
	D3DXVECTOR4 col1(VP(0,1), VP(1,1), VP(2,1), VP(3,1));
	D3DXVECTOR4 col2(VP(0,2), VP(1,2), VP(2,2), VP(3,2));
	D3DXVECTOR4 col3(VP(0,3), VP(1,3), VP(2,3), VP(3,3));
	
	// Planes face inward.
	m_FrustumPlanes[0] = (D3DXPLANE)(col2);        // near
	m_FrustumPlanes[1] = (D3DXPLANE)(col3 - col2); // far
	m_FrustumPlanes[2] = (D3DXPLANE)(col3 + col0); // left
	
	m_FrustumPlanes[3] = (D3DXPLANE)(col3 - col0); // right
	m_FrustumPlanes[4] = (D3DXPLANE)(col3 - col1); // top
	m_FrustumPlanes[5] = (D3DXPLANE)(col3 + col1); // bottom
	
	for(int i = 0; i < 6; i++)
		D3DXPlaneNormalize(&m_FrustumPlanes[i],	&m_FrustumPlanes[i]);

}

bool	cCameraUVNV1::isVisible(const sBoundBox& box) const
{
	// Test assumes frustum planes face inward.
	
	// 我的模仿	
	//	   | /	
	//     |/  
	//-----/----------
	//    /     
	//   /
	
	
	D3DXVECTOR3 P;
	D3DXVECTOR3 Q;
	
	//      N  *Q                    *P
	//      | /                     /
	//      |/                     /
	// -----/----- Plane     -----/----- Plane    
	//     /                     / |
	//    /                     /  |
	//   *P                    *Q  N
	//
	// PQ forms diagonal most closely aligned with plane normal.
	
	// 遍历平截头的面，找出盒子的对角线（这里有4个对角线交叉在盒子中心）
	// 这些点有相同的方向像法线沿着各轴（-_-!)(即，对角线与面法线拥有相同的方向
	// 用于探测盒子在平面的前后
	// For each frustum plane, find the box diagonal(对角线) (there are four main
	// diagonals that intersect the box center point) that points in the
	// same direction as the normal along each axis (i.e., the diagonal 
	// that is most aligned with the plane normal).  Then test if the box
	// is in front of the plane or not.
	for(int i = 0; i < 6; ++i)
	{
		// 遍历x,y,z轴
		// For each coordinate axis x, y, z...
		for(int j = 0; j < 3; ++j)
		{
			// 使PQ拥有和面法线相同的方向
			// Make PQ point in the same direction as the plane normal on this axis.
			if( m_FrustumPlanes[i][j] >= 0.0f )
			{
				P[j] = box.min_pos[j];
				Q[j] = box.max_pos[j];
			}
			else 
			{
				P[j] = box.max_pos[j];
				Q[j] = box.min_pos[j];
			}
		}

		// 如果盒子在负半空间，它就在平面的后面，因此，完全在平截头的外面。
		// 因为PQ在同一个面法线方向上，我们可以推断出Q在外面的话，P也在外面
		// 所以我们仅仅需要测试Q。
		// If box is in negative half space, it is behind the plane, and thus, completely
		// outside the frustum.  Note that because PQ points roughly in the direction of the 
		// plane normal, we can deduce that if Q is outside then P is also outside--thus we
		// only need to test Q.
		static const float MY_ELLISPION =-5.0f;
		if( D3DXPlaneDotCoord(&m_FrustumPlanes[i], &Q) < MY_ELLISPION  ) // outside
			return false;
	}
	return true;
}