
#include "engine_c_fuc.h"

bool	MTF_isPointInsideSphere(sBoundSphere& boundSphere, D3DXVECTOR3& v)
{	
   // The distance between the two spheres.
   D3DXVECTOR3 intersect = boundSphere.center - v;
	
   // Test for collision.
   if(sqrt(intersect.x * intersect.x + intersect.y * intersect.y +
           intersect.z * intersect.z) < boundSphere.radius)
      return true;
   
   return false;	
}