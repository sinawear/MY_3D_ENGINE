#pragma once

#include "engine_base.h"
#include "engine_fvf.h"
#include "engine_color.h"
#include "bound.h"

//
//---------------------------------
// 数据结构区

// 地形
typedef struct Terrain 
{
	Terrain(){}

	// scale缩放Y，cell_size间距大小。
	// 要保证地形width,depth是2的倍数.
	// 如果Cell_size是1的话，v_per_row,v_per_col必须是2n+1;
	Terrain(int _NumRow,int _NumCol,float scaleY,float cell_size, 
		float HeightOffset =0)
	{
		init(_NumRow, _NumCol, scaleY, cell_size, HeightOffset);	
	}

	void init(int _NumRow,int _NumCol,float scale,float cell_size,
		float HeightOffset)
	{
		NumRow =_NumRow;
		NumCol =_NumCol;
			
		NumCellRow =NumRow-1;
		NumCellCol =NumCol-1;

		cell_space =cell_size;  // 只是换个名称
		num_vertex_per_row =NumCol;
		num_vertex_per_col =NumRow;
		
		num_cell_per_row =num_vertex_per_row - 1;
		num_cell_per_col =num_vertex_per_col - 1;
		width =num_cell_per_row * cell_space;
		depth =num_cell_per_col * cell_space;
		height_scale =scale;
		num_triangle = num_cell_per_row * num_cell_per_col *2;
		num_vertex = num_vertex_per_row * num_vertex_per_col;


		Height_Offset =HeightOffset;
	}

	int num_vertex_per_row, num_vertex_per_col;
	int num_cell_per_row, num_cell_per_col;
	float width, depth;
	float height_scale, cell_space;
	int  num_triangle, num_vertex;

	int NumRow; //顶点行数 
	int	NumCol; //顶点列数
	int NumCellRow; //单元行数
	int NumCellCol; //单元列数
	float Height_Offset; // 高度偏移量
}Terrain;


struct SubMeshAndBox
{
	ID3DXMesh* mesh;
	sBoundBox box;


	// For sorting.
	bool operator<(const SubMeshAndBox& rhs)const;

};

///////////////////////全局常量///////////////////////////////////////////////////
//_NumRow, _NumCol, scaleY, cell_size, HeightOffset
const Terrain g_Terrain(257,257,0.5, 2, 0);

//////////////////////////////////////////////////////////////////////////


//
//-----------------------------------------------------
//函数区

// 初始化地形
UINT*	init_terrain(IDirect3DDevice9* pDevice, //[in]
					 char*	file_name,
					 Terrain& terrain_map, 		//[in,out]
					 IDirect3DIndexBuffer9** vertexIndexBuff,	//[in,out] 
					 IDirect3DVertexBuffer9** vertexBuff ,		//[in,out]
					 IDirect3DTexture9**	tex);		//[in,out]
// 读取高度图
UCHAR*	terrain_read_heightmap(TCHAR* file_name,int num_terrain_vertex);

// 得到高度图数据
int		terrain_get_heightmap_value(Terrain& map1,UINT* heightmap,int i,int j);

// 设置高度图数据
void	terrain_set_heightmap_value(Terrain& map1,UINT* heightmap,int i,int j,int value);

// 过程化计算光照
float	terrain_compute_vertex_light(Terrain& map1,UINT* heightmap,int i,int j,D3DXVECTOR3 direction_light);

// 生成顶点缓冲索引
void	terrain_init_vertex_index(IDirect3DDevice9* pDevice, IDirect3DIndexBuffer9** ib, Terrain& map1);

// 给出x,z.得出高度
float	terrain_get_height(Terrain& map1, UINT* heightmap, float x, float z);

