#include "cGroundV1.h"

#include "cCameraV1.h"

// 子网格大小
// 文件型变量
static const int SUBMESH_NUMROW =33; //子网格行数
static const int SUBMESH_NUMCOL =33; //子网格列数
static Terrain s_SubTerrain(SUBMESH_NUMROW, SUBMESH_NUMCOL, 
							g_Terrain.height_scale,
							g_Terrain.cell_space, 
							g_Terrain.Height_Offset);


// 构造出整个网格，将数据放入到容器里.
void cGroundV1::init_light_tex_ground_sub_mesh(cHeightMap* pHeightMap, 
											   D3DXVECTOR3 Center /* =Zero */)
{

	int step =_Terrain.cell_space;
	
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;

	float nCoordIncSize =1.0f/(CellPerRow);
	float vCoordIncSize =1.0f/(CellPerCol);

	LightTextureVertexV1 V;
	vector<LightTextureVertexV1> GobalMeshVertex(NumVertex);

	float h =0;
	for(int z=start_z;z>=end_z;z-=step) //
	{
		float k=0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			V._p.x =x;
			V._p.y =pHeightMap->getHeight(h,k);
			V._p.z =z;
					
			V._n =D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			
			V._uv =D3DXVECTOR2(k*nCoordIncSize, h*vCoordIncSize);

			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&V._p, &V._p, &t);
			//--
			
			GobalMeshVertex[VertexCount++] =V;
	//		GobalMeshVertex.push_back(V);
			k++;
		}	
		h++;
	}


	// 整个网格可以分裂多少
	int NumSubRows =_Terrain.NumCellRow / s_SubTerrain.NumCellRow;
	int NumSubPerRow =_Terrain.num_cell_per_row / s_SubTerrain.num_cell_per_row;
	
	// 0，0，32，32；0，32，32，64
	// 先遍历每列，在遍历每行
	for(int i =0; i<NumSubRows; i++)
	{
		for(int j =0; j<NumSubPerRow; j++)
		{
			RECT r;
			r.left =i * s_SubTerrain.num_cell_per_row ;
			r.top =j*s_SubTerrain.num_cell_per_col;
			r.right =(i+1) * s_SubTerrain.num_cell_per_row;
			r.bottom =(j+1)*s_SubTerrain.num_cell_per_col;
			
			sub_mesh(r, GobalMeshVertex);
		}
	}

}

void cGroundV1::sub_mesh(RECT& Rect, 
						 vector<LightTextureVertexV1>& GobalVertex
						 )
{
	ID3DXMesh* pMesh;
	int re =D3DXCreateMesh(s_SubTerrain.num_triangle, s_SubTerrain.num_vertex, 
		D3DXMESH_MANAGED,
		_pVertexDecl->get_vertex_element(), _pVertexDecl->get_D9_device(),
		&pMesh);
	Failed_Msg_Box(NoSay, re);
	
	sBoundBox BoundBox;
	
	int VertexIndex =0;
	LightTextureVertexV1* pV;
	pMesh->LockVertexBuffer(NULL, (void**)&pV);
	//读取并填充顶点
	for(int i =Rect.top; i<=Rect.bottom; i++) // 循环33次
	{
		for(int j =Rect.left; j<=Rect.right; j++) // 循环33次
		{
			int Index = i*_Terrain.num_vertex_per_row + j;
			
			pV[VertexIndex++] = GobalVertex[Index];
			
		}
	}
	
	// 创建包围框 
	D3DXComputeBoundingBox(
		(D3DXVECTOR3*)pV, s_SubTerrain.num_vertex,
		sizeof(LightTextureVertexV1), &BoundBox.min_pos, &BoundBox.max_pos);
	
	pMesh->UnlockVertexBuffer();
	
	//--索引和属性
	WORD* pI;
	DWORD  index=0;
	DWORD  AttrIndex =0;
	DWORD* pAttr;
	pMesh->LockIndexBuffer(NULL, (void**)&pI);
	pMesh->LockAttributeBuffer(NULL, &pAttr);
	//注意这里，让我调试了好久
	//索引的操作是针对单元格的，
	//索引缓冲溢出了，也不报错
	for(int i =0; i<s_SubTerrain.NumCellRow; i++)
	{
		for(int j =0; j<s_SubTerrain.num_cell_per_row; j++)
		{
			int Size =s_SubTerrain.num_vertex_per_row;
			pI[index+0] = i * Size + j;
			pI[index+1] = i * Size  + j+1;
			pI[index+2] = (i+1) * Size  + j;
			
			pAttr[AttrIndex++] =0;
			
			// 第二个三角形
			pI[index+3] = (i+1) * Size  + j;
			pI[index+4] = i * Size  +j+1;
			pI[index+5] = (i+1) * Size  + j+1;
			
			pAttr[AttrIndex++] =0;
			
			index+=6;
		}
	}	
	pMesh->UnlockIndexBuffer();
	pMesh->UnlockAttributeBuffer();
	
	
	//--优化
	
	D3DXComputeNormals(pMesh, NULL);

	//  生成邻接信息
	DWORD TriangleNum =pMesh->GetNumFaces();
	DWORD*  pAdjacency =new DWORD[s_SubTerrain.num_triangle * 3];
	int r =pMesh->GenerateAdjacency(0.001f, pAdjacency); //epsilon 多近看成一个点
	
	// 优化
	r =pMesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE|D3DXMESHOPT_ATTRSORT, 
		pAdjacency, NULL, NULL, NULL);
	
	delete[] pAdjacency;	
	
	
	SubMeshAndBox SubGrid;
	SubGrid.mesh =pMesh;
	SubGrid.box =BoundBox;

	_SubGrid.push_back(SubGrid);
}

void cGroundV1::draw_light_tex_sub_mesh_ground()
{
/*
	for(vector<SubMeshAndBox>::iterator i = _SubGrid.begin(); 
	i !=_SubGrid.end(); i++)
	{
		(*i).mesh->DrawSubset(0);
	}
*/
	_RayTestMesh.clear(); //清空射线要测试的网格

	// 可以看见的地形网格数据
	list<SubMeshAndBox> VisibleSubGridList;
	for(vector<SubMeshAndBox>::iterator i =_SubGrid.begin();
	i != _SubGrid.end(); i++)
	{
		if(g_CameraV1.isVisible(i->box)) //第一次在引擎内使用全局对象 
		{
			VisibleSubGridList.push_back(*i);
			_RayTestMesh.push_back(i->mesh);
		}
	}

	// 排序
	VisibleSubGridList.sort();


	for(list<SubMeshAndBox>::iterator i = VisibleSubGridList.begin(); 
	i !=VisibleSubGridList.end(); i++)
	{
		(*i).mesh->DrawSubset(0);
	}
}


float cGroundV1::get_light_texGround_sub_mesh_Y(cHeightMap* pHeightMap, 
												float Row, float Col)
{
	const Terrain& map1 =_Terrain;
	
	float height;
	float x,z;
	x = map1.width/2.0f + Row;	// 
	z = map1.depth/2.0f - Col;
	
	x /= map1.cell_space;       // 列数
	z /= map1.cell_space;		// 行数
	
	float col_num =floorf(x);   // 得到数值的最小整数形式
	float row_num =floorf(z);
	
	float dx =x - col_num;	// 真是棒
	float dz =z - row_num;
	
	// Grab the heights of the cell we are in.
	// A*--*B
	//  | /|
	//  |/ |
	// C*--*D
	float A =pHeightMap->getHeight(row_num, col_num);
	float B =pHeightMap->getHeight(row_num, col_num+1);
	float C =pHeightMap->getHeight(row_num+1, col_num);
	float D =pHeightMap->getHeight(row_num+1, col_num+1);
	
	if(dz <1.0f -dx)   //在上半个三角形
	{
		float uy = B - A;
		float vy = C - A;
		
		height =A + uy*dx + vy *dz; 
	}
	else	//在下半个三角形
	{
		float uy = C - D;
		float vy = B - D;
		
		height = D + uy*(1.0f-dx) + vy*(1.0f-dz);
	}
	return height;	
}

