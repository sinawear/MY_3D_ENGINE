#pragma once

class cRenderStateProctect
{
public:
	cRenderStateProctect(IDirect3DDevice9* pDevice):_pDevice(pDevice){}

	void push()
	{
		_pDevice->GetRenderState(D3DRS_FILLMODE, &_t[0]);
		_pDevice->GetRenderState(D3DRS_SHADEMODE, &_t[1]);

	}

	void pop()
	{
		_pDevice->SetRenderState(D3DRS_FILLMODE, _t[0]);
		_pDevice->SetRenderState(D3DRS_SHADEMODE, _t[1]);
	}
private:
	DWORD _t[10];
	IDirect3DDevice9* _pDevice;
};

