
#ifndef _CAMERA_V1
#define _CAMERA_V1

#include "engine_base.h"
#include "input_check.h"
#include "Bound.h"
#include "cTerrainV1.h"

//class cTerrainV1; 也可以不用全局摄像机，用前导词处理
// 将cCameraUVNV1头文件引到cTerrainV1和cGourndV1中。不过
// 需要向cTerrainV1添加新的函数，和传参。

class cCameraUVNV1
{
public:
	enum	camera_type{LAND, AIR};
	
	cCameraUVNV1(camera_type type =LAND);
	~cCameraUVNV1();
	
	void	init(int width =800, int height =600, 
		D3DXVECTOR3 pos =D3DXVECTOR3(0,0,0), D3DXVECTOR3 lookat =D3DXVECTOR3(0,0,1), 
		D3DXVECTOR3 up =D3DXVECTOR3(0,1,0), 
		float fov =D3DX_PI/4, float zn =0.01f, float zf =5000.0f);
	
	void	compute();
	void	reset();
	
	void		rotate_x_axis(float angle);
	void		rotate_y_axis(float angle);
	void		rotate_z_axis(float angle);
	
	void		walk_x_axis(float units);
	void		walk_y_axis(float units);
	void		walk_z_axis(float units);
	
	D3DXMATRIX&	cCameraUVNV1::getViewMatrix();
	D3DXMATRIX&	cCameraUVNV1::getProjMatrix();
	void		getViewPort(D3DVIEWPORT9& t);
	void		setPos(D3DXVECTOR3 t);
	void		getPos(D3DXVECTOR3& t);
	
	void		getDeep(D3DXVECTOR3& t); 
	
	//--增强
	D3DXVECTOR3& get_pos(){
		return m_pos;
	}
	
	void	computeV1();
	
	void	setLookat(D3DXVECTOR3 t){
		m_lookat =t;
		m_deep =m_lookat - m_pos; //m_looat的改变，m_deep要重新计算
	}
	
	// OffsetY是摄像机高度偏移
	void	computeV2(cInputController* pInput, float delt_time,
		cTerrainV1* pTerrain, float OffsetY);
	
	void	setSpeed(float s){
		m_Speed =s;
	}
	
//--平截头
	void	makeFrustum();
	bool	isVisible(const sBoundBox& box) const;
private:
	camera_type	m_type;
	
	D3DXVECTOR3	m_pos,
		m_lookat,
		m_up,
		m_right,
		m_deep;
	
	D3DXMATRIX	m_viewMatrix,
		m_projMatrix;
	
	int			m_width,
		m_height;
	
	float		m_fov,
		m_zn,
		m_zf;
	
	D3DVIEWPORT9  m_viewport;
	
	float m_Speed;
	
//--
	D3DXPLANE m_FrustumPlanes[6];
};


#endif