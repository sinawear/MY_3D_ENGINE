#pragma once


// 根据D3D返回值，弹出错误代号和说明。
inline void Failed_Msg_BoxW(PCWSTR Text, HRESULT r)
{
#if _DEBUG
	if(SUCCEEDED(r))
		return;

	PCWSTR Caption =L"-_-!";
	wstring wstr;

	wstr +=L"错误函数：";
	wstr +=Text;
	wstr +=L"\n";
	wstr +=L"错误代码：";
	wstr +=DXGetErrorString9W(r);
	wstr +=L"\n";
	wstr +=L"错误说明：";
	wstr +=DXGetErrorDescription9W(r);

	::OutputDebugStringW(wstr.c_str());
	int re =MessageBoxW(NULL, wstr.c_str(), Caption, 0 );
	DWORD Error =::GetLastError();
	
	exit(0);

#endif
}

inline void Failed_Msg_BoxA(char* Text, HRESULT r)
{
#ifdef _DEBUG
	int MultiLen =strlen(Text) + 1;
	assert(MultiLen < 1024);
	//wchar_t* wText =new wchar_t[1024];..-_-?动态开辟不行呢。。
	//ZeroMemory(wText, MultiLen);
	wchar_t wText[1024] ={0};
	HRESULT t =MultiByteToWideChar(CP_ACP, NULL, Text, MultiLen, wText, sizeof(wText)/sizeof(wText[0]));
	
	Failed_Msg_BoxW(wText, r);

//	delete[] wText;
#endif
}

//////////////////////////////////////////////////////////////////////////
//fuc() 反正都要调用宽的字符，微软干嘛不这样写呢。
//{
//	变化
//fucW()
//}

//////////////////////////////////////////////////////////////////////////
