#include "engine_terrain.h"
#include "cCameraV1.h"

// 重载小于号，list容器提供了sort
// 作者重写了比较符号，作者，怎么知道是小于号，而不是大于号呢。。
// 哈哈
bool SubMeshAndBox::operator <(const SubMeshAndBox& rhs) const
	{
		D3DXVECTOR3 d1 = box.center() - g_CameraV1.get_pos();
		D3DXVECTOR3 d2 = rhs.box.center() - g_CameraV1.get_pos();
		
		return D3DXVec3LengthSq(&d1) < D3DXVec3LengthSq(&d2);
		
	}

//...........................................

/*
初始化地形
*/
UINT*	init_terrain(IDirect3DDevice9* pDevice, //[in]
					 char*	file_name,
					 Terrain& terrain_map, 		//[in,out]
					 IDirect3DIndexBuffer9** vertexIndexBuff,	//[in,out] 
					 IDirect3DVertexBuffer9** vertexBuff ,		//[in,out]
					 IDirect3DTexture9**	tex)		//[in,out]
{// 运用笛卡尔坐标系。
	// 加载高度图
	UCHAR* char_heightmap =terrain_read_heightmap(_T("terrain"),terrain_map.num_vertex);
	
	// ..
	UINT* int_heightmap = new UINT[terrain_map.num_vertex];
	
	// 把UCHAR地形的数值该成UINT
	for(int i=0;i<terrain_map.num_vertex;i++){
		int_heightmap[i] =(UINT)char_heightmap[i];
	}
	
	// --
	SAFE_DELETE(char_heightmap);
	
	// 地形乘上缩放尺度
	for(int i=0;i<terrain_map.num_vertex;i++){
		int_heightmap[i]=(UINT)(int_heightmap[i] * terrain_map.height_scale);
	}
	// 开辟顶点缓存。
	// 注意这里的顶点缓存，开辟是用顶点数乘FVF大小。
	//..
//	IDirect3DVertexBuffer9*	vb;
	pDevice->CreateVertexBuffer(terrain_map.num_vertex * sizeof(TextureVertex), 0 ,TEXTURE_FVF, \
		D3DPOOL_MANAGED ,vertexBuff, 0);
	
	// 设计变量。
	TextureVertex* v;
	int start_x = (int)-terrain_map.width/2;
	int start_z = (int)terrain_map.depth/2;
	int end_x	= (int)terrain_map.width/2;
	int end_z	= (int)-terrain_map.depth/2;

//	float uCoordIncrementSize = 1.0f / (float)terrain_map.num_cell_per_row;

	//.!!!!!!!!!!!!!!好大的错误!!!!!!!!!!!!!!!!!!!!下行vertex_per_col.....??
//	float vCoordIncrementSize = 1.0f / (float)terrain_map.num_vertex_per_col;
	int cell_space =(int)terrain_map.cell_space;
	
	// 高度图就是Y；
	// 把地图信息放到顶点缓存中。
	(*vertexBuff)->Lock(0,0, (void**)&v, 0);
	int i=0;
	int index;
	for(int z=start_z;z>=end_z;z-=cell_space)
	{
		int j=0;
		for(int x=start_x;x<=end_x;x+=cell_space)
		{
			// 高度图顶点索引。
			index =i * terrain_map.num_vertex_per_row + j;
			v[index] = TextureVertex((float)x, 0/*(float)int_heightmap[index]*/, (float)z, \
				i , j );	// 这里设计顶点的纹理坐标..概念上有点别扭,如3,0
			j++;
		}//列
		i++;
	}//行
	(*vertexBuff)->Unlock();
	
	// 创建顶点索引
	terrain_init_vertex_index(pDevice,vertexIndexBuff,terrain_map);
	

	D3DXCreateTextureFromFile(pDevice, file_name, tex);

// 纹理图要和高度图长宽匹配才能光照
//	pDevice->SetTexture(0, *tex);

// 	D3DLOCKED_RECT  lock_rect;
// 	D3DSURFACE_DESC	suface_desc;
// 	
// 	(*tex)->GetLevelDesc(0,&suface_desc); // 获得纹理的信息
// 	
// 	// 给纹理填充颜色
// 	(*tex)->LockRect(0,&lock_rect,0,0);  // LOCK操作纹理
// 	DWORD*	image_data = (DWORD*)lock_rect.pBits;
// 	for(UINT i=0;i<suface_desc.Height;i++)
// 	{
// 		for(UINT j=0;j<suface_desc.Width;j++) // y不能正常匹配。
// 		{
// 			//	int index =i * suface_desc.Width + j;
// 			// 得到高度
// 			float value =(float)terrain_get_heightmap_value(terrain_map,int_heightmap,i,j);
// 			D3DXCOLOR	c =image_data[i* lock_rect.Pitch/4+j];
// 
// 			// 以光照为分量调节颜色
// 			float sunshine =terrain_compute_vertex_light(terrain_map, int_heightmap,i,j,D3DXVECTOR3(0,1.0f,0));
// 			c*= sunshine;
// 			image_data[i* lock_rect.Pitch/4+j] =(D3DCOLOR)c; 
// 		}
// 	}

	return int_heightmap;
}


UCHAR*	terrain_read_heightmap(TCHAR* file_name,int num_terrain_vertex)
{// 读取高度图。
	FILE*	terrain_handle =_tfopen(file_name,_T("rb"));
	//...
	UCHAR*	char_heightmap =new UCHAR [num_terrain_vertex];
	fread(char_heightmap,sizeof(UCHAR),num_terrain_vertex,terrain_handle);
	fclose(terrain_handle);

	return char_heightmap;
}

int terrain_get_heightmap_value(Terrain& map1,UINT* heightmap,int i,int j)
{
	return heightmap[(map1.num_vertex_per_row * i) + j];
}

void	terrain_set_heightmap_value(Terrain& map1,UINT* heightmap,int i,int j,int value)
{
	heightmap[(map1.num_vertex_per_row * i) + j] =value;
}

float	terrain_compute_vertex_light(Terrain& map1,UINT* heightmap,int i,int j,D3DXVECTOR3 direction_light)
{
	float	A,B,C;
	A =(float)terrain_get_heightmap_value(map1,heightmap,i,j);
	B =(float)terrain_get_heightmap_value(map1,heightmap,i,j+1);
	C =(float)terrain_get_heightmap_value(map1,heightmap,i+1,j);

	D3DXVECTOR3	u =D3DXVECTOR3(map1.cell_space,B-A,0);
	D3DXVECTOR3 v =D3DXVECTOR3(0,C-A,-map1.cell_space);
	
	D3DXVECTOR3 n ;
	D3DXVec3Cross(&n,&u,&v);
	D3DXVec3Normalize(&n,&n);   // 归一化面法线
	// 归一化方向光。
	D3DXVec3Normalize(&direction_light,&direction_light);

	float shade_value =D3DXVec3Dot(&n,&direction_light);

	if(shade_value < 0)  // shade_value:[-1,1]
		shade_value =0;

	return shade_value;
}

/*
初始化顶点索引
*/
void	terrain_init_vertex_index(IDirect3DDevice9* pDevice, IDirect3DIndexBuffer9** ib, Terrain& map1)
{
	// 创建索引缓存
	pDevice->CreateIndexBuffer(map1.num_triangle * 3 * sizeof(WORD), \
		D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, \
		D3DPOOL_DEFAULT,ib,0);
	
	WORD*	v;
	int	index=0;
	(*ib)->Lock(0,0,(void**)&v,0);
	for(int i=0;i<map1.num_cell_per_col;i++)
	{
		for(int j=0;j<map1.num_cell_per_row;j++)
		{//单元格中第一个三角形
			v[index+0] = i * map1.num_vertex_per_row + j;
			v[index+1] = i * map1.num_vertex_per_row + j+1;
			v[index+2] = (i+1) * map1.num_vertex_per_row + j;
			
			// 第二个三角形
			v[index+3] = (i+1) * map1.num_vertex_per_row + j;
			v[index+4] = i * map1.num_vertex_per_row +j+1;
			v[index+5] = (i+1) * map1.num_vertex_per_row + j+1;
			
			index+=6;
		}
	}
	(*ib)->Unlock();
}

/*
给出在高位图的X,Z。算出Y;
*/
float	terrain_get_height(Terrain& map1, UINT* heightmap, float x, float z)
{
	float height;

	x = map1.width/2.0f + x;	// 
	z = map1.depth/2.0f - z;

	x /= map1.cell_space;       // 列数
	z /= map1.cell_space;		// 行数

	float col_num =floorf(x);   // 得到数值的最小整数形式
	float row_num =floorf(z);

	float dx =x - col_num;	// 真是棒
	float dz =z - row_num;

	float A =(float)terrain_get_heightmap_value(map1, heightmap, (int)row_num, (int)col_num);
	float B =(float)terrain_get_heightmap_value(map1, heightmap, (int)(row_num+1.0f), (int)col_num);
	float C =(float)terrain_get_heightmap_value(map1, heightmap, (int)row_num, (int)(col_num+1.0f));
	float D =(float)terrain_get_heightmap_value(map1, heightmap, (int)(row_num+1.0f),(int)(col_num+1.0f));

	if(dz <1.0f -dx)   //在上半个三角形
	{
		float uy = B - A;
		float vy = C - A;
		
		height =A + uy*dx + vy *dz; 
	}
	else	//在下半个三角形
	{
		float uy = C - D;
		float vy = B - D;

		height = D + uy*(1.0f-dx) + vy*(1.0f-dz);
	}
	return height;
}