/*
	文件如果存在，则追加写入。
*/
#include	"engine_disk_io.h"

/*=======================*===============================
FUC:输出字符串到磁盘.
IN:	要打开的文件名
OUT:
负面:
========================================================*/
void	print_string_to_disk(TCHAR* stream)
{
	if(NULL==*stream) return;

	FILE* fp;
	fp =_tfopen(_T("error.txt"),_T("r")); //只读

	if(NULL==fp)
	{
		fp =_tfopen(_T("error.txt"),_T("wt+"));  //打开建立读写.
		_ftprintf(fp,stream);
	}
	else
	{
		fp =_tfopen(_T("error.txt"),_T("at+")); //追加.
		_ftprintf(fp,stream);
	}

	fcloseall();
}