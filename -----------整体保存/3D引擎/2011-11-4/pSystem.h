// 2011-8-17 13：12
// 粒子的抽象基类，（这种形状又叫做：工厂模式）

#ifndef		_POINT_SYSTEM_
#define		_POINT_SYSTEM_


#include "my_engine.h"
#include "random_factory.h"

struct sAttribute
{
	sAttribute()
	{
		life_time = 0.0f;
		age      = 0.0f;
		is_alive  = true;
	}

	D3DXVECTOR3		position;  //位置
	D3DXVECTOR3		velocity;	//速度
	D3DXVECTOR3		acceleration; //加速度
	float			life_time;  //生命周期
	float			age;		//年龄
	D3DCOLOR		color;		//颜色
	D3DCOLOR		color_fade; //随时间减弱的颜色
	bool			is_alive;	// 是否存活
};

class cParticleSystem
{
public:
	cParticleSystem();
	virtual ~cParticleSystem();
	
	virtual	bool	init(IDirect3DDevice9* device, char* FileName);
	virtual void	reset();//重新设定所有粒子

	virtual void	resetParticles(sAttribute* attr) =0;
	virtual	void	addParticle();
	virtual	void	update(float EplasedTime) =0;

	virtual	void	preRender();
	virtual	void	render();
	virtual	void	postRender();

	bool	isEmpty();
	bool	isDead();//如果，系统中的所有粒子均死亡，返回true;

	void	shut_down();

protected:
	virtual	void	removeDeadParticles();


	IDirect3DDevice9*		_device;
	D3DXVECTOR3		_origin;	//粒子源
	cObjectBound*		_bound_box;
	float		_enitRate;//每秒粒子
	float		_size;//粒子大小 
	IDirect3DTexture9*	_tex;
	IDirect3DVertexBuffer9*		_vb;
	std::list<sAttribute>	_particles;//粒子列表
	DWORD		_max_particles;//最多粒子数
	DWORD	_vb_size, 
			_vb_offset,
			_vb_batch_size;


};

// 粒子枪
class cParticleGun :public cParticleSystem
{
public:
	cParticleGun(cCameraUVN*	camera);
	void	resetParticles(sAttribute* attr);
	void	update(float EplasedTime);

	void	preRender();
	void	postRender();
	
	std::list<sAttribute>&	getParticleList(); 


private:
	cCameraUVN*		_camera;
};



// 雪
class cParticleSnow :public cParticleSystem
{
public:
	cParticleSnow(cObjectBound*	bound_box, int numParticles);
	void	resetParticles(sAttribute* attr);
	void	update(float EplasedTime);

	void	preRender();
	void	postRender();


};

// 烟火
class  cParticleFirework :public cParticleSystem
{
public:
	cParticleFirework(D3DXVECTOR3* origin, int numParticles);
	void	resetParticles(sAttribute* attr);
	void	update(float EplasedTime);
	void	preRender();
	void	postRender();

};


#endif