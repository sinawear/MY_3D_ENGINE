
#ifndef	_ENGINE_WAV_
#define _ENGINE_WAV_
#include "engine_base.h"

/*
初始化音效
*/
void	init_sound(IDirectSound**	ppds, HWND	main_window_handle);

/*
加载音效,返回的是最原始的BUFFER，不知道IDirectSoundBuffer8
有什么新特性。。
*/
IDirectSoundBuffer*	 load_sound(IDirectSound* lpds,TCHAR* lpzFileName);

#endif