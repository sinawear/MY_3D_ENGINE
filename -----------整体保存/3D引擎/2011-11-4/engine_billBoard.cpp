
#include "engine_billBoard.h"

// 注意:因为要形成公告牌效果,则一直能看到,则用最初的视口矩阵,
// 绝不改变.加了视口矩阵的保护,可以放到任意地方.

/*=======================*===============================
FUC:画出准心
IN:	
OUT:
负面:要依靠最原始的视口矩阵
========================================================*/
void	draw_cross(IDirect3DDevice9*	device,		//[in]
				   ID3DXSprite*		pSprite,
				   IDirect3DTexture9* billBoardTexture,	//[in]
				   D3DXMATRIX& origin_view_matrix	//[in]
				   )
{// 用来设置一个矩形，贴个准星的箭头在上面。。

	IDirect3DVertexBuffer9* billBoardVertexBuff;
	D3DXMATRIX	tempMatrix;

	D3DXMATRIX faceUserMat,WorldMat,ViewMat;
	D3DXVECTOR3	Pos(0,80,0);
	D3DXMatrixScaling(&faceUserMat,1,-1,1);
	pSprite->SetTransform(&faceUserMat);
	//
	D3DXMatrixTranslation(&WorldMat,Pos.x,Pos.y,Pos.z);
//	D3DXMatrixRotationX(&WorldMat, 3.14f*0.25f);

	D3DXMATRIX	view;
	device->GetTransform(D3DTS_VIEW, &view);	
/*
	// 渲染状态
	device->SetRenderState(D3DRS_LIGHTING, false);
	device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	// enable per pixel alpha testing
	device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);

	// specifies a reference alpha value against which pixels are tested
	device->SetRenderState(D3DRS_ALPHAREF, 0x01);

	// Accept the new pixel if its value is greater than the value of the current pixel
	device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);    

*/
	pSprite->SetWorldViewLH(&WorldMat, &view);
	pSprite->Begin(D3DXSPRITE_BILLBOARD|D3DXSPRITE_ALPHABLEND);
	pSprite->Draw(billBoardTexture,NULL,NULL,NULL,0xffffffff);
	pSprite->End();
/*
	device->GetTransform(D3DTS_VIEW, &tempMatrix);

	device->SetTransform(D3DTS_VIEW, &origin_view_matrix);
	device->CreateVertexBuffer( 6 * sizeof(TextureVertex) , 0 , TEXTURE_FVF , D3DPOOL_DEFAULT , &billBoardVertexBuff, 0);
	
	TextureVertex*	pVertex;
	billBoardVertexBuff->Lock( 0 , sizeof(TextureVertex) *6 , (void**)&pVertex, 0);
//	定义6个顶点，画一个菱形。纹理正常取样。
	pVertex[0] =TextureVertex(-0.5f, 0.5f, 1.01f ,0.0f, 0.0f);
	pVertex[1] =TextureVertex(0.5f, 0.5f, 1.01f  ,1.0f, 0.0f);
	pVertex[2] =TextureVertex(-0.5f, 0.0f, 1.01f ,0.0f, 1.0f);
		
	pVertex[3] =TextureVertex(0.5f, 0.5f, 1.01f ,1.0f, 0.0f);
	pVertex[4] =TextureVertex(0.5f, 0.0f, 1.01f ,1.0f, 1.0f);
	pVertex[5] =TextureVertex(-0.5f, 0.0f, 1.01f ,0.0f, 1.0f);
	billBoardVertexBuff->Unlock();
*/

//	device->SetTransform(D3DTS_VIEW, &sprite_matrix);
/*
	device->SetTexture(0, billBoardTexture);
	device->SetFVF(TEXTURE_FVF);
	device->SetStreamSource(0, billBoardVertexBuff, 0, sizeof(TextureVertex));

	device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);
	// 关闭ALPHA通道
    device->SetRenderState(D3DRS_ALPHATESTENABLE, false);
	// 释放顶点缓存
	SAFE_RELEASE(billBoardVertexBuff);
	
	device->SetTransform(D3DTS_VIEW, &tempMatrix);
	*/
}

IDirect3DVertexBuffer9*	init_sky_box(IDirect3DDevice9* device //[in]
									 )
					 
{
	// 以后可能读入场景块,然后,从内存中载入位图.
	int hr;
	IDirect3DVertexBuffer9*	billBoardVertexBuff;
	// 定一下点.可恶的UV坐标.
	float	x =640.0f,z =640.0f;
	float	y =100.0f;
	float	num_vertex =20;
	// 	TextureVertex	A =TextureVertex(-x/2, y, z/2);
	// 	TextureVertex	B =TextureVertex(x/2, y, z/2 );
	// 	TextureVertex	C =TextureVertex(-x/2, y, -z/2);
	// 	TextureVertex	D =TextureVertex(x/2, y, -z/2);

	// 	TextureVertex	E =TextureVertex(-x/2, 0, z/2);
	// 	TextureVertex	F =TextureVertex(x/2, 0, z/2);
	// 	TextureVertex	G =TextureVertex(-x/2, 0, -z/2);
	// 	TextureVertex	H =TextureVertex(x/2, 0, -z/2);


	device->CreateVertexBuffer( num_vertex * sizeof(TextureVertex) , 0 , TEXTURE_FVF , D3DPOOL_DEFAULT , &billBoardVertexBuff, 0);

	TextureVertex*	pVertex;
	billBoardVertexBuff->Lock( 0 , sizeof(TextureVertex) *num_vertex , (void**)&pVertex, 0);

	//z
	//^
	//|
	//|
	//|
	//@--------------->x
	// front
	pVertex[0] =TextureVertex(-x/2, y, z/2, 0 ,0); // A
	pVertex[1] =TextureVertex(x/2, y, z/2 ,3, 0); // B
	pVertex[2] =TextureVertex(-x/2, 0, z/2, 0,2); // E
	pVertex[3] =TextureVertex(x/2, 0, z/2, 3,2); // F

	// right
	pVertex[4] =TextureVertex(x/2, y, z/2 ,0, 0);  // B
	pVertex[5] =TextureVertex(x/2, y, -z/2, 3,0);  // D
	pVertex[6] =TextureVertex(x/2, 0, z/2, 0,2);  // F
	pVertex[7] =TextureVertex(x/2, 0, -z/2, 3,2); // H

	// back [CDGH 这样贴图的正面将看不到]
	// DCHG
	pVertex[8] =TextureVertex(x/2, y, -z/2, 0, 0);
	pVertex[9] =TextureVertex(-x/2, y, -z/2, 3, 0); //C
	pVertex[10] =TextureVertex(x/2, 0, -z/2, 0, 2);
	pVertex[11] =TextureVertex(-x/2, 0, -z/2, 3, 2); //G

	// left
	// CAGE
	pVertex[12] =TextureVertex(-x/2, y, -z/2, 0, 0);
	pVertex[13] =TextureVertex(-x/2, y, z/2, 3, 0);  //A
	pVertex[14] =TextureVertex(-x/2, 0, -z/2, 0, 2);
	pVertex[15] =TextureVertex(-x/2, 0, z/2, 3, 2);  //E

	// up
	// ABCD [这样贴图,看到的是图片的背面]
	// BADC 
	pVertex[16] =TextureVertex(x/2, y, z/2 ,0, 0);  // B
	pVertex[17] =TextureVertex(-x/2, y, z/2, 1, 0);  //A
	pVertex[18] =TextureVertex(x/2, y, -z/2, 0, 1);  // D
	pVertex[19] =TextureVertex(-x/2, y, -z/2, 1, 1); //C

	// EFGH

	billBoardVertexBuff->Unlock();

	return billBoardVertexBuff;
}
/*=======================*===============================
FUC:画出天空盒
IN:	纹理指针数组
OUT:
负面:
预期:未来会,重写.运用X文件直接加载.
========================================================*/
void	draw_sky_box(IDirect3DDevice9* device, //[in]
					 IDirect3DTexture9** skyBoxTexture,	//[in]
					 IDirect3DVertexBuffer9*	billBoardVertexBuff //[in]
					 )
{
	
	device->SetFVF(TEXTURE_FVF);
	device->SetStreamSource(0, billBoardVertexBuff, 0, sizeof(TextureVertex));
	
	for(int i =0 ;i<5;i++)
	{
		device->SetTexture(0, skyBoxTexture[i]);
		
		device->DrawPrimitive(D3DPT_TRIANGLESTRIP, i*4, 2);
	}
	
	// 释放顶点缓存
//	SAFE_RELEASE(billBoardVertexBuff);
}
