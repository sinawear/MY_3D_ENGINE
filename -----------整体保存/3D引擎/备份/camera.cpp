
#include "camera.h"

cCameraUVN::cCameraUVN(camera_type type)
{
	m_type =type;
}

cCameraUVN::~cCameraUVN()
{

}

void	cCameraUVN::init(int width , int height ,
						 D3DXVECTOR3 pos , D3DXVECTOR3 lookat, 
						 D3DXVECTOR3 up, float fov, float zn, float zf)
{

	m_width =width;
	m_height =height;

	m_pos =pos;
	m_lookat =lookat;
	m_deep = m_lookat - m_pos;
	m_right =D3DXVECTOR3(1.0f, 0, 0);

	m_up =up;
	m_fov =fov;

	m_zn =zn;
	m_zf =zf;

	compute();

	// 生成投影变换矩阵
	D3DXMatrixPerspectiveFovLH(&m_projMatrix, m_fov, (float)m_width/m_height, m_zn, m_zf);

	// viewport translation
	D3DVIEWPORT9  viewport={0,0,m_width, m_height, 0, 1};
	m_viewport =viewport;
}

void	cCameraUVN::compute()
{
	D3DXVECTOR3& U =m_right;
	D3DXVECTOR3& V =m_up;
	D3DXVECTOR3& N =m_deep;

	D3DXVec3Normalize(&N, &N);

	D3DXVec3Cross(&V, &N, &U);
	D3DXVec3Normalize(&V, &V);

	D3DXVec3Cross(&U, &V, &N);
	D3DXVec3Normalize(&U, &U);

//--原来的初始化顺序。会出现有问题，有待验证是否看LAMOTHE这样写的。
// 那本书是不能看的。花费不少时间在上面，气愤。
// 	D3DXVec3Normalize(&N, &N);
// 
// 	D3DXVec3Cross(&U, &V, &N);
// 	D3DXVec3Normalize(&U, &U);
// 	
// 	D3DXVec3Cross(&V, &N, &U);
// 	D3DXVec3Normalize(&V, &V);

	D3DXMATRIX	transform_move(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		-m_pos.x, -m_pos.y,-m_pos.z, 1.0f);

	D3DXMATRIX	transform_rotation(
		U.x, V.x, N.x, 0.0f,
		U.y, V.y, N.y, 0.0f,
		U.z, V.z, N.z, 0.0f,
		0,    0,   0,  1.0f);

	// 生成视口变换矩阵
	m_viewMatrix =transform_move * transform_rotation; 

/*1
	HWND	hwnd =GetFocus();
	HDC		hdc =GetWindowDC(hwnd);

	char	buff[80];
	sprintf_s(buff, 80, "POS:%f,%f,%f ", m_pos.x, m_pos.y, m_pos.z);
	::TextOut(hdc, 0, 0, buff, strlen(buff));  

	sprintf_s(buff, 80, "LOOKAT:%f,%f,%f ", m_lookat.x, m_lookat.y, m_lookat.z);
	::TextOut(hdc, 0, 20, buff, strlen(buff));

	sprintf_s(buff, 80, "UP:%f,%f,%f ", m_up.x, m_up.y, m_up.z);
	::TextOut(hdc, 0, 40, buff, strlen(buff)); 
1*/
}

D3DXMATRIX&	cCameraUVN::getViewMatrix()
{
//	compute();

	return m_viewMatrix;

}

D3DXMATRIX&	cCameraUVN::getProjMatrix()
{
	return m_projMatrix;
}
void	cCameraUVN::getViewPort(D3DVIEWPORT9& t)
{
	t =m_viewport;
}

void	cCameraUVN::rotate_x_axis(float angle)
{
	D3DXMATRIX	temp;

	D3DXMatrixRotationAxis(&temp, &m_right, angle); //生成绕X轴旋转矩阵

	D3DXVec3TransformCoord(&m_deep, &m_deep, &temp);
	D3DXVec3TransformCoord(&m_up, &m_up, &temp);
}

void	cCameraUVN::rotate_y_axis(float angle)
{
	D3DXMATRIX	temp;

	if(m_type == LAND) // 我认为这样写没必要,因为,LAND不会改变up向量(未证实)
		D3DXMatrixRotationY(&temp, angle);

	if(m_type == AIR)
		D3DXMatrixRotationAxis(&temp, &m_up, angle); //生成绕X轴旋转矩阵

	//  函数名字好怪。
	//  应设计：D3DXVec3TransfromMatrix();哈哈
	//	m_deep =m_deep * temp; 向量是3列的，矩阵是4行的。乘不了。
	//	m_right =m_right * temp;

	//Transforms a 3-D vector by a given matrix, projecting the result back into w = 1.
	D3DXVec3TransformCoord(&m_deep, &m_deep, &temp);
	D3DXVec3TransformCoord(&m_right, &m_right, &temp);
}

void	cCameraUVN::rotate_z_axis(float angle)
{
	D3DXMATRIX	temp;
	D3DXMatrixRotationAxis(&temp, &m_deep, angle);  //生成绕Z轴旋转矩阵

	D3DXVec3TransformCoord(&m_right, &m_right, &temp);
	D3DXVec3TransformCoord(&m_up, &m_up, &temp);
}

void	cCameraUVN::walk_x_axis(float units)
{
	if(m_type == LAND)
		m_pos +=D3DXVECTOR3(m_right.x, 0, m_right.z) * units;	

	if(m_type == AIR)
		m_pos +=m_right * units;
}

void	cCameraUVN::walk_y_axis(float units)
{
	if(m_type == LAND)
			m_pos.y +=units; 

	if(m_type == AIR)
		m_pos +=m_up * units;
}

void	cCameraUVN::walk_z_axis(float units)
{
	if(m_type == LAND)
		m_pos +=D3DXVECTOR3(m_deep.x, 0, m_deep.z) * units;	

	if(m_type == AIR)
		m_pos +=m_deep * units;
}

void	cCameraUVN::getPos(D3DXVECTOR3& t)
{
	t =m_pos;
}

void	cCameraUVN::setPos(D3DXVECTOR3 t)
{
	m_pos =t;
}

void	cCameraUVN::getDeep(D3DXVECTOR3& t)
{
	t =m_deep;
}