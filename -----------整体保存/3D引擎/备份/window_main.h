
#ifndef _WINDOW_MAIN_
#define _WINDOW_MAIN_

#include "engine_base.h"

class cWindow
{
public:
	cWindow(HINSTANCE instance);
	~cWindow();

	// 如果ExternWindowProc为空，则自动使用内部窗口过程
	void		initWindow(int	windth, int height, WNDPROC ExternWindowProc);
	void		messageLoop(MSG&	msg, void (*fuc)()); //消息循环
	void		shutDownWindow();//释放窗口

	HWND		getWindowHandle();
	HINSTANCE	getWindowInstance();

protected:
	// 窗口过程
	static LRESULT CALLBACK InsideWindowProc(HWND hwnd,UINT uMsg,WPARAM wParam,LPARAM lParam);							 
	
private:
	HWND		m_handle;//窗口句柄
	HINSTANCE	m_instance;//程序实例
	int			m_width;
	int			m_height;
	WNDCLASSEX	m_wc;
};


#endif