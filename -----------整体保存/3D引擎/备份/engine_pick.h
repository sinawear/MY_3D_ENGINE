
#ifndef	_RAY_
#define _RAY_

#include "engine_base.h"
//#include "engine_bound.h"
#include "bound.h"

//  射线
typedef	struct Ray 
{
	D3DXVECTOR3	origin;
	D3DXVECTOR3	direction;
}Ray;

// 判断射线是否与球相交
bool	is_ray_cross_bound_sphere(IDirect3DDevice9* device,
						   sBoundSphere& bound_sphere, POINT& point);


#endif
