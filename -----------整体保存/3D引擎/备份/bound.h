
#ifndef	_BOUND_
#define _BOUND_

#include "engine_base.h"

// 包围球
struct sBoundSphere 
{
	D3DXVECTOR3	center;
	float		radius;
};

// 包围盒
struct  sBoundBox
{
	D3DXVECTOR3		min_pos;
	D3DXVECTOR3		max_pos;
	
};

// cObjectBound构造函数还是要写成指针的形式，在内部对空指针做
// 相应操作
class cObjectBound
{
public:
	// 设置是可选的，可以只设置盒子，或球
	cObjectBound(D3DXVECTOR3& min, D3DXVECTOR3& max, D3DXVECTOR3& center, float radius);
	cObjectBound(sBoundBox* box, sBoundSphere* sphere);
	cObjectBound();
	~cObjectBound();

	// 创建包围盒
	void	generate_bound_box( ID3DXMesh* pMeshObject);//生成网格的包围盒	
	void	generate_bound_box(D3DXVECTOR3* pointList, UINT numPoints);//根据顶点列表生成包围盒

	// 创建包围球
	void	generate_bound_sphere( ID3DXMesh* pMeshObject);//生成网格的包围球	

	// 生成包围的网格,可以用于显示的输出网格的样子
	void	bound_box_mesh(IDirect3DDevice9* pDevice, ID3DXMesh** ppBoundBoxMesh);
	void	bound_shpere_mesh(IDirect3DDevice9* pDevice, ID3DXMesh** ppBoundSphereMesh);

	// 设置包围体
	void	set_bound_box(sBoundBox& box);
	void	set_bound_sphere(sBoundSphere&	sphere);

	// 得到包围体
	sBoundBox*	get_bound_box();
	sBoundSphere*	get_bound_sphere();

	// 点是否在包围体内
	bool	isPointInsideBox(D3DXVECTOR3& v);
	bool	isPointInsideSphere(D3DXVECTOR3& v);

	void	shut_down();

	// 边界框碰撞检测
	bool	isCollision(sBoundBox& A);

	// 边界球碰撞检测
	bool	isCollision(sBoundSphere A, sBoundSphere B);


private:
	sBoundBox			m_boundBox;
	sBoundSphere		m_boundSphere;

};

// 生成包围盒的网格,用于显示的输出网格的样子
//void	bound_box_mesh(IDirect3DDevice9* pDevice, sBoundBox& boundBox, ID3DXMesh** ppBoundBoxMesh);


// 生成包围球的网格,用于显示的输出网格的样子
//void	bound_shpere_mesh(IDirect3DDevice9* pDevice, sBoundSphere& boundSphere, ID3DXMesh** ppBoundSphereMesh);

#endif
