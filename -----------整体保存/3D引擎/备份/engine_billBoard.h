#pragma once

#include "engine_base.h"
#include "engine_fvf.h"


// 画准心 
void	draw_cross(IDirect3DDevice9*	device,		//[in]
				   ID3DXSprite*		pSprite,
				   IDirect3DTexture9* billBoardTexture,	//[in]
				   D3DXMATRIX& origin_view_matrix	//[in]
				   );
// 初始化天空盒
IDirect3DVertexBuffer9*	init_sky_box(IDirect3DDevice9* device //[in]
									 );
// 画天空盒
void	draw_sky_box(IDirect3DDevice9* device, //[in]
					 IDirect3DTexture9** skyBoxTexture,	//[in]
					 IDirect3DVertexBuffer9*	billBoardVertexBuff //[in]
					 );