
#include "system_log.h"

void	cSystemLog::clearLog(TCHAR*	fileName)
{
	m_file.open(fileName, ios::out);
	write( _T(""), NULL);
	m_file.close();
}

// 可变参数不能动态开辟内存呀..
void	cSystemLog::writeLog(TCHAR* fileName, TCHAR* message,...)
{
	va_list	va;
	
	va_start(va, message);
	
	m_file.open(fileName, ios::out);
	
	write(message, va);
	
	m_file.close();
	
	va_end(va);
}

// 单行的BUFF是 1024.
void	cSystemLog::write(TCHAR* message, va_list start)
{
	TCHAR	buff[1024];
	
	_vstprintf(buff, message, start);
	
	m_file<<buff;
}

void	cSystemLog::appendLog(TCHAR* fileName, TCHAR* message,...)
{
	va_list	va;
	
	va_start(va, message);
	
	m_file.open(fileName,  ios::app );	//不要这样ios::out || ios::app
	
	write(message, va);
	
	m_file.close();
	
	va_end(va);
}


void	cSystemLog::writeMatrix(TCHAR* fileName, D3DXMATRIX&	t)
{
	appendLog(fileName, _T("%f,%f,%f,%f\n"), t._11, t._12, t._13, t._14);
	appendLog(fileName, _T("%f,%f,%f,%f\n"), t._21, t._22, t._23, t._24);
	appendLog(fileName, _T("%f,%f,%f,%f\n"), t._31, t._32, t._33, t._34);
	appendLog(fileName, _T("%f,%f,%f,%f\n"), t._41, t._42, t._43, t._44);
}
