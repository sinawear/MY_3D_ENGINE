
#pragma once

#include <stdio.h>
#include <windows.h>
#include <math.h>

class cHighTimer
{
public:
	cHighTimer()
	{
		m_int64StartCounts = m_int64CurrentCounts =0;

		m_FPS =0;

		// 得到高精度计时器频率
		if(QueryPerformanceFrequency((LARGE_INTEGER*)&m_int64Frequency))
			;
		else
			MessageBox(NULL, "CPU不支持高精度计时器", "!", 0);
	}

	void start_counter()
	{
		init_point1();
	}

	// 高精度计数值之差× 1秒/频率（一次多少秒） =用时; 两帧之间的用时.
	// 返回时间单位：毫秒	
	float get_elapsed_time()
	{
		init_point2();
		float temp = (
			(float)(m_int64CurrentCounts - m_int64StartCounts) / m_int64Frequency)*1000.0f;
	
		update_point();

		return temp;
	}

	// 接受逝去的时间
	float get_fps(float elapsed)
	{
		static DWORD FrameCounter =0;
		static float Second =0;
		
		FrameCounter++; //帧计数增加
		Second +=elapsed; //用时增加
		if( Second > 1000.0f)
		{
			m_FPS =FrameCounter;// 得到FPS
			FrameCounter =0;
			Second =0;
		}

		return m_FPS;
	}

protected:
	void init_point1()
	{
		//使用高精度计时器，得到第一个计数
		QueryPerformanceCounter((LARGE_INTEGER*)&m_int64StartCounts); 
	}

	void init_point2()
	{
		// 得到第二个计数
		QueryPerformanceCounter((LARGE_INTEGER*)&m_int64CurrentCounts); 
	}

	// 更新并交换数据
	void update_point()
	{
		m_int64StartCounts =m_int64CurrentCounts;
	}
private:
	LONGLONG			m_int64Frequency; //频率
	LONGLONG			m_int64StartCounts;// 开始计数值
	LONGLONG			m_int64CurrentCounts; //当前计数值

	DWORD		m_FPS;
};