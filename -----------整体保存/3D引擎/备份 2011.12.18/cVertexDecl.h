#pragma once
#include "engine_base.h"

class cVertexDecl
{
public:
	cVertexDecl(IDirect3DDevice9* pDevice)
	{	
		_pDevice =pDevice;

	}
		
	void create(DWORD FVF)
	{
		D3DXDeclaratorFromFVF(FVF, _Declaration);
		
		// ���������ӿ�
		int r =_pDevice->CreateVertexDeclaration(_Declaration, &_pVertexDecl);
		
	}

	void apply()
	{
		_pDevice->SetVertexDeclaration(_pVertexDecl);
	}

	~cVertexDecl()
	{
		SAFE_RELEASE(_pVertexDecl);
	}

protected:
	IDirect3DDevice9* _pDevice;

	D3DVERTEXELEMENT9 _Declaration[MAX_FVF_DECL_SIZE];
	IDirect3DVertexDeclaration9* _pVertexDecl;
};