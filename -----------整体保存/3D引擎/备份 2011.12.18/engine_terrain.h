#pragma once

#include "engine_base.h"
#include "engine_fvf.h"
#include "engine_color.h"

//
//---------------------------------
// 数据结构区

// 地形
typedef struct Terrain 
{
	Terrain(){}

	// scale缩放Y，cell_size间距大小。
	Terrain(int v_per_row,int v_per_col,float scale,float cell_size)
	{
		cell_space =cell_size;  // 只是换个名称
		num_vertex_per_row =v_per_row;
		num_vertex_per_col =v_per_col;

		num_cell_per_row =num_vertex_per_row - 1;
		num_cell_per_col =num_vertex_per_col - 1;
		width =num_cell_per_row * cell_space;
		depth =num_cell_per_col * cell_space;
		height_scale =scale;
		num_triangle = num_cell_per_row * num_cell_per_col *2;
		num_vertex = num_vertex_per_row * num_vertex_per_col;

	}

	int num_vertex_per_row, num_vertex_per_col;
	int num_cell_per_row, num_cell_per_col;
	float width, depth;
	float height_scale, cell_space;
	int  num_triangle, num_vertex;

}Terrain;

//
//-----------------------------------------------------
//函数区

// 初始化地形
UINT*	init_terrain(IDirect3DDevice9* pDevice, //[in]
					 char*	file_name,
					 Terrain& terrain_map, 		//[in,out]
					 IDirect3DIndexBuffer9** vertexIndexBuff,	//[in,out] 
					 IDirect3DVertexBuffer9** vertexBuff ,		//[in,out]
					 IDirect3DTexture9**	tex);		//[in,out]
// 读取高度图
UCHAR*	terrain_read_heightmap(TCHAR* file_name,int num_terrain_vertex);

// 得到高度图数据
int		terrain_get_heightmap_value(Terrain& map1,UINT* heightmap,int i,int j);

// 设置高度图数据
void	terrain_set_heightmap_value(Terrain& map1,UINT* heightmap,int i,int j,int value);

// 过程化计算光照
float	terrain_compute_vertex_light(Terrain& map1,UINT* heightmap,int i,int j,D3DXVECTOR3 direction_light);

// 生成顶点缓冲索引
void	terrain_init_vertex_index(IDirect3DDevice9* pDevice, IDirect3DIndexBuffer9** ib, Terrain& map1);

// 给出x,z.得出高度
float	terrain_get_height(Terrain& map1, UINT* heightmap, float x, float z);

