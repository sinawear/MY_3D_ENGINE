

#pragma once

#include "engine_base.h"



// 生成方向光源
void	generate_direction_light(D3DLIGHT9&	dir_light, D3DVECTOR&	dir, 
								 D3DXCOLOR&	color);

// 生成点光源
void	generate_point_light(D3DLIGHT9& point_light, D3DVECTOR& pos,
							 D3DXCOLOR& color);

// 生成聚光灯光源
void	generate_spot_ligth(D3DLIGHT9&	spot_light, D3DVECTOR&	pos,
							D3DVECTOR& dir, D3DXCOLOR& color);
