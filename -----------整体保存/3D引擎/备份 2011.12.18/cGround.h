#pragma once
#include "engine_base.h"
#include "engine_terrain.h"

//////////////////////////////////////////////////////////////////////////
// 基础地板，用于给出个参照物
class cGround
{
public:
	// 这里由于，修改麻烦的原因没有用指针传递地形结构。
	cGround(IDirect3DDevice9* pDevice, Terrain t)
	{
		_pDevice =pDevice;
		_Terrain =t;
	}
	
	~cGround()
	{
		SAFE_RELEASE(_pIndexBuffer);
		SAFE_RELEASE(_pVertexBuffer);
	}
	
	void	init_ground(D3DXVECTOR3 Center);

	// 0默认线框输出，1可以输出点阵，2可以SOLID输出
	void	draw_ground(int i);
	
private:
	IDirect3DDevice9* _pDevice;
	Terrain _Terrain;
	IDirect3DVertexBuffer9* _pVertexBuffer;
	IDirect3DIndexBuffer9* _pIndexBuffer;

};

