
#include "cGround.h"


// 初始化地板
void cGround::init_ground(D3DXVECTOR3 Center)
{
	int step =_Terrain.cell_space;
	
	int Color =0;
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;
	
	// 用D9的方法会产生多余的线，从最底端连到了另列的最高端。
	// 不知道，为什么OpenGL不会产生多余的线？
	// 不过即使，D9可以，但，顶点缓冲里会有冗余顶点的。
	ColorVertex* pV;
	_pDevice->CreateVertexBuffer(sizeof(ColorVertex)*NumVertex, NULL, COLOR_FVF, D3DPOOL_DEFAULT,
		&_pVertexBuffer, NULL);		
	_pVertexBuffer->Lock(0, sizeof(ColorVertex)*NumVertex, (void**)&pV, NULL);
	
	
	int h =1; //设计h,k生成黑白交叉的颜色
	for(int z=start_z;z>=end_z;z-=step) //
	{
		int k =0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			if( k % 2 || h%2)
				Color =128;
			else
				Color =0;
			
			pV[VertexCount] =ColorVertex(D3DXVECTOR3(x, 0, z), 
				D3DCOLOR_XRGB(Color,Color,Color));

		//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount].pos, &pV[VertexCount].pos, &t);
		//--

			VertexCount++;
			k++;
		}
		h++;		
	}
	_pVertexBuffer->Unlock();
	
	DWORD* pI;
	DWORD  index=0;
	// 创建索引缓冲,用索引缓冲的冗余换顶点缓冲的冗余。
	_pDevice->CreateIndexBuffer(sizeof(DWORD)*NumTriangle*3, NULL, D3DFMT_INDEX32, 
		D3DPOOL_DEFAULT, &_pIndexBuffer, NULL);
	_pIndexBuffer->Lock(0, sizeof(DWORD)*NumTriangle*3, (void**)&pI, NULL);
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			index+=6;
		}
	}
	_pIndexBuffer->Unlock();
}

// 0默认线框输出，1可以输出点阵，2可以SOLID输出
void cGround::draw_ground(int i)
{

	DWORD t;
	_pDevice->GetRenderState(D3DRS_FILLMODE, &t);
	_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	
	// 设计点大小
//	_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);
//	_pDevice->SetRenderState(D3DRS_POINTSIZE, FtoDw(0.01));

	if(i == 2)
		_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

//。。
	D3DVERTEXELEMENT9 Declaration[MAX_FVF_DECL_SIZE];

	IDirect3DVertexDeclaration9* pVertexDecl;
	D3DXDeclaratorFromFVF(COLOR_FVF, Declaration);
	
	// 顶点声明接口
	int r =_pDevice->CreateVertexDeclaration(Declaration, &pVertexDecl);

	r =_pDevice->SetVertexDeclaration(pVertexDecl);
//	_pDevice->SetFVF(COLOR_FVF);
	_pDevice->SetStreamSource(0, _pVertexBuffer, 0, sizeof(ColorVertex));
	_pDevice->SetIndices(_pIndexBuffer);
	SAFE_RELEASE(pVertexDecl);

	if(i == 1)// 输出点阵
		_pDevice->DrawPrimitive(D3DPT_POINTLIST, 0, _Terrain.num_vertex);
	else
		_pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 
			_Terrain.num_vertex, 0, _Terrain.num_triangle);


	// 还原状态
//	_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
	_pDevice->SetRenderState(D3DRS_FILLMODE, t);
}