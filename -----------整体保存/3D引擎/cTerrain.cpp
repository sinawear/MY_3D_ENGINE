#include "cTerrain.h"




void cTerrain::init(cRender* pRender, Terrain terrain,
				   string HeightMap, string StoneTex, 
				   string GrassTex, string DirtTex,
				   string BlendMap, float HeightScal, float HeightOffset)
{
	
	IDirect3DDevice9* pDevice =pRender->getDevice9();
	m_Terrain =terrain;
	
	// 生成顶点的格式
	m_pVertexDecl =new cVertexDecl(pDevice);
	m_pVertexDecl->create(LIGHT_TEXTURE_FVF_V1, sizeof(LightTextureVertexV1));
	
	
	// 加载纹理
	int StoneImageID =pRender->addTexture((char*)StoneTex.c_str());
	int GrassImageID =pRender->addTexture((char*)GrassTex.c_str());
	int GroundImageID =pRender->addTexture((char*)DirtTex.c_str());
	int BlendImageID =pRender->addTexture((char*)BlendMap.c_str());
	
	// 加载特效
	m_pFx =new cFx(pDevice);
	m_pFx->load_fx("Terrain.fx");
	
	// 设置纹理
	m_pFx->set_texture("gTexGround", pRender->getTexture(GroundImageID));
	m_pFx->set_texture("gTexGrass", pRender->getTexture(GrassImageID));
	m_pFx->set_texture("gTexStone", pRender->getTexture(StoneImageID));
	m_pFx->set_texture("gBlendMap", pRender->getTexture(BlendImageID));
	
	
	// 加载高度图
	m_HeightMapObject.load_row(terrain.NumRow, terrain.NumCol, 
		HeightMap, HeightScal, HeightOffset);
	
/*
	cSystemLog Log;
	for(int i =0; i<257; i++)
	{
		for(int j =0; j<257; j++)
		{
			float t =m_HeightMapObject.getHeight(i,j);
			Log.appendLog("经过优化器的高度图数据", "%f\n", t);
		}
	}
*/
	
	// 初始化地形
	m_pGround =new cGround(pDevice, m_Terrain);
//	m_pGround->init_ligth_tex_ground_mesh(m_pVertexDecl, &m_HeightMapObject);
	
	m_pGround->init_light_tex_ground_sub_mesh(m_pVertexDecl, &m_HeightMapObject);	
	
}

cTerrain::~cTerrain()
{
	SAFE_DELETE(m_pFx);
	SAFE_DELETE(m_pGround);
	SAFE_DELETE(m_pVertexDecl);
}

void cTerrain::draw(D3DXMATRIX& world, D3DXMATRIX& view, D3DXMATRIX& proj, 
					D3DXVECTOR3 LightDir, float AmbientEssentially)
{
	// 更新shader变量
	m_pFx->modify_matrix("g_WorldViewProjMatrix", world*view*proj);
	m_pFx->modify_value("g_LightDir", &LightDir, sizeof(D3DXVECTOR3));
	m_pFx->modify_float("g_AmbientEssentially", AmbientEssentially);
	
	// 成员函数的指针传递
	m_pFx->apply("Main", this, &cTerrain::shader_draw);
}


void cTerrain::shader_draw()
{
//	m_pGround->draw_light_tex_mesh_Ground();
	m_pGround->draw_light_tex_sub_mesh_ground();
}

