
//以后估计要把窗口类与创建窗口分开.
//通过CONTORLLER来提供简单的窗口

// 应提供窗口标题栏的文字，修改。
#include "window_main.h"


cWindow::cWindow(HINSTANCE instance)
{
	m_instance =instance;
}

cWindow::~cWindow()
{
	shutDownWindow();
}

void	cWindow::initWindow(int	width, int height, WNDPROC ExternWindowProc)
{

	
	int		x =0, y=0;  // 窗口的屏幕坐标

	int temp_x =GetSystemMetrics(SM_CXSCREEN);
	int temp_y =GetSystemMetrics(SM_CYSCREEN);

	//800/2-800/2 =0
	//1600/2-800/2 =400
	x =temp_x/2 - width/2 ;
	y =temp_y/2 - height/2 ;

	m_width =width;
	m_height =height;

	m_wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	m_wc.hIcon =	LoadIcon(NULL, IDI_INFORMATION);
	m_wc.hIconSm = LoadIcon(NULL, IDI_INFORMATION);
	m_wc.hInstance = m_instance;
	if(ExternWindowProc)
		m_wc.lpfnWndProc = ExternWindowProc;
	else
		m_wc.lpfnWndProc = cWindow::InsideWindowProc;

	m_wc.cbSize = sizeof(WNDCLASSEX );
	m_wc.lpszClassName = _T("sinawear");
	m_wc.lpszMenuName = NULL;
	m_wc.hCursor = LoadCursor(NULL,IDC_ARROW); 
	m_wc.style  = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	m_wc.cbWndExtra =0;
	m_wc.cbClsExtra =0;

	RegisterClassEx(&m_wc);

	m_handle =CreateWindowEx(NULL, _T("sinawear"), _T("今天打老虎"),\
//		WS_OVERLAPPEDWINDOW ,
		WS_POPUP,
		x , y, m_width, m_height, NULL ,NULL, m_instance, NULL);

// 保持客户区为指定大小
	RECT window_rect = {0,0,m_width-1, m_height-1};
	
	// make the call to adjust window_rect
	AdjustWindowRectEx(&window_rect,
		GetWindowStyle(m_handle),
		GetMenu(m_handle) != NULL,
		GetWindowExStyle(m_handle));
		
	// now resize the window with a call to MoveWindow()
	MoveWindow(m_handle,
		x, // x position
		y, // y position
		window_rect.right - window_rect.left, // width
		window_rect.bottom - window_rect.top, // height
		TRUE);
	
	// show the window, so there's no garbage on first render
	ShowWindow(m_handle, SW_SHOW);
}

void	cWindow::messageLoop(MSG& msg, void (*fuc)())
{
	while(msg.message!=WM_QUIT)
	{
		/// 消息队列中有消息时，调用相应的处理过程
		if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else
			/// 如果没有需要处理的消息，调用Render()函数
			fuc();
	}
	
}

void	cWindow::shutDownWindow()
{
	UnregisterClass(m_wc.lpszClassName, m_instance);
}

HWND	cWindow::getWindowHandle()
{
	return m_handle;
}

HINSTANCE	cWindow::getWindowInstance()
{
	return m_instance;
}

LRESULT CALLBACK cWindow::InsideWindowProc(HWND hwnd,UINT uMsg,WPARAM wParam,LPARAM lParam)							 
{
	switch(uMsg)
	{	
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
		
	}
	return DefWindowProc (hwnd, uMsg, wParam, lParam) ;
}