#ifndef _ENGINE_GUI_
#define _ENGINE_GUI_


#include "engine_base.h"
#include "engine_fvf.h"

// 鼠标按钮状态
#define BUTTON_UP      1
#define BUTTON_OVER    2
#define BUTTON_DOWN    3

//控件类型
const	DWORD	CONTROL_BACK =1;
const	DWORD	CONTROL_TEXT =2;
const	DWORD	CONTROL_BUTTON =3;


struct GUIcontrol 
{
	int	type;	//类型
	int ID;		//索引
	DWORD	color;	
	int		m_listID;	//队列索引
	int	x, y;			//坐标
	int	width, heigh;  // 控件宽高
	TCHAR*	text;	//输出文本(对于字体控件)

	//4种纹理
	IDirect3DTexture9	*back, *up, *down, *over;
};

class cGUIsystem
{
public:
	cGUIsystem();
	cGUIsystem(IDirect3DDevice9* _device, int _width, int _heigh);
	~cGUIsystem();

	void	setGUIsystem(IDirect3DDevice9* _device, int _width, int _heigh);
	bool	add_background_control(int ID, TCHAR* texName); //添加背景控件
	bool	create_font(TCHAR*	name, int& fontID, int width=12, int height=25);
	bool	add_text_control(int ID, int fontID, TCHAR* text, int x, int y, DWORD color);
	bool	add_button_control(int ID, TCHAR* up, TCHAR* down, TCHAR* over, int x, int y);

	ID3DXFont*	get_font(int listID);
	GUIcontrol*	get_control(int	listID);
	IDirect3DVertexBuffer9* get_vertex_buff(int listID);
	int	get_total_buff();
	int	get_total_control();
	int	get_total_font();
	void get_width_heigh(int& width, int& height);
	IDirect3DDevice9*	get_device();
	void	shut_down();

	// 背景控件专用
	GUIcontrol*	get_back_control();	//得到背景控件
	IDirect3DVertexBuffer9*	get_back_control_vb();	//得到背景控件顶点缓存
	bool	isUseBackControl();	//是否用背景控件

private:
	IDirect3DDevice9*	m_pDevice;
	ID3DXFont**		m_ppFont;
	GUIcontrol*		m_pControl;
	IDirect3DVertexBuffer9**	m_ppVb;

	bool	m_useBackground;
	int		m_totalBuff;	// 背景控件不算在总控件中
	int		m_totalFont;	
	int		m_totalControl;	// 背景控件不算在总控件中
	int		m_width, m_heigh;	//窗口的宽高

	// 背景控件专用
	GUIcontrol	m_BackControl;
	IDirect3DVertexBuffer9*	m_pBackVb;
};

//
//GUI系统过程处理
void	process_GUI(cGUIsystem*	gui,
					bool	LButtonDown,
					POINT	mousePoint,
					void (*fun)(int ID, int state));



#endif