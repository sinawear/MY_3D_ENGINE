
#include "system_log.h"

void	cSystemLog::clearLog(char*	fileName)
{
	m_file.open(fileName, ios::out);
	write( "", NULL);
	m_file.close();
}

// 可变参数不能动态开辟内存呀..
void	cSystemLog::writeLog(char* fileName, char* message,...)
{
	va_list	va;
	
	va_start(va, message);
	
	m_file.open(fileName, ios::out);
	
	write(message, va);
	
	m_file.close();
	
	va_end(va);
}

// 单行的BUFF是 1024.
void	cSystemLog::write(char* message, va_list start)
{
	char	buff[1024];
	vsprintf(buff, message, start);
	
	m_file<<buff;
}

void	cSystemLog::appendLog(char* fileName, char* message,...)
{
	va_list	va;
	
	va_start(va, message);
	
	m_file.open(fileName,  ios::app );	//不要这样ios::out || ios::app
	
	write(message, va);
	
	m_file.close();
	
	va_end(va);
}


void	cSystemLog::writeMatrix(char* fileName, D3DXMATRIX&	t)
{/*
	appendLog(fileName, "%f,%f,%f,%f\n", t._11, t._12, t._13, t._14);
	appendLog(fileName, "%f,%f,%f,%f\n", t._21, t._22, t._23, t._24);
	appendLog(fileName, "%f,%f,%f,%f\n", t._31, t._32, t._33, t._34);
	appendLog(fileName, "%f,%f,%f,%f\n", t._41, t._42, t._43, t._44);

  */
	float* p =t;
	appendLog(fileName, "%f,%f,%f,%f\n", p[0], p[1], p[2], p[3]);
	appendLog(fileName, "%f,%f,%f,%f\n", p[4], p[5], p[6], p[7]);
	appendLog(fileName, "%f,%f,%f,%f\n", p[8], p[9], p[10], p[11]);
	appendLog(fileName, "%f,%f,%f,%f\n", p[12], p[13], p[14], p[15]);
}


void	cSystemLog::writeVector(char* fileName, D3DXVECTOR3& t)
{
	appendLog(fileName, "%f,%f,%f\n", t.x, t.y, t.z);
}