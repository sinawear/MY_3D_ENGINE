// by sinawear [12/29/2011 O.O]


#pragma once

#include "NoXRender.h"
#include "cHeightMap.h"
#include "cFx.h"
#include "cGround.h"

// 本来想像以前一样，用函数指针，来处理shader_draw()
// 结果，发现只能用static来突破，可是呀，如果shader_draw()是静态的
// 意味着整个类的成员函数都要是静态，因为，shader_draw使用了成员变量
// 这可不是我喜欢的。不到万不得已，绝不怎样用。
// 看了Bjarne书，发现还是有办法的。

class cTerrain
{
public:
	cTerrain(){}

	void init(cRender* pRender, Terrain terrain,
		string HeightMap, 
		string StoneTex, string GrassTex, string DirtTex,
		string BlendMap,
		float HeightScal, float HeightOffset);

	~cTerrain();

	void draw(D3DXMATRIX& world, D3DXMATRIX& view, D3DXMATRIX& proj,
					D3DXVECTOR3 LightDir =D3DXVECTOR3(0,1,0), 
					float AmbientEssentially =1.0f);
	
	// 给出x,z;得到高度y
	float getY(float x, float z){
		return m_pGround->get_light_texGround_sub_mesh_Y(
			&m_HeightMapObject, x, z);
	}

protected:	
	void shader_draw();

protected:
	cHeightMap	m_HeightMapObject;
	cVertexDecl* m_pVertexDecl;
	cFx* m_pFx;
	cGround* m_pGround;

public:
	Terrain m_Terrain; //地形结构体

};

