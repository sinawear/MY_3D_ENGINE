#pragma once


#include "comm_base.h"


class cError
{
public:
	cError(char* Text)
	{
		m_Str =string(Text);
	}

	const char* getData()
	{
		return m_Str.data();
	}

	void print()
	{
		cout<<m_Str<<endl;
	}

protected:
	string m_Str;
};

