#pragma once
#include "engine_base.h"
#include "engine_terrain.h"
#include "cVertexDecl.h"
#include "cHeightMap.h"
#include "bound.h"
//////////////////////////////////////////////////////////////////////////
// 基础地板，用于给出个参照物
class cGround
{
public:
	// 1这里由于，修改麻烦的原因没有用指针传递地形结构。
	// 2构造函数写成（cVertexDecl* pVertexDecl, Terrain t）多好
	cGround(IDirect3DDevice9* pDevice, const Terrain& t):_Terrain(t)
	{
		_pDevice =pDevice;	

		_pVertexBuffer =NULL;
		_pIndexBuffer =NULL;

		_pMesh =NULL;
	}
	
	~cGround()
	{
		SAFE_RELEASE(_pIndexBuffer);
		SAFE_RELEASE(_pVertexBuffer);

		// 释放掉整个网格
		SAFE_RELEASE(_pMesh);

		// 释放掉各子网格
		for(vector<ID3DXMesh*>::iterator i = _pSubMesh.begin(); i !=_pSubMesh.end();
		i++)
		{
			SAFE_RELEASE((*i));
		}		
	}
	
	// Center可以调节地形位置
	void	init_ground(D3DXVECTOR3 Center =Zero); // 兼容存在

	// 0默认线框输出，1可以输出点阵，2可以SOLID输出 ！T&L下可用
	void	draw_ground(int i); 
	
//--更新加强

	// 该函数抛弃旧版内容,虽然可以改写原函数
	// 但是，因为，要背历史包袱，就写个新的呃。
	void	draw_ground( cVertexDecl* pVertexDecl);

	void	init_color_ground(D3DXVECTOR3 Center =Zero, 
		D3DXCOLOR Color1 =GRAY, D3DXCOLOR Color2 =BLACK);

	void	init_light_ground(D3DXVECTOR3 Center =Zero);

	// 纹理坐标在[0,+infinite]
	void	init_tex_ground(D3DXVECTOR3 Center =Zero, float Scale =1.0f);

	// 纹理坐标控制在[0,1]
	void	init_tex_groundV1(D3DXVECTOR3 Center =Zero);

//--
	// 使用网格存放地形各数据
	void	init_ligth_tex_ground_mesh(cVertexDecl* pVertexDecl, 
		cHeightMap * pHeightMap, D3DXVECTOR3 Center =Zero, bool bOptimize =true);

	void	draw_light_tex_mesh_Ground();

//--
	// 将整个网格分解成若干子网格
	void	init_light_tex_ground_sub_mesh(cVertexDecl* pVertexDecl,
		cHeightMap* pHeightMap, D3DXVECTOR3 Center =Zero);

	// 渲染子网格
	void	draw_light_tex_sub_mesh_ground();

	// 给出位置得到所处的高度
	float	get_light_texGround_sub_mesh_Y(cHeightMap* pHeightMap,
		float Row, float Col);

protected:
	void	sub_mesh(RECT& Rect, vector<LightTextureVertexV1>& GobalVertex,
					   cVertexDecl* pVertexDecl);

private:
	IDirect3DDevice9* _pDevice;
	const Terrain& _Terrain; // 地形的结构体描述
	IDirect3DVertexBuffer9* _pVertexBuffer;
	IDirect3DIndexBuffer9* _pIndexBuffer;

	// 地形顶点，索引，法线，纹理坐标都在网格里
	ID3DXMesh*	_pMesh;	 // 地形的整个网格
	vector<ID3DXMesh*> _pSubMesh; // 地形的子网格
	vector<sBoundBox> _BoundBoxList; // 地形的包围框

};



