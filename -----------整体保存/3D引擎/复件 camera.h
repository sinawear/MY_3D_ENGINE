
#ifndef _CAMERA_
#define _CAMERA_

#include "engine_base.h"
#include "input_check.h"

class cCameraUVN
{
public:
	enum	camera_type{LAND, AIR};

	cCameraUVN(camera_type type =LAND);
	~cCameraUVN();
	
	void	init(int width =800, int height =600, 
		D3DXVECTOR3 pos =D3DXVECTOR3(0,0,0), D3DXVECTOR3 lookat =D3DXVECTOR3(0,0,1), 
		D3DXVECTOR3 up =D3DXVECTOR3(0,1,0), 
		float fov =D3DX_PI/4, float zn =1.0f, float zf =5000.0f);

	void	compute();
	void	reset();
	
	void		rotate_x_axis(float angle);
	void		rotate_y_axis(float angle);
	void		rotate_z_axis(float angle);

	void		walk_x_axis(float units);
	void		walk_y_axis(float units);
	void		walk_z_axis(float units);

	D3DXMATRIX&	cCameraUVN::getViewMatrix();
	D3DXMATRIX&	cCameraUVN::getProjMatrix();
	void		getViewPort(D3DVIEWPORT9& t);
	void		setPos(D3DXVECTOR3 t);
	void		getPos(D3DXVECTOR3& t);

	void		getDeep(D3DXVECTOR3& t); 

//--增强
	D3DXVECTOR3& get_pos(){
		return m_pos;
	}
	
	void	computeV1();

	void	setLookat(D3DXVECTOR3 t){
		m_lookat =t;
		m_deep =m_lookat - m_pos; //m_looat的改变，m_deep要重新计算
	}

	void	computeV2(cInputController* pInput, float delt_time);

	void	setSpeed(float s){
		m_Speed =s;
	}
private:
	camera_type	m_type;

	D3DXVECTOR3	m_pos,
				m_lookat,
				m_up,
				m_right,
				m_deep;

	D3DXMATRIX	m_viewMatrix,
				m_projMatrix;

	int			m_width,
				m_height;

	float		m_fov,
				m_zn,
				m_zf;

	D3DVIEWPORT9  m_viewport;

	float m_Speed;
};


#endif