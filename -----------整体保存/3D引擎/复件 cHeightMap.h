// by sinawear [12/28/2011 O.O]
#pragma once
#include "engine_base.h"
#include "system_log.h"

static cSystemLog Log;

template<class TYPE>
class cTable
{
public:
	cTable(){}

	void resize(int NumRow, int NumCol)
	{
		m_NumRow =NumRow;
		m_NumCol =NumCol;

		m_Table.resize(m_NumRow * m_NumCol);
	}
	
	TYPE& operator()(int i, int j)
	{
		return m_Table[i*m_NumCol + j];
	}

	

protected:
	vector<TYPE> m_Table;

	int m_NumRow;
	int m_NumCol;
};





class cHeightMap
{
public:
	cHeightMap(){}
	
	cHeightMap(int NumRow, int NumCol, string FileName, 
		float HeightScal, float HeightOffset)
	{
		m_bIsLoad =false;
		
		load_row(NumRow, NumCol, FileName, HeightScal, HeightOffset);
		
		m_bIsLoad =true;
	}

	void load_row(int NumRow, int NumCol, const string FileName, 
		float HeightScal, float HeightOffset)
	{
		if(m_bIsLoad == true)
			MyMsgBox(L" 高度图已经被加载。", true);

		// 高度图文件和内存
		ifstream FileRead;
		vector<UCHAR> FileMemory(NumRow * NumCol);
		
// 打开文件读取到内存
		FileRead.open(FileName.c_str(), ios::binary);
			if(FileRead.is_open())
			{
				FileRead>>(UCHAR*)&FileMemory[0];
			}
// 关闭文件
		FileRead.close();
	
		
		// 调整高度图大小
		m_HeightMap.resize(NumRow, NumCol);

		for(int i =0; i< NumRow; i++)
		{
			for(int j =0; j< NumCol; j++)
			{
				float t =FileMemory[i*NumCol+j] * HeightScal + HeightOffset;
				m_HeightMap(i, j) = t;
				Log.appendLog(LOG_FILE_NAME, "%f\n", t);
			}
		}
				
	}

	float getHeight(int i, int j)
	{
		float h =(float)m_HeightMap(i, j);
		return  h;
	}

protected:
	bool m_bIsLoad;
//	vector<BYTE> m_HeightMap; // 好吧，要模仿就模仿的像些
	cTable<float> m_HeightMap;

};