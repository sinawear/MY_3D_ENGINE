#pragma once

const D3DXVECTOR3 Zero(0,0,0);


const D3DXMATRIX I_MATRIX(
						  1,0,0,0,
						  0,1,0,0,
						  0,0,1,0,
						  0,0,0,1);
