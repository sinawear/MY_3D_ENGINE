#pragma once
#include "engine_base.h"

class cVertexDecl
{
public:
	cVertexDecl(IDirect3DDevice9* pDevice)
	{	
		_pDevice =pDevice;

	}

	~cVertexDecl()
	{
		SAFE_RELEASE(_pVertexDecl);
	}

	void createV1(DWORD FVF, void* p){
		
	}
	
	// 1：灵活顶点格式；2：一个灵活顶点的大小
	void create(DWORD FVF, DWORD VertexStride)
	{
		_FVF =FVF;
		_VertexStride =VertexStride;

		HR(D3DXDeclaratorFromFVF(FVF, _Declaration));
		
		// 顶点声明接口
		int r =_pDevice->CreateVertexDeclaration(_Declaration, &_pVertexDecl);
		
	}

	// 相当于SetFVF()
	void apply()
	{
		_pDevice->SetVertexDeclaration(_pVertexDecl);
	}

	// 返回一个灵活顶点的大小
	DWORD get_vertex_stride()
	{
		return _VertexStride;
	}

	D3DVERTEXELEMENT9* get_vertex_element()
	{
		return _Declaration;
	}

	IDirect3DDevice9* get_D9_device()
	{
		return _pDevice;
	}

	DWORD get_fvf()
	{
		return _FVF;
	}


protected:
	IDirect3DDevice9* _pDevice;

	D3DVERTEXELEMENT9 _Declaration[MAX_FVF_DECL_SIZE];
	IDirect3DVertexDeclaration9* _pVertexDecl;

	DWORD _VertexStride;
	DWORD _FVF;
};