
#ifndef _MOUSE_ANIMATE_H
#define _MOUSE_ANIMATE_H

#include "engine_base.h"

class cMouseAnimate
{
public:
	cMouseAnimate(){D3DXMatrixIdentity(&obj_world);}

	void chance(cInputController* pInput, bool bEnableCursorFx =false)
	{
		static bool is_process =false;
		bool bLButtonDown =false;

		if(pInput->isMouseDown(0))
			bLButtonDown =true;

		if(pInput->isMouseUp(0))
			bLButtonDown =false;

		if(bEnableCursorFx)
		{
			//控制鼠标显示
			if(bLButtonDown == true)  
			{
				if(is_process == false)//没有处理过，并且按下，则隐藏光标
				{
					ShowCursor(FALSE);
					is_process =true;  // 设置处理过
				}
			}
			else //没有按下
			{
				if(is_process == true)
				{
					ShowCursor(TRUE);
					is_process =false;
				}
			}
		}
		if(bLButtonDown) // 做相应的转动
		{
			static	int i =0;
			static int j =0;

			if(pInput->getMouseState(0) <0)
				i +=5;
			if(pInput->getMouseState(0) >0)
				i -=5;

			if(pInput->getMouseState(1) < 0)
				j +=1;
			if(pInput->getMouseState(1) > 0)
				j -=1;

			D3DXMATRIX t, t1;
			D3DXMatrixRotationY(&t, D3DXToRadian(i));
			D3DXMatrixRotationX(&t1, D3DXToRadian(j));

			obj_world =t * t1;
		}
	}

	
	void chanceObjWorld(cInputController* pInput, bool bEnableCursorFx =false)
	{
		chance(pInput, bEnableCursorFx);
	}

	void chanceCameraView(cInputController* pInput, cCameraUVN& camera)
	{
		static bool is_process =false;
		bool bRButtonDown =false;

		float i =0;
		float j =0;

		if(pInput->isMouseDown(1))
			bRButtonDown =true;

		if(pInput->isMouseUp(1))
			bRButtonDown =false;

		//控制鼠标显示s
		if(bRButtonDown == true)  
		{
			if(is_process == false)//没有处理过，并且按下，则隐藏光标
			{
				ShowCursor(FALSE);
				is_process =true;  // 设置处理过
			}
		}
		else //没有按下
		{
			if(is_process == true)
			{
				ShowCursor(TRUE);
				is_process =false;
			}
		}

		if(bRButtonDown) // 做相应的转动
		{
			if(pInput->getMouseState(0) <0)
				i -=1.1;
			if(pInput->getMouseState(0) >0)
				i +=1.1;

			if(pInput->getMouseState(1) < 0)
				j -=1.1;
			if(pInput->getMouseState(1) > 0)
				j +=1.1;

			//D9角度为正，顺时针旋转。
			camera.rotate_x_axis(D3DXToRadian(j));
			camera.rotate_y_axis(D3DXToRadian(i));
	
		}	
	}

	D3DXMATRIX obj_world;

};

#endif