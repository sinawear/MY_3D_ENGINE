
//以后估计要把窗口类与创建窗口分开.
//通过CONTORLLER来提供简单的窗口

// 应提供窗口标题栏的文字，修改。
#include "window_main.h"

// 文件型变量就是好。
static bool	_bFocusOn; //程序是否在焦点
	

cWindow::cWindow(HINSTANCE instance)
{
	m_instance =instance;
}

cWindow::~cWindow()
{
	shutDownWindow();
}

void	cWindow::initWindow(int	width, int height, WNDPROC ExternWindowProc, bool top_most, DWORD style)
{

	
	int		x =0, y=0;  // 窗口的屏幕坐标

	//得到系统窗口的大小，即：分辨率
	int temp_x =GetSystemMetrics(SM_CXSCREEN);
	int temp_y =GetSystemMetrics(SM_CYSCREEN);

	x =temp_x/2 - width/2 ;
	y =temp_y/2 - height/2 ;

	m_width =width;
	m_height =height;

	m_wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	m_wc.hIcon =	LoadIcon(NULL, IDI_INFORMATION);
	m_wc.hIconSm = LoadIcon(NULL, IDI_INFORMATION);
	m_wc.hInstance = m_instance;
	if(ExternWindowProc)
		m_wc.lpfnWndProc = ExternWindowProc;
	else
		m_wc.lpfnWndProc = cWindow::InsideWindowProc;

	m_wc.cbSize = sizeof(WNDCLASSEX );
	m_wc.lpszClassName = _T("sinawear");
	m_wc.lpszMenuName = NULL;
	m_wc.hCursor = LoadCursor(NULL,IDC_ARROW); 
	m_wc.style  = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	m_wc.cbWndExtra =0;
	m_wc.cbClsExtra =0;

	RegisterClassEx(&m_wc);

	m_handle =CreateWindowEx(
		top_most == true ? WS_EX_TOPMOST:NULL, 
		_T("sinawear"), _T("window_name"), 
//		WS_OVERLAPPEDWINDOW ,
//		WS_POPUP,
		style,
		x , y, m_width, m_height, NULL ,NULL, m_instance, NULL);

// 保持客户区为指定大小
	RECT window_rect = {0,0,m_width, m_height};
	
	// make the call to adjust window_rect
	AdjustWindowRectEx(&window_rect,
		GetWindowStyle(m_handle),
		GetMenu(m_handle) != NULL,
		GetWindowExStyle(m_handle));
		
	// now resize the window with a call to MoveWindow()
	MoveWindow(m_handle,
		x, // x position
		y, // y position
		window_rect.right - window_rect.left, // width
		window_rect.bottom - window_rect.top, // height
		TRUE);
	
	// show the window, so there's no garbage on first render
	ShowWindow(m_handle, SW_SHOW);
}

// 用_bFoucsOn来调整窗口间切换，保证流畅
// 可是，如果，我不采取任何措施，切换窗口，得到焦点的
// 窗口会遮挡住游戏窗口，游戏窗口的FPS会显著升高，
// 显然D9，交换链做了操作，只拷贝没遮挡的地方，
// 导致拷贝更快。

// 暂时看来，_bFoucsOn还是必要的
void	cWindow::messageLoop(MSG& msg, void (*fuc)())
{
	while(msg.message!=WM_QUIT)
	{
		/// 消息队列中有消息时，调用相应的处理过程
		if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else
			/// 如果没有需要处理的消息，将进入渲染
		{
			if(isWindowFocusOn() == false) // 如果程序不再焦点，则放出些时间片
				Sleep(1);

			fuc();
		}
	}
	
}

void	cWindow::shutDownWindow()
{
	UnregisterClass(m_wc.lpszClassName, m_instance);
}

HWND	cWindow::getWindowHandle()
{
	return m_handle;
}

HINSTANCE	cWindow::getWindowInstance()
{
	return m_instance;
}

bool cWindow::isWindowFocusOn()
{
	return _bFocusOn;
}


LRESULT CALLBACK cWindow::InsideWindowProc(HWND hwnd,UINT uMsg,WPARAM wParam,LPARAM lParam)							 
{
	switch(uMsg)
	{	
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
		
		// WM_ACTIVE is sent when the window is activated or deactivated.
		// We pause the game when the main window is deactivated and 
		// unpause it when it becomes active.
	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE )
			_bFocusOn = true;
		else
			_bFocusOn = false;
		break;
	}
	return DefWindowProc (hwnd, uMsg, wParam, lParam) ;
}