#pragma once


#include "engine_base.h"
#include "engine_fvf.h"
#include "cFx.h"

class cTerrainV1;
// grass不参与光照，用因子改变纹理的颜色
class cGrass
{
	struct GrassVertex
	{
		GrassVertex():pos(0.0f, 0.0f, 0.0f),tex0(0.0f, 0.0f),amplitude(0.0f){}
		GrassVertex(
			const D3DXVECTOR3& v, 
			const D3DXVECTOR2& uv, float amp
			):pos(v), tex0(uv), amplitude(amp){}

		D3DXVECTOR3 pos;
		D3DXVECTOR3 quadPos;
		D3DXVECTOR2 tex0;
		float amplitude; // for wind oscillation.
		D3DCOLOR colorOffset;
		
		static IDirect3DVertexDeclaration9* Decl;
	};
public:
	cGrass(IDirect3DDevice9* pDevice);
	~cGrass();


	void init(cTerrainV1* pTerrain, char* TexName ="grassfin0.dds");

	void draw(D3DXMATRIX& view, D3DXMATRIX& proj, float Time,
				  D3DXVECTOR3 EyePos);

protected:
	void _build(GrassVertex* v, WORD* k, int& indexOffset, 
		 D3DXVECTOR3 worldPos, const D3DXVECTOR3& scale);

	void _paint();
private:
	// 作者使得，我使不得？
//	static const int NUM_GRASS = 400;

	IDirect3DDevice9* _pDevice;
	ID3DXMesh* _pGrassMesh;

	cFx* _pGrassFx;
	IDirect3DTexture9* _pGrassTex;
};