
#pragma once

#include "engine_base.h"
#include "engine_fvf.h"
#include "engine_disk_io.h"
//
//---------------------------------
// 数据结构区


//
//-----------------------------------------------------
//函数区

// 初始化设备
IDirect3DDevice9*	init_d3d9_device(HWND main_window_handle, int width =800, int height =600, BOOL windowed=TRUE);

// 初始化顶点缓冲,仅仅初始化一个三角形
IDirect3DVertexBuffer9*	init_vertex_buffer(IDirect3DDevice9* pDevice);

// 初始化物体到屏幕的变换矩阵									
void	init_draw_matrix(IDirect3DDevice9* pDevice);

// 初始化渲染状态 
void	init_render_machine_state(IDirect3DDevice9* pDevice);



												  
									