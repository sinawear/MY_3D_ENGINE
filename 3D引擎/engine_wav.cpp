
#include "engine_wav.h"

void	init_sound(IDirectSound**	ppds, HWND	main_window_handle)
{
	DSCAPS	dscaps;

	DirectSoundCreate(NULL,ppds,NULL) ;

	(*ppds)->GetCaps(&dscaps);

	(*ppds)->SetCooperativeLevel(main_window_handle,DSSCL_NORMAL);
}


IDirectSoundBuffer*		load_sound(IDirectSound* lpds,TCHAR* lpzFileName)
{
	   WAVEFORMATEX pwfx; 
	   HMMIO hmmio; 
	   MMCKINFO mmckinfo={0}; 
	   MMCKINFO mmckinfoParent={0}; 
	   LPDIRECTSOUNDBUFFER lpdsbStatic=NULL;
	   LPVOID lpvAudio1,lpvAudio2=NULL;
	   DWORD dwBytes1 =0,dwBytes2 =0;
	   DSBUFFERDESC dsbdesc;
	   UINT cbBytesRead =0; 
	   CHAR* buff;
	   
	   /*
	   *	读wav文件.
	   */	
	   // MMIO打开文件
	   hmmio =mmioOpen(lpzFileName,NULL,MMIO_READ);
	   if(NULL==hmmio) MessageBox(NULL,_T("can't open music"),_T("!"),0);
	   
	   // 判断类型
	   mmckinfo.fccType =mmioFOURCC('W','A','V','E');  //RIFF的形式类型.
	   mmioDescend(hmmio,&mmckinfo,NULL,MMIO_FINDRIFF);
	   mmckinfoParent.ckid =mmioFOURCC('f','m','t',' ');
	   mmioDescend(hmmio,&mmckinfoParent,&mmckinfo,MMIO_FINDCHUNK);
	   mmioRead(hmmio,(char*)&pwfx,sizeof(pwfx));
	   
	   // 判断是不是PCM类型
	   if(WAVE_FORMAT_PCM==mmckinfoParent.dwFlags) 
		   MessageBox(NULL,_T("not PCM wav"),_T("!"),0);
	   mmioAscend(hmmio,&mmckinfoParent,0);
	   mmckinfoParent.ckid =mmioFOURCC('d','a','t','a');
	   mmioDescend(hmmio,&mmckinfoParent,&mmckinfo,MMIO_FINDCHUNK);

	   // 开辟缓冲
	   //..
	   buff=(CHAR*)malloc(mmckinfoParent.cksize);
	   // 缓冲得到数据
	   mmioRead(hmmio,buff,mmckinfoParent.cksize);

	   // MMIO关闭文件
	   mmioClose(hmmio,NULL);
	   
#if 0	
 	   TCHAR temp[80];
	   HWND	main_handle =GetFocus();
 	   HDC hdc =GetDC(main_handle);
 	   _stprintf(temp,_T("频率 %d   "),pwfx.nAvgBytesPerSec);
 	   SetBkColor(hdc,RGB(0,0,0));
 	   SetTextColor(hdc,RGB(0,255,255));
 	   TextOut(hdc,0,16*4,temp,_tcslen(temp));
	   
#endif
	   // 创建SOUNDBUFFER
	   if ( lpdsbStatic == NULL )
	   {
		   memset( &dsbdesc, 0, sizeof( DSBUFFERDESC ) ); 
		   dsbdesc.dwSize = sizeof( DSBUFFERDESC ); 
		   dsbdesc.dwFlags = DSBCAPS_STATIC | 0xE0;  //静态加音乐控制.
		   dsbdesc.dwBufferBytes = mmckinfoParent.cksize; 
		   dsbdesc.lpwfxFormat = &pwfx; 
		   lpds->CreateSoundBuffer(&dsbdesc, &lpdsbStatic, NULL); 
	   }
	   
	   // 这有点像顶点缓存的数据拷贝。
	   lpdsbStatic->Lock( 0, 0, &lpvAudio1, &dwBytes1, NULL, NULL, DSBLOCK_ENTIREBUFFER );
	   memcpy(lpvAudio1,buff,mmckinfoParent.cksize);
	   lpdsbStatic->Unlock( lpvAudio1, dwBytes1, NULL, 0 );
	   
	   // 释放BUFF
	   //--	
	   SAFE_DELETE(buff);

	   return lpdsbStatic;
}