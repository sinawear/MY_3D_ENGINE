1)NoXRender.cpp 包含头文件有问题。以修改。

2)input_check.cpp文件：
cInputController类写的，有个小问题：
当第一次执行时，判断pInput->isKeyUp()会返回成功。
已修改。


1:42 2011/11/17
input_check.cpp
182行，错误调用。应调用鼠标的成员函数，却调用了键盘的成员函数。

13:20 2011/11/17
input_check
修复清晰化鼠标独占参数。
我记得以前窗口模式下，设置鼠标独占没有用呢，鼠标不会消失的。-_-?

0:36 2011-11-22
input_check
isKeyDown却调用了mouse的成员函数。


22:38 2011-12-12
-_-noXRender.cpp，Render.cpp
addTexture()代码的队列设置错误。
m_pTexList[index].少了index索引。


15:27 2012/6/25
修改了DXERR，使脱离了DX2004，全面使用DX2010.
23:52 2011-12-12
setMaterial()成员为const以便接受设计好的材质。

23:56 2011-12-12
设计错误：
修改applyTexture();
没给出合理的索引。

19:53 2011-12-17
对camera进行更新。

20:23 2011-12-18
增加D9_comm.h；cUnionMatrix.h
前者设计一些常量，后者是解决矩阵声明太麻烦了。
通过类来提供。（不过，还一种思想是给D3DXMATRIX改名成matrix.
这样短些，好看一些。未实现。）

22:30 2011-12-27
input_check.
void	cInputController::updateDevice()
if(m_hwnd == GetFocus)改为if(m_hwnd == GetForegroundWindow)
因为，GetFocus得到的是有键盘焦点的窗口，用MFC时，会出现得到的是客户区的窗口句柄。这和初始化D9传入的是整个窗口的句柄。于是，各updateDevice失败。
判断出现错误。

23:33 2011-12-27
input_check.
void	cInputController::updateDevice()再次修改为：

::GetCursorPos(&Point);
if(m_hwnd == ::WindowFromPoint(Point))
	;
else
	m_pMouseDevice->clear();
因为，思想改变了，我在MFC的View中得到其窗口句柄，这样就能整体保持一致。
当鼠标移动到显示区外的时候，显示区不要在谁鼠标改变了。透过鼠标位置得到窗口句柄并比较，就能完成了（想到了，DNF，不用Windows的标题栏了。）。
这样input_check（）就能支持在MFC下运行了。


0:31 2012-1-1
修改了camera。m_deep = m_looat - m_pos;
改为:m_deep =m_lookat;

23:26 2012-1-3
加入
cCameraV1
cGroundV1
cTerrainV1
因为，要使用平截头剔除技术。
1）针对cGroundV1,可以更新冗余操作，比如：将数据放入顶点网格后，还要再读出，再写入到子网格。
修改后为：为直接用向量储存整个地形数据再写入。期间也忽略掉了，整个地形的索引缓存和优化操作。

2）针对cCameraV1，加入平截头提出操作。

3）cTerrainV1是cGroundV1的上层。负责统筹，渲染。因为，要依赖摄像机。
如果将摄像机设计为传参的话，那么,cTerrainV1将需要新的函数负责渲染。
而且还要设计些类的前导词。不太好看。
如果，按作者的方法将camera设计为全局的话，则几乎不用修改。


3:24 2012-1-4
random_factory.h

GetRandomFloat()中的：
float f = (my_rand() % 10000) * 0.0001f;
修改为：
float f = (my_rand() % 10001) * 0.0001f;
修改后的取值范围才是[0,1];

17:45 2012/6/24
camera.setpos()
m_deep =m_lookat - m_pos;注释掉，不需要再次根据位置计算深度了。
没改以前，如果摄像机初始化位置是负值。调用setpos（）设置为正值。摄像机将
发生反转，朝向了负Z轴。

16:28 2012/6/27
修改camera，compute（）
