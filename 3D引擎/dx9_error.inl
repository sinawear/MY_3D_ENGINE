#pragma once


// 根据D3D返回值，弹出错误代号和说明。
inline void Failed_Msg_BoxW(PCWSTR Text, HRESULT r)
{
#if _DEBUG
	if(SUCCEEDED(r))
		return;

	PCWSTR Caption =L"-_-!";
	wstring wstr;

/*__FILE__只能比标示，当前文件，因为不是传进来的所以老是DX9_ERROR.INL；
	//好一个--FILE--呀
#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)
#define __WFILE__ WIDEN(__FILE__)

	wstr +=L"错误文件：";
	wstr += __WFILE__;
	wstr +=L"\n";
	*/
	wstr +=L"错误函数：";
	wstr +=Text;
	wstr +=L"\n";
	wstr +=L"错误代码：";
//	wstr +=DXGetErrorString9W(r);
	wstr +=DXGetErrorStringW(r);
	wstr +=L"\n";
	wstr +=L"错误说明：";
//	wstr +=DXGetErrorDescription9W(r);
	wstr +=DXGetErrorDescriptionW(r);

	::OutputDebugStringW(wstr.c_str());
	int re =MessageBoxW(NULL, wstr.c_str(), Caption, 0 );

	DWORD Error =::GetLastError();
	
	exit(0);

#endif
}

inline void Failed_Msg_BoxA(char* Text, HRESULT r)
{
#ifdef _DEBUG

	int MultiLen =strlen(Text) + 1;
	assert(MultiLen < 1024);

	//wchar_t* wText =new wchar_t[1024];..-_-?动态开辟不行呢。。
	//ZeroMemory(wText, MultiLen);

	HRESULT t;	
	wchar_t wText[1024] ={0};
	t =MultiByteToWideChar(CP_ACP, NULL, Text, MultiLen, wText, sizeof(wText)/sizeof(wText[0]));

	Failed_Msg_BoxW(wText, r);

//	delete[] wText;
#endif
}

//////////////////////////////////////////////////////////////////////////
//fuc() 反正都要调用宽的字符，微软干嘛不这样写呢。
//{
//	变化
//fucW()
//}

//////////////////////////////////////////////////////////////////////////


/*
char* File_Name =__FILE__;
int MultiLen1 =strlen(File_Name) + 1;
assert(MultiLen < 1024);

wchar_t wText1[1024] ={0};
t =MultiByteToWideChar(CP_ACP, NULL, File_Name, MultiLen, wText1, sizeof(wText1)/sizeof(wText1[0]));

wcscat(wText1 , wText);
*/