 // 通过测试 [5/26/2012 sinawear]

#pragma once
#include "engine_base.h"
#include "engine_terrain.h"
#include "cVertexDecl.h"
#include "cHeightMap.h"
#include "bound.h"
//////////////////////////////////////////////////////////////////////////

/**
  基础地板，用于当参照物。
  可以运行在固定管道或SHADER；
 */
class cGround
{
public:
	/**
	  @remark
		要确保Terrain中的单元大小是2的倍数。
	  @param
		t: 地形属性结构体，存放地形的相关数据。
	 */
	cGround(IDirect3DDevice9* pDevice, const Terrain t):_Terrain(t)
	{
		_pDevice =pDevice;	

		_pVertexBuffer =NULL;
		_pIndexBuffer =NULL;

		_pMesh =NULL;
	}
	
	~cGround()
	{
		SAFE_RELEASE(_pIndexBuffer);
		SAFE_RELEASE(_pVertexBuffer);

		// 释放掉整个网格
		SAFE_RELEASE(_pMesh);

		// 释放掉各子网格
		for(vector<ID3DXMesh*>::iterator i = _pSubMesh.begin(); i !=_pSubMesh.end();
		i++)
		{
			SAFE_RELEASE((*i));
		}		
	}

	/**
		用的COLOR_FVF创建顶点缓冲。
	@param
		Center: 地形的原点,从而可以改变地形的位置。
	 */
	void	init_ground(D3DXVECTOR3 Center =Zero); // 兼容存在

	/// 用COLOR_FVF创建网格。
	void	init_ground_mesh(IDirect3DDevice9* device,
		D3DXVECTOR3 Center =Zero, bool bOptimize =true);

	/// 得到init_ground_mesh（）创建的网格
	ID3DXMesh* get_ground_mesh(){return _pMesh;}

	/**
	  渲染网格。
	  @param 
		i: 0默认线框输出，1可以输出点阵，2可以SOLID输出;
	 */
	void	draw_ground(int i =0); 

	/**
	  渲染网格。
	  @remark
		通用渲染办法。
		针对init_ground();pVertexDecl创建时要使用COLOR_FVF。
		如果传入的是LIGHT_TEXTURE_FVF的话会渲染错误，线路紊乱状。
	  @param
		pVertexDecl: 顶点格式类。
	 */
	void	draw_ground( cVertexDecl* pVertexDecl);

//--更新加强
// 该函数抛弃旧版内容,虽然可以改写原函数
// 但是，因为，要背历史包袱，就写个新的呃。

	/**
	  创建顶点格式有，颜色地形。
	 */
	void	init_color_ground(D3DXVECTOR3 Center =Zero, 
		D3DXCOLOR Color1 =GRAY, D3DXCOLOR Color2 =BLACK);

	/**
	  创建顶点格式有，法线地形。
	 */
	void	init_light_ground(D3DXVECTOR3 Center =Zero);

	/**
	   创建顶点格式有，法线，纹理地形。
	   纹理坐标在[0,+infinite]。
	   @param
		Scale: 缩放地图的UV值，这样调节重复次数。
	 */
	void	init_tex_ground(D3DXVECTOR3 Center =Zero, float Scale =1.0f);

	/**
	   创建顶点格式有，法线，纹理地形。
	   纹理坐标控制在[0,1]。
	 */
	void	init_tex_groundV1(D3DXVECTOR3 Center =Zero);

//-1-
	/**
		生成地形网格。
	  @param
		pVertexDecl: 必须是LIGHT_TEXTURE_FVF创建的。
	  @param	
		pHeightMap: 外部高度图。
	  @param	
		Center: 网格的原点，用以调整位置。
	  @param
		bOptimize: 是否优化网格顶点数据;
	@remark
		内部创建整块网格存放地形数据。
		UV【0,1】
	 */ 
	void	init_light_tex_ground_mesh(cVertexDecl* pVertexDecl, 
		cHeightMap * pHeightMap, D3DXVECTOR3 Center =Zero, bool bOptimize =true);

	/**
	  渲染网格，仅一行代码：_pMesh->DrawSubset(0);
	 */
	void	draw_light_tex_mesh_Ground();
//-1-


//////////////////////////////////////////////////////////////////////////
	/**
	  生成地形。
	  @param
		  pVertexDecl: 必须是LIGHT_TEXTURE_FVF创建的。
	@param
		  pHeightMap: 外部高度图。
	@param
		  Center: 网格的原点，用以调整位置。		
	  @remark
		内部创建整块网格存放地形数据，整块网格分解成若干子网格，整块网格被释放。
	 */
	void	init_light_tex_ground_sub_mesh(cVertexDecl* pVertexDecl,
		cHeightMap* pHeightMap, D3DXVECTOR3 Center =Zero);

	/**
		渲染地形。
		内部是循环渲染子地形块。
	 */
	void	draw_light_tex_sub_mesh_ground();

	/**
		给出位置得到所处的高度	
	 */
	float	get_light_texGround_sub_mesh_Y(cHeightMap* pHeightMap,
		float Row, float Col);

protected:
	/**
	  被init_light_tex_ground_sub_mesh（）内部调用。
	 */
	void	sub_mesh(RECT& Rect, vector<LightTextureVertexV1>& GobalVertex,
					   cVertexDecl* pVertexDecl);

private:
	IDirect3DDevice9* _pDevice;
	const Terrain _Terrain; // 地形的结构体描述
	IDirect3DVertexBuffer9* _pVertexBuffer;
	IDirect3DIndexBuffer9* _pIndexBuffer;

	// 地形顶点，索引，法线，纹理坐标都在网格里
	ID3DXMesh*	_pMesh;	 // 地形的整个网格
	vector<ID3DXMesh*> _pSubMesh; // 地形的子网格
	vector<sBoundBox> _BoundBoxList; // 地形的包围框

};
//网格的层次更高，包含了vertexbuff,indexbuff;



