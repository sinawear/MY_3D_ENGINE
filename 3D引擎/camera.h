// 验证通过。 [5/27/2012 sinawear]
#ifndef _CAMERA_
#define _CAMERA_

#include "engine_base.h"
#include "input_check.h"
#include "cTerrain.h"
#include "Bound.h"

/**
  摄像机类。
  computeV2()中调用cTerrain函数，而cTerrain又依赖cGround函数。因此camera还依赖cGround；
 */
class cCameraUVN
{
public:
	enum	camera_type{LAND, AIR};

	cCameraUVN(camera_type type =LAND);
	~cCameraUVN();
	
	void	init(int width =800, int height =600, 
		D3DXVECTOR3 pos =D3DXVECTOR3(0,0,0), D3DXVECTOR3 lookat =D3DXVECTOR3(0,0,1), 
		D3DXVECTOR3 up =D3DXVECTOR3(0,1,0), 
		float fov =D3DX_PI/4, float zn =0.01f, float zf =5000.0f);

	void	compute();

	///内部调用init()。
	void	reset(); 
	
	/**
		rotate_x_axis
		rotate_y_axis
		rotate_z_zxis
		接受的参数是弧度。
	 */
	void		rotate_x_axis(float angle);
	void		rotate_y_axis(float angle);
	void		rotate_z_axis(float angle);

	void		walk_x_axis(float units);
	void		walk_y_axis(float units);
	void		walk_z_axis(float units);

	const D3DXMATRIX&	getViewMatrix() {return m_viewMatrix;}
	const D3DXMATRIX&	getProjMatrix() {return m_projMatrix;}
	const D3DXMATRIX&	getViewProjMatrix() {return m_viewProjMatrix =m_viewMatrix*m_projMatrix;}
	void		getViewPort(D3DVIEWPORT9& t);
	void		setPos(const D3DXVECTOR3& t);
	void		getPos(D3DXVECTOR3& t);

	void		getDeep(D3DXVECTOR3& t); 

//--增强
	const D3DXVECTOR3& get_pos() {return m_pos;}
	const D3DXVECTOR3& get_deep() {return m_deep;}
	
	const D3DVIEWPORT9& get_viewport() {return m_viewport;}
	
	/** 
		使用DX，D3DXMatrixLookAtLH()；为了应对不同的UP，和deep的改变。
		新龙书采用的办法是设计一个共用的BulidView（）。lookat（）实现deep随pos调整的情况。
		update()就是deep，不受pos的影响的情况。
		lookat的实现，我是可以做到的。不过先用DX的吧。
	*/
	void	computeV1(const D3DXVECTOR3& pos, const D3DXVECTOR3& lookat, const D3DXVECTOR3& up,
		int width , int height, float fov =D3DX_PI/4, float zn =0.01f, float zf =5000.0f);

	void	setLookat(const D3DXVECTOR3& t)
	{
		m_lookat =t;
		m_deep =m_lookat - m_pos; //m_looat的改变，m_deep要重新计算
	}

	void	computeV2(cInputController* pInput, float delt_time,
		cTerrain* pTerrain, float OffsetY);

	/// 设置摄像机移动的速度。
	void	setSpeed(float s) {m_Speed =s;}

//--平截头
	void	makeFrustum();
	bool	isVisible(const sBoundBox& box) const;

private:
	camera_type	m_type;

	D3DXVECTOR3	m_pos,
				m_lookat,
				m_up,
				m_right,
				m_deep;

	D3DXMATRIX	m_viewMatrix,
				m_projMatrix,
				m_viewProjMatrix;

	int			m_width,
				m_height;

	float		m_fov,
				m_zn,
				m_zf;

	D3DVIEWPORT9  m_viewport;

	float m_Speed;

//--
	D3DXPLANE m_FrustumPlanes[6];
};


#endif 