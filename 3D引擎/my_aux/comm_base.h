
#ifdef USE_PCH
#include "stdafx.h"
#endif



#ifndef COMM_BASE_H
#define COMM_BASE_H

#include <math.h>
#include <memory.h>
#include <time.h>
#include <tchar.h>
#include <wchar.h>
#include <stdio.h>
#include <assert.h>

#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <vector>
#include <map>
#include <stack>

#include <cctype>
#include <sstream>


using namespace std;
using std::ifstream;
using std::ofstream;
using std::wifstream;
using std::wofstream;

#endif