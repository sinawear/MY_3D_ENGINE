


#ifndef ZL_WINDOW_SYSTEM_
#define ZL_WINDOW_SYSTEM_

#include "engine_base.h"


#ifdef CAMERA
// 头文件问题关闭cCamera;    1)使用2004D9; 或则2)使用新winsdk+2010
#include "cCamera.h"
#endif


#include <shlobj.h>
#pragma comment(lib, "shell32.lib")






BEGINE_ZL_NAMESPACE


/// 存放和系统相关的函数.
class cWindowSystem
{
public:

	cWindowSystem()
	{
		m_pStrSystemFolder = new TCHAR[MAX_PATH * 2];
		m_pClipboardBuff =NULL;

		_GetOSInfo();
		_GetSystemInfo();
		_GetCPUInfo();
	}

	~cWindowSystem()
	{

		SAFE_DELETE(m_pStrSystemFolder);
		SAFE_DELETE(m_pClipboardBuff);
	}

	/// 系统使用的语言
	STRING m_SystemLanguage; 
	/// 系统使用的代码页
	STRING m_SystemPageCode; 
	/// 系统使用的代码页,十进制整数形式
	int m_SystemPageCode_int;
	
	/// 是否为64位操作系统
	bool m_is64BitSystem; 

	/// 得到操作系统名称
	STRING m_OSName; 

protected:

	/**
		  得到操作系统名称
	*/
	void _GetOSInfo()
	{

		SYSTEM_INFO info;        //用SYSTEM_INFO结构判断64位操作系统 
		GetSystemInfo(&info);
		if(info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64 ||
			info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64 || 
			info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_ALPHA64||
			info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA32_ON_WIN64)
				m_is64BitSystem = true;


	
		///
		/// 得到操作系统类型
		///
		OSVERSIONINFOEX os;
		RtlZeroMemory(&os, sizeof(os));
		os.dwOSVersionInfoSize = sizeof(os);

		if(GetVersionEx((OSVERSIONINFO *)&os))                  
		{ 
			
			//下面根据版本信息判断操作系统名称 
			switch(os.dwMajorVersion)
			{                        //判断主版本号 
			case 4: 
				switch(os.dwMinorVersion)
				{                //判断次版本号 
					case 0: 
						if(os.dwPlatformId==VER_PLATFORM_WIN32_NT) 
							m_OSName=_T("Microsoft Windows NT 4.0");                //1996年7月发布 
						else if(os.dwPlatformId==VER_PLATFORM_WIN32_WINDOWS) 
							m_OSName=_T("Microsoft Windows 95"); 
						break; 
					case 10: 
						m_OSName=_T("Microsoft Windows 98"); 
						break; 
					case 90: 
						m_OSName=_T("Microsoft Windows Me"); 
						break; 
				} 
				break; 
			case 5: 
				switch(os.dwMinorVersion)
				{               //再比较dwMinorVersion的值 
					case 0: 
						m_OSName=_T("Microsoft Windows 2000");                    //1999年12月发布 
						break; 
					case 1: 
						m_OSName=_T("Microsoft Windows XP");                    //2001年8月发布 
						break; 
					case 2: 
						if(os.wProductType==VER_NT_WORKSTATION && m_is64BitSystem)
							m_OSName=_T("Microsoft Windows XP Professional x64 Edition"); 
						else if(GetSystemMetrics(SM_SERVERR2)==0) 
							m_OSName=_T("Microsoft Windows Server 2003");        //2003年3月发布 
						else if(GetSystemMetrics(SM_SERVERR2)!=0) 
							m_OSName=_T("Microsoft Windows Server 2003 R2"); 
						break; 
				} 
				break; 
			case 6: 
				switch(os.dwMinorVersion)
				{ 
					case 0: 
						if(os.wProductType==VER_NT_WORKSTATION)
							m_OSName=_T("Microsoft Windows Vista"); 
						else 
							m_OSName=_T("Microsoft Windows Server 2008");          //服务器版本 
						break; 
					case 1: 
						if(os.wProductType==VER_NT_WORKSTATION) 
							m_OSName=_T("Microsoft Windows 7"); 
						else 
							m_OSName=_T("Microsoft Windows Server 2008 R2"); 
						break; 
				} 
				break; 
			default: 
				m_OSName=_T("未知操作系统"); 


			} //end switch 

		}




	}


	STRING 	_GetCPUInfo_string;
	void _GetCPUInfo()
	{
//#define CPUID dw 0xa20f

		DWORD dwCPUName, dwCPUReserved1,dwCPUReserved2,dwCPUID;

		TCHAR strCPUID[80];

		__asm
		{

			PUSHAD

				MOV EAX, 1  ;EAX是输入参数, 验证bit位确定功能.

				CPUID

				MOV dwCPUName, EAX

				MOV dwCPUReserved1, EBX

				MOV dwCPUReserved2, ECX

				MOV dwCPUID, EDX

			POPAD

		}

		_stprintf(strCPUID, _T("%08X-%08X-%08X"), dwCPUID,dwCPUName,dwCPUReserved2);//dwCPUReserved1不用
		
		_GetCPUInfo_string = strCPUID;
	}

	/**
		  得到操作系统的信息.
	*/
	void _GetSystemInfo()
	{

		///
		/// 得到操作系统使用的语言
		///
		LCID lcidLocaleId;     // locale identifier
		LCTYPE lctyLocaleInfo; // information type
		PWSTR pstr;            // information buffer
		INT iBuffSize;         // size of buffer

		lcidLocaleId = LOCALE_USER_DEFAULT;
		lctyLocaleInfo = LOCALE_SENGLANGUAGE /*0x00001001*/;

		// Determine the size of the buffer needed to retrieve information.
		iBuffSize = ::GetLocaleInfo( lcidLocaleId, lctyLocaleInfo, NULL, 0 );

		// 调整缓冲内部大小
		if(m_SystemLanguage.length() < iBuffSize)
		{
			m_SystemLanguage.resize(iBuffSize+1);
		}

		::GetLocaleInfo(lcidLocaleId, lctyLocaleInfo, (TCHAR*)m_SystemLanguage.c_str(), iBuffSize);
		cSTRING_FRESH::fresh(m_SystemLanguage);  //m_SystemLanguage = m_SystemLanguage.c_str();		

		///
		/// 得到操作系统使用的代码页.
		///似乎OEM代码页会和ANSI是相同的. 比如cmd属性代码页的写法: ANSI/OEM
		/// 如果仅仅得到ANSI. 有专门的API. GetACP();
		///
		lctyLocaleInfo = LOCALE_IDEFAULTANSICODEPAGE;

		// Determine the size of the buffer needed to retrieve information.
		iBuffSize = ::GetLocaleInfo( lcidLocaleId, lctyLocaleInfo, NULL, 0 );

		// 调整缓冲内部大小
		if(m_SystemPageCode.length() < iBuffSize)
		{
			m_SystemPageCode.resize(iBuffSize+1);
		}

		::GetLocaleInfo(lcidLocaleId, lctyLocaleInfo, (TCHAR*)m_SystemPageCode.c_str(), iBuffSize);
		cSTRING_FRESH::fresh(m_SystemPageCode); 	//m_SystemPageCode = m_SystemPageCode.c_str();
		m_SystemPageCode_int =_ttoi(m_SystemPageCode.c_str());
		

	//	GetCPInfoEx()

	}


public:
	
#ifdef CAMERA
	cCamera m_Camera;
#endif	

	/**

	*/
	




	/**
		执行DOS命令. 并得到返回值. 返回值没有用，能执行就返回1，不管命令对错.
		chcp===>活动代码页:	
	*/ 
	void DosExec(TCHAR* pStr)
	{
		_tsystem(pStr);
	}

	


	/**
		得到系统内置文件夹. "桌面" "我的图片" "我的音乐"
		http://baike.baidu.com/view/3143145.htm
	*/
	const TCHAR* GetSomeSystemFolder(int f)
	{	
		// 函数说明:	http://baike.baidu.com/view/3449405.htm 
		//  TRUE if successful; otherwise, FALSE.
		BOOL b =SHGetSpecialFolderPath(NULL, m_pStrSystemFolder, f, FALSE);

		if(b){
			_tcscat(m_pStrSystemFolder, _T("\\") );
			return m_pStrSystemFolder;
		}
		else
			return NULL;
	
	}


	/**
		得到"我的图片"的地址.
	*/
	const TCHAR* GetMyPicFolder()
	{
		if(GetSomeSystemFolder(CSIDL_MYPICTURES))
			return m_pStrSystemFolder;

		return NULL;
	}


	/**
		拷贝字符串到剪切板.
	*/
	bool CopyToClipboard(STRING copy_str) const 
	{
		int Length =copy_str.length() + 1;
		BOOL b =::OpenClipboard(NULL); //如果这个参数为NULL，打开剪贴板与当前任务相关联。
		if(b)
		{
			::EmptyClipboard();

			HGLOBAL hData =::GlobalAlloc(GMEM_MOVEABLE|GMEM_ZEROINIT, Length);
			TCHAR* pStr =(TCHAR*)::GlobalLock(hData);
			memcpy(pStr, copy_str.c_str(), Length);
			::GlobalUnlock(hData);

			::SetClipboardData(CF_TEXT, hData);
			::CloseClipboard();
		}

		return b > 0;
	}


	/**
		从剪切板读出字符串.
	*/
	const TCHAR* ReadClipboard(HWND hWnd) 
	{
		BOOL b =::OpenClipboard(hWnd); //如果通过CWnd打开了剪贴板，则返回非零值。 如果其他应用程序或窗口已经打开了剪贴板，则返回零。
		if(b)
		{
			HANDLE hData = ::GetClipboardData(CF_TEXT);  
			if( hData != NULL)
			{
				SIZE_T size =::GlobalSize(hData);
				if(size == 0)
				{	::CloseClipboard(); return NULL;}


				// 释放掉之前的.
				if( m_pClipboardBuff && size > _tcslen(m_pClipboardBuff) )
				{
					delete m_pClipboardBuff;
				}
				
				m_pClipboardBuff = new TCHAR[size+1];
				RtlZeroMemory(m_pClipboardBuff, size+1);

				// 从全局内存复制出数据
				TCHAR* p =(TCHAR*)::GlobalLock(hData);
				_tcscpy(m_pClipboardBuff, p);
				::GlobalUnlock(hData);

			}

			::CloseClipboard();
		}

		return m_pClipboardBuff;	
	}


private:
//	STRING m_GetSystemInfo_BUFF;

	TCHAR* m_pStrSystemFolder;	//系统内置的那些文件夹路径
	TCHAR* m_pClipboardBuff; //从剪切板读出字符串的缓冲
};





END_ZL_NAMESPACE



#endif