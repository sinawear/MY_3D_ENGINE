

#ifndef ZL_UNITY
#define ZL_UNITY

#include "engine_base.h"
#include "cWindowSystem.h"




BEGINE_ZL_NAMESPACE

/************************************************************************/
/*        16:09 2013/4/20          code by: mtf                                                   */
/************************************************************************/
/// 用对象的思想,可以方便主框架不变.  20个对象左右.
class cUnity
{

public:
	/// 使用了Window操作系统 API
	cWindowSystem m_WindowSystem, *m_pWS;



protected:
	void _init()
	{
		///
		/// 得到操作系统类型
		///
		
		
	}


public:
	cUnity() 
	{
		m_pWS = &m_WindowSystem;
		

		_init();
	}
	~cUnity() 
	{

	}


	/**
		宽字符转换为ANSI字符.
		http://msdn.microsoft.com/en-us/library/dd374130(VS.85).aspx
	*/
	const char* WideCharToMultiByte(const wchar_t* pWstr) //WideToAnsiString
	{

		// 假设StrLength为21. 返回的转换字符个数为:31... 这是为什么呢?  是编码格式吗?...应该是编码格式,指明UTF8,文字会乱码
		// 
		int hr;
		int StrLength =wcslen(pWstr); //源字符串长度
		int DestLength = StrLength * 4;
		if(StrLength > m_AnsiBuff.length()) //调整缓冲大小
			m_AnsiBuff.resize(DestLength);
		

		//The function returns 0 if it does not succeed. 
		// 返回的是已转换的个数
		hr =::WideCharToMultiByte(
			m_pWS->m_SystemPageCode_int, //代码页CP_UTF8, 用这个居然会乱码//
			NULL, //
			pWstr, //
			StrLength, //宽字符个数
			(char*)m_AnsiBuff.data(), //转换为Ansi字符的缓冲区
			DestLength, //目标ANSI缓冲区长度
			NULL, 
			NULL );


		m_AnsiBuff =m_AnsiBuff.c_str();  


		if(hr)
			return m_AnsiBuff.c_str();
		else
		{
			DWORD error =::GetLastError();
			return NULL;
		}
	}

	/**
		ANSI转换为宽字符.
		http://msdn.microsoft.com/en-us/library/dd319072(VS.85).aspx
	*/
	const wchar_t* MultiByteToWideChar(const char* pStr) //AnsiToWideString
	{

/*
		int HasTrans;
		int StrSize =strlen(pStr);
		int DestSize = StrSize + 1;
		if( StrSize > m_WideBuff.length() )
			m_WideBuff.resize(DestSize);


		HasTrans =::MultiByteToWideChar(
			m_pWS->m_SystemPageCode_int,
			NULL,
			pStr,
			StrSize,
			(wchar_t*)m_WideBuff.data(), // 目标宽字符缓冲区
			DestSize); //Size, in characters, of the buffer indicated by lpWideCharStr.

		m_WideBuff =m_WideBuff.c_str();

		if(HasTrans)
			return m_WideBuff.c_str();
		else
			return NULL;
*/
		///*  认为目标字符缓冲区, 因为不考虑最后一个,因此最后一个就会被设置为0. 但是,如果之前字符串有值呢,结尾可能不是\0的.这样字符串就连一块了.
		// 即StrSize为length为目标宽字符串缓冲区大小
		int HasTrans;
		int StrSize =strlen(pStr);
		int DestSize = StrSize + 1;
		if( StrSize > m_WideBuff.length() )
			m_WideBuff.resize(DestSize);

		
		HasTrans =::MultiByteToWideChar(
			m_pWS->m_SystemPageCode_int,
			NULL,
			pStr,
			StrSize,
			(wchar_t*)m_WideBuff.data(), // 目标宽字符缓冲区
			DestSize); //Size, in characters, of the buffer indicated by lpWideCharStr.

		m_WideBuff =m_WideBuff.c_str();

		if(HasTrans)
			return m_WideBuff.c_str();
		else
			return NULL;
			//*/

	}



	/** 
		保存文件,如果存在创建一个副本.
		比如:C:\Users\Robot_001\Pictures\2700.jpg
		运行后为:C:\Users\Robot_001\Pictures\2700_copy_年月日_毫秒.jpg
		!没有检测文件夹中是否已经有该文件名.

	*/
	const TCHAR* GetCopyPathFileName(const TCHAR* lpszPathName) const
	{

		STRING PathName(lpszPathName);
		int dot_pos =PathName.find_last_of( _T('.') );
		if(dot_pos != -1)
		{
			STRING dest_front =PathName.substr(0, dot_pos-1);
			STRING dest_back =PathName.substr(dot_pos, PathName.length());

			//
			// 
	//		dest_back.sprintf
			
			SYSTEMTIME time;
			RtlZeroMemory(&time, sizeof(time));
			GetLocalTime(&time);
			
			DWORD Count =GetTickCount();

			TCHAR Buff[80];
			_stprintf(Buff, _T("_copy_%d%d%d_%d"), time.wYear, time.wMonth, time.wDay, Count);


			// string, 不够用啊... 少一个formt()
			STRING StopWrap =Buff;
			


			STRING dest =dest_front + StopWrap + dest_back;

			return dest.c_str();
		}

		return NULL;
	}





	/**
		 打开可供输出的控制台.
		 只能打开一个.

		 !注意,在工程为ansi时, 是不能去输出宽字符串的... 
		 执意输出宽字符:比如wcout<<L"1"; wcout<<L"2"; 仅仅能输出1的. 
		 在wcout后再cout还是可以输出的.     


		 stdout是工程ansi,unicode来切换的.
	*/
	void open_debug_console() const
	{
		AllocConsole(); 
		_tfreopen( _T("conout$"), _T("w"), stdout); 
		std::cout<<_T("=============ZL_ENGINE: 运行日志=============")<<std::endl;
	}

	


private:
	string m_AnsiBuff;
	wstring m_WideBuff; 


	
};






END_ZL_NAMESPACE





#endif