
#pragma once

#define BEGINE_ZL_NAMESPACE namespace ZL{
#define END_ZL_NAMESPACE }


#include "comm_base.h"
#include "comm_macro.h"
#include "comm_Error.h"



#include <Windows.h>
#include <WindowsX.h>
#include <tchar.h>

#include "COMM_UTILITY.INL"

#define KEYDOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)








#ifdef UNICODE
#define STRING std::wstring

/*
class STRING : public std::wstring
{

};
*/


#else
#define STRING std::string

/*
class STRING : public std::string
{
	static const int STRING_BUFF_MAX =1024 * 4; //4KB

public:
	STRING():std::string()
	{
		memset(STRING_buff, 0, STRING_BUFF_MAX);
	}

	STRING(char* p):std::string(p){

		memset(STRING_buff, 0, STRING_BUFF_MAX);
	}

	

	void sprintf(char* format, ...)
	{
		va_list	va;
		va_start(va, format);

		::vsnprintf(STRING_buff, STRING_BUFF_MAX, format, va);

		va_end(va);

	}


	char STRING_buff[STRING_BUFF_MAX]; 


};
*/


#endif //end UNICODE



class cSTRING_FRESH
{
public:
	static void fresh(STRING& s)
	{
		s = s.c_str();
	}
};
