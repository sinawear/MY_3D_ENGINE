



#ifndef _ZL_CAMERA_
#define _ZL_CAMERA_



#include "3dparty_ddshow.h"

#include "engine_base.h"

#include <atlbase.h>
#include <atlcom.h>
#include <atltypes.h> //CString


#include <atlcomcli.h>
#include <atlcomtime.h>
#include <atlcommem.h>


#include <comdef.h>
 


#define MYFREEMEDIATYPE(mt)	{if ((mt).cbFormat != 0)		\
					{CoTaskMemFree((PVOID)(mt).pbFormat);	\
					(mt).cbFormat = 0;						\
					(mt).pbFormat = NULL;					\
				}											\
				if ((mt).pUnk != NULL)						\
				{											\
				(mt).pUnk->Release();					\
				(mt).pUnk = NULL;						\
				}}	


// 使用ddshow
class cCamera
{
public:

	cCamera()
	{
		::CoInitialize(NULL);

		m_bConnected =false;
	}

	~cCamera()
	{
		
		CloseCamera();
		::CoUninitialize();
	}

	
	/**
		得到机器摄像头数量.
	*/
	int GetCameraCount() const 
	{
		int count =0;

		// enumerate all video capture devices
		CComPtr<ICreateDevEnum> pCreateDevEnum;
		HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
			IID_ICreateDevEnum, (void**)&pCreateDevEnum);

		CComPtr<IEnumMoniker> pEm;
		pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEm, NULL);

		if(pEm)
		{
			pEm->Reset();


			ULONG cFetched;
			IMoniker *pM;
			while(hr = pEm->Next(1, &pM, &cFetched), hr==S_OK)
			{
				pM->Release(); // 不Release()也没有什么..
				count++;
	
			}

		}


		return count;
	}
	
	_bstr_t m_GetCameraName_bstr;
	/**
		得到摄像头名称.
	*/
	wchar_t* GetCameraName(int nCamID)
	{
		int count =0;
		if(m_GetCameraName_bstr.length() > 0)
			m_GetCameraName_bstr.Detach();

		// enumerate all video capture devices
		CComPtr<ICreateDevEnum> pCreateDevEnum;
		HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
			IID_ICreateDevEnum, (void**)&pCreateDevEnum);

		CComPtr<IEnumMoniker> pEm;
		pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEm, NULL);

		if(pEm)
		{
			pEm->Reset();


			ULONG cFetched;
			IMoniker* pM;
			while(hr = pEm->Next(1, &pM, &cFetched), hr==S_OK)
			{
				if (count++ == nCamID)
				{
					CComPtr<IPropertyBag> pBag;
					hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
					if(SUCCEEDED(hr))
					{
						VARIANT var;
						var.vt = VT_BSTR;
						hr = pBag->Read(L"FriendlyName", &var, NULL); //还有其他属性,像描述信息等等...
						if(hr == NOERROR)
						{
							//获取设备名称			
						//	var.bstrVal
							m_GetCameraName_bstr =var.bstrVal;

							SysFreeString(var.bstrVal);				
						}

					}

					pM->Release();
					break;
				}

			}

		}


		return m_GetCameraName_bstr;
	}


	long m_nBufferSize;

	/**
		得到摄像头缓存数据.
	*/
	void GetBuff(char* ImageData)
	{
		

		long evCode;
		long size = 0;

		m_pMediaControl->Run();
		m_pMediaEvent->WaitForCompletion(INFINITE, &evCode);

		m_pSampleGrabber->GetCurrentBuffer(&size, NULL);

		//if the buffer size changed
		if (size != m_nBufferSize)
		{
			
			m_nBufferSize = size; 

		}

		m_pSampleGrabber->GetCurrentBuffer(&m_nBufferSize, (long*)ImageData);
		
	}

	/**
		 得到摄像头缓存大小. 
	*/
	long GetBuffSize() //要在Run()调用之后
	{
		long size ;   //没有Run().不行. 得不到当前缓冲.
		long evCode;
		m_pMediaControl->Run();
		m_pMediaEvent->WaitForCompletion(INFINITE, &evCode);

		m_pSampleGrabber->GetCurrentBuffer(&size, NULL);

		return size;
	}


	/**
		 
	*/
	int GetWidth() { return m_nWidth; }


	/**
		 
	*/
	int GetHeight() { return m_nHeight; }
	



	/**
		 关闭摄像头，析构函数会自动调用这个函数
	*/
	void CloseCamera()
	{
		if(m_bConnected){
			m_bConnected =false;
			m_pMediaControl->Stop();
		}


	}

//	bool Cap


	/**
		@param bDisplayProperties: 是否显示属性设置对话框. 如果是, nWidth和nHeight将不用设置.
	*/
	bool OpenCamera(int nCamID, bool bDisplayProperties, int nWidth =320, int nHeight =240)
	{

		HRESULT hr;

		// IGraphBuilder CLSID_FilterGraph
		m_GraphBuilder.CoCreateInstance(CLSID_FilterGraph);	


		// IBaseFilter CLSID_SampleGrabber
		m_SampleGrabberFilter.CoCreateInstance(CLSID_SampleGrabber);
		m_pSampleGrabber =m_SampleGrabberFilter;

		// IBaseFilter CLSID_NullRenderer
		m_NullRendererFilter.CoCreateInstance(CLSID_NullRenderer);

		
		// 
		m_pMediaControl = m_GraphBuilder;
		m_pMediaEvent = m_GraphBuilder;


		m_GraphBuilder->AddFilter(m_NullRendererFilter, L"NullRenderer");

	
		AM_MEDIA_TYPE   mt;
		ZeroMemory(&mt, sizeof(AM_MEDIA_TYPE));
		mt.majortype = MEDIATYPE_Video;
		mt.subtype = MEDIASUBTYPE_RGB24;
		mt.formattype = FORMAT_VideoInfo; 
		hr = m_pSampleGrabber->SetMediaType(&mt);
		MYFREEMEDIATYPE(mt);


		m_GraphBuilder->AddFilter(m_SampleGrabberFilter, L"Grabber");


		// Bind Device Filter.  We know the device because the id was passed in
		BindFilter(nCamID, &m_pDeviceFilter);
		m_GraphBuilder->AddFilter(m_pDeviceFilter, NULL);

		
		//
		CComPtr<IEnumPins> pEnum;
		m_pDeviceFilter->EnumPins(&pEnum);

		hr = pEnum->Reset();
		hr = pEnum->Next(1, &m_pCameraOutput, NULL); 

		pEnum = NULL; 
		m_SampleGrabberFilter->EnumPins(&pEnum);
		pEnum->Reset();
		hr = pEnum->Next(1, &m_pGrabberInput, NULL); 

		pEnum = NULL;
		m_SampleGrabberFilter->EnumPins(&pEnum);
		pEnum->Reset();
		pEnum->Skip(1);
		hr = pEnum->Next(1, &m_pGrabberOutput, NULL); 

		pEnum = NULL;
		m_NullRendererFilter->EnumPins(&pEnum);
		pEnum->Reset();
		hr = pEnum->Next(1, &m_pNullInputPin, NULL);

		//
		if (bDisplayProperties) 
		{
			CComPtr<ISpecifyPropertyPages> pPages;

			HRESULT hr = m_pCameraOutput->QueryInterface(IID_ISpecifyPropertyPages, (void**)&pPages);
			if (SUCCEEDED(hr))
			{
				PIN_INFO PinInfo;
				m_pCameraOutput->QueryPinInfo(&PinInfo);

				CAUUID caGUID;
				pPages->GetPages(&caGUID);

				OleCreatePropertyFrame(NULL, 0, 0,
					L"Property Sheet", 1,
					(IUnknown **)&(m_pCameraOutput.p),
					caGUID.cElems,
					caGUID.pElems,
					0, 0, NULL);
				CoTaskMemFree(caGUID.pElems);
				PinInfo.pFilter->Release(); 
			}
			pPages = NULL;
		}
		else	//加入由 lWidth和lHeight设置的摄像头的宽和高
		{
			int _Width = nWidth, _Height = nHeight;
			IAMStreamConfig*   iconfig; 
			iconfig = NULL;
			hr = m_pCameraOutput->QueryInterface(IID_IAMStreamConfig,   (void**)&iconfig);   

			AM_MEDIA_TYPE* pmt;    
			if(iconfig->GetFormat(&pmt) !=S_OK) //得到设置 
			{
				//printf("GetFormat Failed ! \n");
				return   false;   
			}

			VIDEOINFOHEADER*   phead;
			if ( pmt->formattype == FORMAT_VideoInfo)   
			{   
				phead=( VIDEOINFOHEADER*)pmt->pbFormat;   
				phead->bmiHeader.biWidth = _Width;   
				phead->bmiHeader.biHeight = _Height;   
				if(( hr=iconfig->SetFormat(pmt)) != S_OK ) //设置   
				{
					return   false;
				}

			}   

			iconfig->Release();   
			iconfig=NULL;   
			MYFREEMEDIATYPE(*pmt);
		}


		hr = m_GraphBuilder->Connect(m_pCameraOutput, m_pGrabberInput);
		hr = m_GraphBuilder->Connect(m_pGrabberOutput, m_pNullInputPin);

		if (FAILED(hr))
		{
			switch(hr)
			{
			case VFW_S_NOPREVIEWPIN :
				break;
			case E_FAIL :
				break;
			case E_INVALIDARG :
				break;
			case E_POINTER :
				break;
			}
		}


		m_pSampleGrabber->SetBufferSamples(TRUE);
		m_pSampleGrabber->SetOneShot(TRUE);

		hr = m_pSampleGrabber->GetConnectedMediaType(&mt);
		if(FAILED(hr))
			return false;


		VIDEOINFOHEADER *videoHeader;
		videoHeader = reinterpret_cast<VIDEOINFOHEADER*>(mt.pbFormat);
		m_nWidth = videoHeader->bmiHeader.biWidth;
		m_nHeight = videoHeader->bmiHeader.biHeight;
		m_bConnected = true;

		pEnum = NULL;
		return true;

		
	}

public:
	bool m_bConnected;
	int m_nWidth;
	int m_nHeight;


protected:
	bool BindFilter(int nCamID, IBaseFilter **pFilter)
	{
		if (nCamID < 0)
			return false;

		// enumerate all video capture devices
		CComPtr<ICreateDevEnum> pCreateDevEnum;
		HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
			IID_ICreateDevEnum, (void**)&pCreateDevEnum);
		if (hr != NOERROR)
		{
			return false;
		}

		CComPtr<IEnumMoniker> pEm;
		hr = pCreateDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory,
			&pEm, 0);
		if (hr != NOERROR) 
		{
			return false;
		}

		pEm->Reset();
		ULONG cFetched;
		IMoniker *pM;
		int index = 0;
		while(hr = pEm->Next(1, &pM, &cFetched), hr==S_OK, index <= nCamID)
		{
			IPropertyBag *pBag;
			hr = pM->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
			if(SUCCEEDED(hr)) 
			{
				VARIANT var;
				var.vt = VT_BSTR;
				hr = pBag->Read(L"FriendlyName", &var, NULL);
				if (hr == NOERROR) 
				{
					if (index == nCamID)
					{
						pM->BindToObject(0, 0, IID_IBaseFilter, (void**)pFilter);
					}
					SysFreeString(var.bstrVal);
				}
				pBag->Release();
			}
			pM->Release();
			index++;
		}

		pCreateDevEnum = NULL;
		return true;
	}


private:
	CComPtr<IGraphBuilder> m_GraphBuilder;
	CComPtr<IBaseFilter> m_SampleGrabberFilter;
	CComPtr<IBaseFilter> m_NullRendererFilter;
	CComPtr<IBaseFilter> m_pDeviceFilter;

	CComPtr<IMediaControl> m_pMediaControl;
	CComPtr<IMediaEvent> m_pMediaEvent;

	CComPtr<ISampleGrabber> m_pSampleGrabber;


	CComPtr<IPin> m_pCameraOutput;
	CComPtr<IPin> m_pGrabberInput;
	CComPtr<IPin> m_pGrabberOutput;
	CComPtr<IPin> m_pNullInputPin;


};







#endif