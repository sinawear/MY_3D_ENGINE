

#ifndef _COMM_ERROR_
#define _COMM_ERROR_



#include "comm_base.h"


BEGINE_ZL_NAMESPACE

class cError
{
public:
	cError(char* Text)
	{
		m_Str =string(Text);
	}

	const char* getData()
	{
		return m_Str.data();
	}

	void print()
	{
		cout<<m_Str<<endl;
	}

protected:
	string m_Str;
};

END_ZL_NAMESPACE



#endif 