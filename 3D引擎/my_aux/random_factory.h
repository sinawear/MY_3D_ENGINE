//2011-8-18 15:31

// 随机数产生工厂

#ifndef _RANDOM_FACTORY_
#define _RANDOM_FACTORY_

#include "engine_base.h"


BEGINE_ZL_NAMESPACE


// 0) 初始化随机数生成器种子

inline	void	my_srand()
{
	srand( (unsigned)time( NULL ) );
}

// 1) 控制在【0，0x7fff】整数范围内

inline int my_rand()
{
	return rand();
}


// 2）得到给定范围内的浮点数

inline float GetRandomFloat(float lowBound, float highBound)
{
	if( lowBound >= highBound ) // bad input
		return lowBound;
	
	// get random float in [0, 1] interval
	float f = (my_rand() % 10000) * 0.0001f; 
	
	// return float in [lowBound, highBound] interval. 
	return (f * (highBound - lowBound)) + lowBound; 
}


// 4）得到给定范围内的随机整形[x,y]
inline int	GetRandomInt(int low, int height)
{
	return (low + rand()%(height - low));
}

END_ZL_NAMESPACE


#endif