

#ifndef ZL_UNITY
#define ZL_UNITY

#include "engine_base.h"


#include <shlobj.h>
#pragma comment(lib, "shell32.lib")


BEGINE_ZL_NAMESPACE

/************************************************************************/
/*        16:09 2013/4/20          code by: mtf                                                   */
/************************************************************************/

class cUnity
{
	
public:
	cUnity() 
	{
//		m_strSystemFolder.resize(MAX_PATH*2);

//		m_pSystemFolder = new char[MAX_PATH * 2];
	}
	~cUnity() 
	{
//		SAFE_DELETE(m_pSystemFolder);
	}


	/**
		得到系统内置文件夹. "桌面" "我的图片" "我的音乐"
		http://baike.baidu.com/view/3143145.htm
	*/
	bool GetSomeSystemFolder(int f)
	{	
		// 函数说明:	http://baike.baidu.com/view/3449405.htm 
		//  TRUE if successful; otherwise, FALSE.
		string t;
		t.resize(MAX_PATH*2);
		BOOL b =SHGetSpecialFolderPath(NULL, (char*)t.data(), f, FALSE);
		m_strSystemFolder =t;
		m_strSystemFolder +="\\";  // 竟无法加上去


		return b == TRUE; 
	}

	/**
		得到"我的图片"的地址.
	*/
	const char* GetMyPicFolder()
	{
		if(GetSomeSystemFolder(CSIDL_MYPICTURES))
			return m_strSystemFolder.c_str();

		return NULL;
	}
	

	/** 
		保存文件,如果存在创建一个副本.
	*/


	/**
		 打开可供输出的控制台.
		 只能打开一个.
	*/
	void open_debug_console() const
	{
		AllocConsole();
		freopen("conout$", "w", stdout); 
		std::cout<<"==============运行日志==========="<<std::endl;
	}

	


private:
	string m_strSystemFolder; 

//	char* m_pSystemFolder;
};






END_ZL_NAMESPACE





#endif