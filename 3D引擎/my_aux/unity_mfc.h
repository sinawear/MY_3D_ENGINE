
#ifndef UNITY_MFC
#define UNITY_MFC

#include "engine_base.h"


//#include <atlsimpstr.h>

BEGINE_ZL_NAMESPACE


class cUnityMFC
{
public:
	cUnityMFC()
	{

	}

	~cUnityMFC()
	{

	}

	/**
		void CWnd::GetWindowText(CString& rString) const
		{
			ASSERT(::IsWindow(m_hWnd));

	#ifndef _AFX_NO_OCC_SUPPORT
			if (m_pCtrlSite == NULL)
			{
	#endif
				int nLen = ::GetWindowTextLength(m_hWnd);
				::GetWindowText(m_hWnd, rString.GetBufferSetLength(nLen), nLen+1);
				rString.ReleaseBuffer();

	#ifndef _AFX_NO_OCC_SUPPORT
			}
			else
			{
				m_pCtrlSite->GetWindowText(rString);
			}
	#endif
		}
		*/
	CString& ReadFile(const TCHAR* pFileName) 
	{
		CFile DevFile;
	//	CString FileStream;
		if(DevFile.Open(pFileName, CFile::modeRead))
		{
			int Length =DevFile.GetLength();
			DevFile.Read(m_FileStream.GetBufferSetLength(Length), Length);
			m_FileStream.ReleaseBuffer();			


			
		}

		return m_FileStream;
	}

private:
	CString m_FileStream;


};





END_ZL_NAMESPACE

#endif