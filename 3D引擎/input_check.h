//在win7下，切换别的窗口在回来，键盘检查不能正常工作。

#ifndef	_INPUT_CHECK_
#define _INPUT_CHECK_

#define	KEYSIZE	256

#include "engine_base.h"


class cMouseDevice
{
public:
	cMouseDevice(IDirectInput8* pDI, HWND	handle, bool exclusive);
	~cMouseDevice();
	void	shutDown() ;

	void	updateDevice() ;
	bool	isUp(DWORD t) ;
	bool	isDown(DWORD t) ;

	POINT	getPos() 
	{ 
		::GetCursorPos(&m_Point); // 得到屏幕坐标的点
		::ScreenToClient(m_hWnd, &m_Point);// 转换到客户区中的坐标
		return m_Point; 
	}
	
	LONG	getWheelPos() { return m_MouseWheel; }	
	
	///得到鼠标的移动状态，X方向，Y方向。滚轮的移动状态Z方向。相对于上次的偏移距离
	LONG    getState(int t) ; 


	void	clear(){ ZeroMemory(&m_mouseState, sizeof(DIMOUSESTATE)); }

protected:		//该类的成员函数或派生类的成员函数才能访问
	IDirectInputDevice8*	m_pMouseDevice;
	DIMOUSESTATE	m_mouseState;
	DIMOUSESTATE	m_mouseOldState;
	POINT m_Point;
	LONG m_MouseWheel;
	HWND m_hWnd;

};


class cKeyboardDevice
{
public:
	cKeyboardDevice(IDirectInput8* pDI, HWND handle);
	~cKeyboardDevice();
	void	shutDown() ;

	void	updateDevice() ;
	bool	isUp(DWORD t) ;
	bool	isDown(DWORD t) ;
	
	void	clear(){ ZeroMemory(&m_key, KEYSIZE); }
protected:
	IDirectInputDevice8*	m_pKeyDevice;
	BYTE	m_key[KEYSIZE];
	BYTE	m_oldKey[KEYSIZE];
};


class cInputInterface
{
public:
	virtual	void		init() =0;
	virtual	void		updateDevice() =0;
	virtual	bool		isKeyDown(DWORD	t) =0;
	virtual	bool		isKeyUp(DWORD t) =0;
	virtual	bool		isMouseDown(DWORD t) =0;
	virtual bool		isMouseUp(DWORD t) =0;
	virtual POINT		getMousePos() =0;
	virtual LONG		getMouseWheelPos() =0;
	virtual	void		shutDown() =0;
	virtual LONG		getMouseState(int t) =0;
};

class cInputController: public cInputInterface
{
public:
	cInputController(HINSTANCE hinstance, HWND	hwnd, bool MouseExclusive);
	~cInputController();

	void	init();
	void	updateDevice();
	bool	isKeyDown(DWORD t);
	bool	isKeyUp(DWORD t) ;
	bool	isMouseDown(DWORD t) ;
	bool	isMouseUp(DWORD t) ;
	POINT	getMousePos() ;
	LONG	getMouseWheelPos() ;
	void	shutDown() ;
	LONG	getMouseState(int t) ;


protected:
	inline void is_updated() //这点代码量，我都给你省了。
	{
#ifdef _DEBUG
		if(m_NoUpdated)
			MyMsgBox_F(L"没有调用updateDevice.");
#endif
	}

	bool m_NoUpdated;//用于检测是否已调用了updateDevice()。

protected:
	

	IDirectInput8*		m_pDI;
	cMouseDevice*		m_pMouseDevice;
	cKeyboardDevice*	m_pKeyDevice;
	HINSTANCE	m_hinstance;
	HWND		m_hwnd;
};



#endif