// 通过验证。 [5/27/2012 sinawear]
// 更新以应对剪切的处理 [1/3/2012 O.O]
#pragma once



#include "NoXRender.h"
#include "cHeightMap.h"
#include "cFx.h"

#include "cGroundV1.h"

/**
  创建地形，与cTerrain加载和结构相同，区别是需要cGroundV1。
 */
class cTerrainV1
{
public:
	cTerrainV1(){}
	
	void init(cRender* pRender, Terrain terrain,
		string HeightMap, 
		string StoneTex, string GrassTex, string DirtTex,
		string BlendMap,
		float HeightScal, float HeightOffset);
	
	~cTerrainV1();
	
	void draw(D3DXMATRIX& world, D3DXMATRIX& view, D3DXMATRIX& proj,
		D3DXVECTOR3 LightDir =D3DXVECTOR3(0,1,0), 
		float AmbientEssentially =1.0f, 
		D3DXVECTOR3 FogColor =D3DXVECTOR3(0.5f, 0.5f, 0.5f));
	
	// 给出x,z;得到高度y
	float getY(float x, float z){
		return m_pGround->get_light_texGround_sub_mesh_Y(
			&m_HeightMapObject, x, z);
	}

	cGroundV1* getGround(){
		return m_pGround;
	}
	
protected:	
	void shader_draw();
	
protected:
	cHeightMap	m_HeightMapObject;
	cVertexDecl* m_pVertexDecl;
	cFx* m_pFx;
	cGroundV1* m_pGround;
	
public:
	Terrain m_Terrain; //地形结构体
	
};

