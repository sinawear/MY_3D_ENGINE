#pragma once

#include "engine_base.h"

class cVertexBuffer
{
public: 
	cVertexBuffer(IDirect3DDevice9* pDevice)
	{
		_pDevice =pDevice;
	}
	
	~cVertexBuffer()
	{
		SAFE_RELEASE(_pVertexBuff);
	}
	
	HRESULT create_vertex_buff(DWORD Length, DWORD FVF, D3DPOOL Pool =D3DPOOL_MANAGED)
	{
		_Length =Length;

		// 创建顶点缓冲, 
		return _pDevice->CreateVertexBuffer(Length, NULL, FVF, Pool, 
			&_pVertexBuff, NULL);
	}

	IDirect3DVertexBuffer9* get_vertex_buff()
	{
		return _pVertexBuff;
	}

	void lock( 
		VOID** ppbData,
		UINT OffsetToLock =0,
		DWORD Flags =D3DLOCK_READONLY
		)
	{	// _Length为0使全部锁定
		_pVertexBuff->Lock(OffsetToLock, _Length, ppbData, Flags);
	}
	
	void unlock()
	{
		_pVertexBuff->Unlock();
	}

private:
	IDirect3DDevice9* _pDevice;
	IDirect3DVertexBuffer9* _pVertexBuff;
	DWORD _Length;
};


