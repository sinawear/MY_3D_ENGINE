#pragma once

#include "engine_color.h"
//----------------------------------
// Primitive types.
enum PrimType
{
	NULL_TYPE,
	POINT_LIST,
	TRIANGLE_LIST,
	TRIANGLE_STRIP,
	TRIANGLE_FAN,
	LINE_LIST,
	LINE_STRIP
};

struct sD3DStaticBuffer
{
	sD3DStaticBuffer() : vbPtr(0), ibPtr(0), numVerts(0),
		numIndices(0), stride(0), fvf(0),
		primType(NULL_TYPE) {}
	
	LPDIRECT3DVERTEXBUFFER9 vbPtr; 
	LPDIRECT3DINDEXBUFFER9 ibPtr;
	UINT numVerts;
	UINT numFaces;
	UINT numIndices;
	UINT stride;  // ÿ�����ֽ���
	unsigned long fvf;
	PrimType primType;
};

//-------------------------------------
struct sMaterial
{
	sMaterial(){}

	sMaterial(D3DXCOLOR _a, D3DXCOLOR _d, D3DXCOLOR _s, D3DXCOLOR _e, float _p)
	{
		a=_a;
		d=_d;
		s=_s;
		e=_e;
		p=_p;
	}

	D3DXCOLOR a; //ambient
	D3DXCOLOR d; //diffuse
	D3DXCOLOR s; //specular
	D3DXCOLOR e; //emissive
	float p;
};

const sMaterial WHITE_MATERIAL(WHITE, WHITE, WHITE, BLACK, 2);
const sMaterial BLACK_MATERIAL(BLACK, BLACK, BLACK, BLACK, 0);

// define lights
#define LIGHT_POINT        1
#define LIGHT_DIRECTIONAL  2
#define LIGHT_SPOT         3

struct sLight
{
	sLight()
	{

	}
	D3DXVECTOR3	pos;
	D3DXVECTOR3 dir;
	D3DXCOLOR	ambient_color;
	D3DXCOLOR   diffuse_color;
	D3DXCOLOR	specular_color;
	int type;
};

struct sTexture
{
	TCHAR*	name;
	int		width;
	int		height;
	IDirect3DTexture9*	image;
};

// Texture states.
enum TextureFilter
{
	MIN_FILTER,
	MAG_FILTER,
	MIP_FILTER
};

// Texture filtering.
enum FilterType
{
	POINT_TYPE,
	LINEAR_TYPE,
	ANISOTROPIC_TYPE
};
