
/*
未通过宽字符测试.
*/

#pragma	once

#include "engine_base.h"

#define LOG_FILE_NAME "Debug.txt"

/// LOG到文件
class cSystemLog
{
public:
	void	clearLog(char*	fileName );
	void	writeLog(char*	fileName , char* message,...);
	void	appendLog(char* fileName , char* message,...);
	
	void	writeMatrix(char* fileName , D3DXMATRIX&	t);
	void	writeVector(char* fileName , D3DXVECTOR3&	t);

	void	writeMatrixOnce(char* fileName , D3DXMATRIX&	t);
private:
	void	write(char* message, va_list start);

	ofstream	m_file;
};


/// Log到内存 
class EngineMemLog
{
public:
	static void printf_my(const char * Format, ...)
	{
		va_list	va;

		va_start(va, Format);
			write(Format, va);
		va_end(va);
	}

	static void printf_vector(D3DXVECTOR3& t)
	{
		printf("%f,%f,%f\n", t.x, t.y, t.z);
	}
private:
	static void write(const char* Format, va_list start)
	{

		char	buff[1024];
		
		vsprintf_s(buff, 1024, Format, start);
		cout<<buff<<endl;
	}
};
