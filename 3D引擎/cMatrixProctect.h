#pragma once

#include "engine_base.h"

class cMatrixProctect
{
public:
	cMatrixProctect(IDirect3DDevice9* pDevice)
	{
		_pDevice = pDevice;
	}

	//执行_pDevice->GetTransform(D3DTS_WORLD, &_World);
	void push_world( )
	{
		_pDevice->GetTransform(D3DTS_WORLD, &_World);
	}
	
	//执行_pDevice->SetTransform(D3DTS_WORLD, &_World);
	void pop_world()
	{
		_pDevice->SetTransform(D3DTS_WORLD, &_World);
	}

	//执行_pDevice->GetTransform(D3DTS_VIEW, &_View);
	void push_view()
	{
		_pDevice->GetTransform(D3DTS_VIEW, &_View);
	}

	//执行_pDevice->SetTransform(D3DTS_VIEW, &_View);
	void pop_view()
	{
		_pDevice->SetTransform(D3DTS_VIEW, &_View);
	}

	//执行_pDevice->GetTransform(D3DTS_PROJECTION, &_Perspective);
	void push_proj()
	{
		_pDevice->GetTransform(D3DTS_PROJECTION, &_Perspective);
	}

	//执行_pDevice->SetTransform(D3DTS_PROJECTION, &_Perspective);
	void pop_proj()
	{
		_pDevice->SetTransform(D3DTS_PROJECTION, &_Perspective);
	}

	void push_all()
	{
		_pDevice->GetTransform(D3DTS_WORLD, &_World);
		_pDevice->GetTransform(D3DTS_VIEW, &_View);
		_pDevice->GetTransform(D3DTS_PROJECTION, &_Perspective);		
	}

	void pop_all()
	{
		_pDevice->SetTransform(D3DTS_WORLD, &_World);
		_pDevice->SetTransform(D3DTS_VIEW, &_View);
		_pDevice->SetTransform(D3DTS_PROJECTION, &_Perspective);
	}

	D3DXMATRIX& get_world_matrix(){
		return _World;
	}

	D3DXMATRIX& get_view_matrix(){
		return _View;
	}

	D3DXMATRIX& get_proj_matrix(){
		return _Perspective;
	}

		
private:
	D3DXMATRIX _World;
	D3DXMATRIX _View;
	D3DXMATRIX _Perspective;

	IDirect3DDevice9* _pDevice;
};
