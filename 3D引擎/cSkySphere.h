
#pragma once

#include "engine_base.h"

extern IDirect3DDevice9* g_pDevice;

class cSkySphere
{
public:
	~cSkySphere()
	{
		SAFE_RELEASE(m_pCubeTex);
		SAFE_DELETE_ARRAY(m_pSphereVertexIndex);
		SAFE_DELETE_ARRAY(m_pSphereVertex);
	}

	void load(const char* p3DTexName)
	{
		HR(D3DXCreateCubeTextureFromFile(g_pDevice, p3DTexName, &m_pCubeTex));

		//生成球
		ID3DXMesh* pSphere;
		D3DXCreateSphere(g_pDevice, 3e+3, 30, 30, &pSphere, NULL);

		//从球中拷贝顶点和索引数据
		m_NumVertices =pSphere->GetNumVertices();
		m_pSphereVertex =new LightVertex[m_NumVertices];

		LightVertex* pSrc;
		pSphere->LockVertexBuffer(NULL, (void**)&pSrc);
		for(int i =0; i<m_NumVertices; i++)
			m_pSphereVertex[i] =pSrc[i];
		pSphere->UnlockVertexBuffer();

		m_NumFaces =pSphere->GetNumFaces();
		WORD IndexNum =m_NumFaces * 3;
		m_pSphereVertexIndex =new WORD[IndexNum];
		WORD* pIndexSrc;
		pSphere->LockIndexBuffer(NULL, (void**)&pIndexSrc);
		for(int i =0; i<IndexNum; i++)
			m_pSphereVertexIndex[i] = pIndexSrc[i];
		pSphere->UnlockIndexBuffer();


		SAFE_RELEASE(pSphere);
	}

	void draw(D3DXMATRIX& World)
	{
		g_pDevice->SetTransform(D3DTS_WORLD, &World);

		g_pDevice->SetTexture(0, m_pCubeTex);

		g_pDevice->SetFVF(CUBE_TEX_FVF);

		// trilinear filter
		g_pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		g_pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		g_pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);


		g_pDevice->DrawIndexedPrimitiveUP(D3DPT_TRIANGLELIST, 0, m_NumVertices,
			m_NumFaces, m_pSphereVertexIndex, D3DFMT_INDEX16, m_pSphereVertex,
			sizeof(CubeTexFvf));	

	}


protected:
	IDirect3DCubeTexture9* m_pCubeTex;

	DWORD m_NumVertices;
	DWORD m_NumFaces;
	LightVertex* m_pSphereVertex;
	WORD* m_pSphereVertexIndex;

};