
#include "cGround.h"
#include "system_log.h"
#include "cVertexDecl.h"

// 文件型变量
static const int SUBMESH_NUMROW =33; //子网格行数 2^n+1
static const int SUBMESH_NUMCOL =33; //子网格列数 2^n+1
static Terrain s_SubTerrain(SUBMESH_NUMROW, SUBMESH_NUMCOL, 1, 1);

static cSystemLog Log; 


// 初始化地板
void cGround::init_ground(D3DXVECTOR3 Center)
{
	int step =_Terrain.cell_space;
	
	int Color =0;
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;
	
	// 用D9的方法会产生多余的线，从最底端连到了另列的最高端。
	// 不知道，为什么OpenGL不会产生多余的线？
	// 不过即使，D9可以，但，顶点缓冲里会有冗余顶点的。

	//D3DPOOL_DEFAULT用这个Control Debug报性能浪费
	ColorVertex* pV;
	_pDevice->CreateVertexBuffer(sizeof(ColorVertex)*NumVertex, NULL, COLOR_FVF, D3DPOOL_MANAGED,
		&_pVertexBuffer, NULL);		
	_pVertexBuffer->Lock(0, sizeof(ColorVertex)*NumVertex, (void**)&pV, NULL);
	
	
	int h =1; //设计h,k生成黑白交叉的颜色
	for(int z=start_z;z>=end_z;z-=step) //
	{
		int k =0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			if( k % 2 || h%2)
				Color =128;
			else
				Color =0;
			
			pV[VertexCount] =ColorVertex(D3DXVECTOR3(x, 0, z), 
				D3DCOLOR_XRGB(Color,Color,Color));

		//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount].pos, &pV[VertexCount].pos, &t);
		//--

			VertexCount++;
			k++;
		}
		h++;		
	}
	_pVertexBuffer->Unlock();
	
	DWORD* pI;
	DWORD  index=0;
	// 创建索引缓冲,用索引缓冲的冗余换顶点缓冲的冗余。
	_pDevice->CreateIndexBuffer(sizeof(DWORD)*NumTriangle*3, NULL, D3DFMT_INDEX32, 
		D3DPOOL_DEFAULT, &_pIndexBuffer, NULL);
	_pIndexBuffer->Lock(0, sizeof(DWORD)*NumTriangle*3, (void**)&pI, NULL);
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			index+=6;
		}
	}
	_pIndexBuffer->Unlock();
}

// 0默认线框输出，1可以输出点阵，2可以SOLID输出
void cGround::draw_ground(int i)
{
	if(_pVertexBuffer == NULL || _pIndexBuffer == NULL)
		MyMsgBox(L"没有初始化。");

	DWORD t;
	_pDevice->GetRenderState(D3DRS_FILLMODE, &t);
	_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	
	//不用纹理
	_pDevice->SetTexture(0, NULL);

	// 设计点大小
	_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);
	_pDevice->SetRenderState(D3DRS_POINTSIZE, FtoDw(0.01f));

	if(i == 2)
		_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

	_pDevice->SetFVF(COLOR_FVF);
	_pDevice->SetStreamSource(0, _pVertexBuffer, 0, sizeof(ColorVertex));
	_pDevice->SetIndices(_pIndexBuffer);

	if(i == 1)// 输出点阵
		_pDevice->DrawPrimitive(D3DPT_POINTLIST, 0, _Terrain.num_vertex);
	else
		_pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 
			_Terrain.num_vertex, 0, _Terrain.num_triangle);


	// 还原状态
	_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
	_pDevice->SetRenderState(D3DRS_FILLMODE, t);
}

void	cGround::draw_ground(cVertexDecl* pVertexDecl)
{
	
	pVertexDecl->apply();
				
	_pDevice->SetStreamSource(0, _pVertexBuffer, 0, pVertexDecl->get_vertex_stride());
	_pDevice->SetIndices(_pIndexBuffer);

	_pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 
		_Terrain.num_vertex, 0, _Terrain.num_triangle);

}


void cGround::init_color_ground(D3DXVECTOR3 Center /* =Zero */, D3DXCOLOR Color1 /* =GRAY */, D3DXCOLOR Color2 /* =BLACK */)
{
	int step =_Terrain.cell_space;
	
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;
	
	// 用D9的方法会产生多余的线，从最底端连到了另列的最高端。
	// 不知道，为什么OpenGL不会产生多余的线？
	// 不过即使，D9可以，但，顶点缓冲里会有冗余顶点的。
	
	//D3DPOOL_DEFAULT用这个Control Debug报性能浪费
	ColorVertex* pV;
	_pDevice->CreateVertexBuffer(sizeof(ColorVertex)*NumVertex, NULL, COLOR_FVF, D3DPOOL_MANAGED,
		&_pVertexBuffer, NULL);		
	_pVertexBuffer->Lock(0, sizeof(ColorVertex)*NumVertex, (void**)&pV, NULL);
	
	
	int h =1; //设计h,k生成黑白交叉的颜色
	for(int z=start_z;z>=end_z;z-=step) //
	{
		int k =0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			if( k % 2 || h%2)
				pV[VertexCount] =ColorVertex(D3DXVECTOR3(x, 0, z), 
					Color1);
			else
				pV[VertexCount] =ColorVertex(D3DXVECTOR3(x, 0, z), 
					Color2);
					
			
			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount].pos, &pV[VertexCount].pos, &t);
			//--
			
			VertexCount++;
			k++;
		}
		h++;		
	}
	_pVertexBuffer->Unlock();
	
	DWORD* pI;
	DWORD  index=0;
	// 创建索引缓冲,用索引缓冲的冗余换顶点缓冲的冗余。
	_pDevice->CreateIndexBuffer(sizeof(DWORD)*NumTriangle*3, NULL, D3DFMT_INDEX32, 
		D3DPOOL_DEFAULT, &_pIndexBuffer, NULL);
	_pIndexBuffer->Lock(0, sizeof(DWORD)*NumTriangle*3, (void**)&pI, NULL);
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			index+=6;
		}
	}
	_pIndexBuffer->Unlock();
}

void	cGround::init_light_ground(D3DXVECTOR3 Center /* =Zero */)
{
	int step =_Terrain.cell_space;
	
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;
	
	// 用D9的方法会产生多余的线，从最底端连到了另列的最高端。
	// 不知道，为什么OpenGL不会产生多余的线？
	// 不过即使，D9可以，但，顶点缓冲里会有冗余顶点的。
	
	//D3DPOOL_DEFAULT用这个Control Debug报性能浪费
	LightVertex* pV;
	_pDevice->CreateVertexBuffer(sizeof(LightVertex)*NumVertex, NULL, 
		LIGHT_FVF, D3DPOOL_MANAGED, &_pVertexBuffer, NULL);		
	_pVertexBuffer->Lock(0, sizeof(LightVertex)*NumVertex, (void**)&pV, NULL);
	

	for(int z=start_z;z>=end_z;z-=step) //
	{
		for(int x=start_x;x<=end_x;x+=step) //
		{	

			pV[VertexCount] =LightVertex(D3DXVECTOR3(x, 0, z), 
				D3DXVECTOR3(0.0f, 1.0f, 0.0f));
					
			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount]._p, &pV[VertexCount]._p, &t);
			//--
			
			VertexCount++;
		}	
	}
	_pVertexBuffer->Unlock();
	
	DWORD* pI;
	DWORD  index=0;
	// 创建索引缓冲,用索引缓冲的冗余换顶点缓冲的冗余。
	_pDevice->CreateIndexBuffer(sizeof(DWORD)*NumTriangle*3, NULL, D3DFMT_INDEX32, 
		D3DPOOL_DEFAULT, &_pIndexBuffer, NULL);
	_pIndexBuffer->Lock(0, sizeof(DWORD)*NumTriangle*3, (void**)&pI, NULL);
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			index+=6;
		}
	}
	_pIndexBuffer->Unlock();
}

void cGround::init_tex_ground(D3DXVECTOR3 Center /* =Zero */, float Scale)
{
	int step =_Terrain.cell_space;
	
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;
	

	LightTextureVertexV1* pV;
	_pDevice->CreateVertexBuffer(sizeof(LightTextureVertexV1)*NumVertex, NULL, 
		LIGHT_TEXTURE_FVF_V1, D3DPOOL_MANAGED, &_pVertexBuffer, NULL);		
	_pVertexBuffer->Lock(0, sizeof(LightTextureVertexV1)*NumVertex, (void**)&pV, NULL);
	
	int h =0;
	for(int z=start_z;z>=end_z;z-=step) //
	{
		int k=0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			
			pV[VertexCount]._p =D3DXVECTOR3(x, 0, z);
			pV[VertexCount]._n =D3DXVECTOR3(0.0f, 1.0f, 0.0f);
				
			pV[VertexCount]._uv =D3DXVECTOR2(k, h) * Scale;

			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount]._p, &pV[VertexCount]._p, &t);
			//--
			
			VertexCount++;
			k++;
		}	
		h++;
	}

// 	for(int i =0; i<VertsPerRow; i++)
// 	{
// 		for(int j =0; i<VertsPerCol; j++)
// 		{
// 			pV[i*VertsPerCol + j]._uv = D3DXVECTOR2(j, i) ;
// 		}
// 	}
	_pVertexBuffer->Unlock();
	
	DWORD* pI;
	DWORD  index=0;
	// 创建索引缓冲,用索引缓冲的冗余换顶点缓冲的冗余。
	_pDevice->CreateIndexBuffer(sizeof(DWORD)*NumTriangle*3, NULL, D3DFMT_INDEX32, 
		D3DPOOL_DEFAULT, &_pIndexBuffer, NULL);
	_pIndexBuffer->Lock(0, sizeof(DWORD)*NumTriangle*3, (void**)&pI, NULL);
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			index+=6;
		}
	}
	_pIndexBuffer->Unlock();
}

void cGround::init_tex_groundV1(D3DXVECTOR3 Center /* =Zero */)
{
	int step =_Terrain.cell_space;
	
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;
	
	
	LightTextureVertexV1* pV;
	_pDevice->CreateVertexBuffer(sizeof(LightTextureVertexV1)*NumVertex, NULL, 
		LIGHT_TEXTURE_FVF_V1, D3DPOOL_MANAGED, &_pVertexBuffer, NULL);		
	_pVertexBuffer->Lock(0, sizeof(LightTextureVertexV1)*NumVertex, (void**)&pV, NULL);
	
	float nCoordIncSize =1.0f/(CellPerRow*step);
	float vCoordIncSize =1.0f/(CellPerCol*step);

//	float Temp, Temp1;

	float w =CellPerRow;
	float d =CellPerCol;

	float h =0;
	for(int z=start_z;z>=end_z;z-=step) //
	{
		float k=0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			pV[VertexCount]._p =D3DXVECTOR3(x, 0, z);
			pV[VertexCount]._n =D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			
			pV[VertexCount]._uv =D3DXVECTOR2(k*nCoordIncSize, h*vCoordIncSize);

/*1 这两种方法都行，数值完全一样.
			Log.appendLog(LOG_FILE_NAME, 
					"n:%f  v:%f\n",  pV[VertexCount]._uv.x, 
					pV[VertexCount]._uv.y);

			Temp =(pV[VertexCount]._p.x + (0.5f*w)) / w;
			Temp1 =(pV[VertexCount]._p.z - (0.5f*d)) / -d;

			Log.appendLog(LOG_FILE_NAME, 
				"n:%f  v:%f\n",  Temp, Temp1);
1*/

//			pV[VertexCount]._uv.x =(pV[VertexCount]._p.x + (0.5f*w)) / w;
//			pV[VertexCount]._uv.y =(pV[VertexCount]._p.z - (0.5f*d)) / -d;
		
			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount]._p, &pV[VertexCount]._p, &t);
			//--
			
			VertexCount++;
			k++;
		}	
		h++;
	}

	_pVertexBuffer->Unlock();
	
	DWORD* pI;
	DWORD  index=0;
	// 创建索引缓冲,用索引缓冲的冗余换顶点缓冲的冗余。
	_pDevice->CreateIndexBuffer(sizeof(DWORD)*NumTriangle*3, NULL, D3DFMT_INDEX32, 
		D3DPOOL_DEFAULT, &_pIndexBuffer, NULL);
	_pIndexBuffer->Lock(0, sizeof(DWORD)*NumTriangle*3, (void**)&pI, NULL);
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			index+=6;
		}
	}
	_pIndexBuffer->Unlock();
}


void cGround::init_light_tex_ground_mesh(
										 cVertexDecl* pVertexDecl, 
										 cHeightMap * pHeightMap,
										 D3DXVECTOR3 Center /* =Zero */,
										 bool bOptimize /*=true*/)
{
	int step =_Terrain.cell_space;
	
	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;
	
	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;

	float nCoordIncSize =1.0f/(CellPerRow);
	float vCoordIncSize =1.0f/(CellPerCol);

	int FaceNum =NumTriangle;

	// 创建地形网格
	int r =D3DXCreateMesh(FaceNum, NumVertex, D3DXMESH_MANAGED | D3DXMESH_32BIT,
		pVertexDecl->get_vertex_element(), pVertexDecl->get_D9_device(),
		&_pMesh);

	// 加载顶点数据
	LightTextureVertexV1* pV;
	_pMesh->LockVertexBuffer(0, (void**)&pV);
			
	float h =0;
	for(int z=start_z;z>=end_z;z-=step) //
	{
		float k=0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			pV[VertexCount]._p.x =x;
			pV[VertexCount]._p.y =pHeightMap->getHeight(h,k);
			pV[VertexCount]._p.z =z;

			pV[VertexCount]._n =D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			
			pV[VertexCount]._uv =D3DXVECTOR2(k*nCoordIncSize, h*vCoordIncSize);
			
	//		Log.appendLog("纹理坐标.txt", "%f,%f\n", 
	//			pV[VertexCount]._uv.x, pV[VertexCount]._uv.y);
			
			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount]._p, &pV[VertexCount]._p, &t);
			//--
			
			VertexCount++;
			k++;
		}	
		h++;
	}

	_pMesh->UnlockVertexBuffer();


	DWORD* pI;
	DWORD* pAttr;

	// 加载索引数据
	// 先锁先解
	r =_pMesh->LockIndexBuffer(NULL, (void**)&pI);
	r =_pMesh->LockAttributeBuffer(NULL, &pAttr); //属性列表是以三角形为单元

	int AttrIndex =0;
	int index =0;
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;
			
			pAttr[AttrIndex++] =0;

			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;
			
			pAttr[AttrIndex++] =0;

			index+=6;

		}
	}
	r =_pMesh->UnlockIndexBuffer();
	r =_pMesh->UnlockAttributeBuffer();


	// 单位化网格中法线
	D3DXComputeNormals(_pMesh, NULL);

// 优化会改变顶点的存储顺序
	if(bOptimize)
	{
		//  生成邻接信息
		DWORD*  pAdjacency =new DWORD[FaceNum * 3];
		_pMesh->GenerateAdjacency(0.001f, pAdjacency); //epsilon 多近看成一个点
		
		// 优化
		_pMesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE|D3DXMESHOPT_ATTRSORT, 
			pAdjacency, NULL, NULL, NULL);
		
		delete[] pAdjacency;
	}

}

void cGround::draw_light_tex_mesh_Ground()
{
	_pMesh->DrawSubset(0);
}


void cGround::init_light_tex_ground_sub_mesh(cVertexDecl* pVertexDecl,
											 cHeightMap* pHeightMap, 
											 D3DXVECTOR3 Center /* =Zero */)
{
	// 生成整个网格
	init_light_tex_ground_mesh(pVertexDecl, pHeightMap, Center, false);

	// 得到整个地形网格的顶点数据
	IDirect3DVertexBuffer9* pVB;
	_pMesh->GetVertexBuffer(&pVB);

	// 做个顶点容器
	int NumVertexes =_pMesh->GetNumVertices();
	vector<LightTextureVertexV1> GobalMeshVertex(NumVertexes);

	// 将整个顶点的网格数据复制到容器中
	LightTextureVertexV1* pV;
	pVB->Lock(0, 0, (void**)&pV, NULL);
		for(int i =0; i<NumVertexes; i++)
		{
			GobalMeshVertex[i] =pV[i];
		}
	pVB->Unlock();
	
/* 得到父网格的索引数据。没用，子网格要用自己产生的。
	// 得到整个地形网格的索引数据
	IDirect3DIndexBuffer9* pIB;
	_pMesh->GetIndexBuffer(&pIB);

	D3DINDEXBUFFER_DESC	IndexBuffDesc; // 要获取索引的数量还得间接一下
	pIB->GetDesc(&IndexBuffDesc);
	
	// 做个索引容器;!!!注意.size返回的是字节数
	int NumIndexes =IndexBuffDesc.Size/sizeof(WORD);
	vector<WORD> GobalMeshIndex(NumIndexes);

	// 将整个网格索引数据复制到容器中
	WORD* pI;
	pIB->Lock(0, 0, (void**)&pI, NULL);
		for(int i =0; i<NumIndexes; i++)
			GobalMeshIndex[i] =pI[i];
	pIB->Unlock();
*/
	// 释放掉原整张地形网格
	SAFE_RELEASE(_pMesh);
	
	// 整个网格可以分裂多少
	int NumSubRows =_Terrain.NumCellRow / s_SubTerrain.NumCellRow;
	int NumSubPerRow =_Terrain.num_cell_per_row / s_SubTerrain.num_cell_per_row;


	for(int i =0; i<NumSubRows; i++) //y
	{
		for(int j =0; j<NumSubPerRow; j++) //x
		{
			RECT r;
			r.left =i * s_SubTerrain.num_cell_per_row ;
			r.top =j*s_SubTerrain.num_cell_per_col;
			r.right =(i+1) * s_SubTerrain.num_cell_per_row;
			r.bottom =(j+1)*s_SubTerrain.num_cell_per_col;

			sub_mesh(r, GobalMeshVertex, pVertexDecl);
		}
	}

}

void cGround::sub_mesh(RECT& Rect, vector<LightTextureVertexV1>& GobalVertex,
					   cVertexDecl* pVertexDecl)
{
	ID3DXMesh* pMesh;
 	int re =D3DXCreateMesh(s_SubTerrain.num_triangle, s_SubTerrain.num_vertex, 
		D3DXMESH_MANAGED,
		pVertexDecl->get_vertex_element(), pVertexDecl->get_D9_device(),
 		&pMesh);
	Failed_Msg_Box(NoSay, re);

	sBoundBox BoundBox;

	int VertexIndex =0;
	LightTextureVertexV1* pV;
	pMesh->LockVertexBuffer(NULL, (void**)&pV);
		//读取并填充顶点
		for(int i =Rect.top; i<=Rect.bottom; i++) // 循环33次
		{
			for(int j =Rect.left; j<=Rect.right; j++) // 循环33次
			{
				int Index = i*_Terrain.num_vertex_per_row + j;

				pV[VertexIndex++] = GobalVertex[Index];

			}
		}
		
		// 创建包围框 
		D3DXComputeBoundingBox(
			(D3DXVECTOR3*)pV, s_SubTerrain.num_vertex,
			sizeof(LightTextureVertexV1), &BoundBox.min_pos, &BoundBox.max_pos);

	pMesh->UnlockVertexBuffer();

//--索引和属性
	WORD* pI;
	DWORD  index=0;
	DWORD  AttrIndex =0;
	DWORD* pAttr;
	pMesh->LockIndexBuffer(NULL, (void**)&pI);
	pMesh->LockAttributeBuffer(NULL, &pAttr);
	//注意这里，让我调试了好久
	//索引的操作是针对单元格的，
	//索引缓冲溢出了，也不报错
		for(int i =0; i<s_SubTerrain.NumCellRow; i++)
		{
			for(int j =0; j<s_SubTerrain.num_cell_per_row; j++)
			{
				int Size =s_SubTerrain.num_vertex_per_row;
				pI[index+0] = i * Size + j;
				pI[index+1] = i * Size  + j+1;
				pI[index+2] = (i+1) * Size  + j;
				
				pAttr[AttrIndex++] =0;
				
				// 第二个三角形
				pI[index+3] = (i+1) * Size  + j;
				pI[index+4] = i * Size  +j+1;
				pI[index+5] = (i+1) * Size  + j+1;
				
				pAttr[AttrIndex++] =0;
				
				index+=6;
			}
		}	
	pMesh->UnlockIndexBuffer();
	pMesh->UnlockAttributeBuffer();


//--优化

	//  生成邻接信息
	DWORD TriangleNum =pMesh->GetNumFaces();
	DWORD*  pAdjacency =new DWORD[s_SubTerrain.num_triangle * 3];
	int r =pMesh->GenerateAdjacency(0.001f, pAdjacency); //epsilon 多近看成一个点
	
	// 优化
	r =pMesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE|D3DXMESHOPT_ATTRSORT, 
		pAdjacency, NULL, NULL, NULL);
	
	delete[] pAdjacency;	


	_pSubMesh.push_back(pMesh);
	_BoundBoxList.push_back(BoundBox);

}

void cGround::draw_light_tex_sub_mesh_ground()
{
//1/*
	for(vector<ID3DXMesh*>::iterator i = _pSubMesh.begin(); i !=_pSubMesh.end();
	i++)
	{
		(*i)->DrawSubset(0);
	}
//1*/

//	_pSubMesh[0]->DrawSubset(0);
}

float cGround::get_light_texGround_sub_mesh_Y(cHeightMap* pHeightMap,
											  float Row, float Col)
{
	const Terrain& map1 =_Terrain;

	float height;
	float x,z;
	x = map1.width/2.0f + Row;	// 
	z = map1.depth/2.0f - Col;
	
	x /= map1.cell_space;       // 列数
	z /= map1.cell_space;		// 行数
	
	float col_num =floorf(x);   // 得到数值的最小整数形式
	float row_num =floorf(z);
	
	float dx =x - col_num;	// 真是棒
	float dz =z - row_num;
	
	// Grab the heights of the cell we are in.
	// A*--*B
	//  | /|
	//  |/ |
	// C*--*D
	float A =pHeightMap->getHeight(row_num, col_num);
	float B =pHeightMap->getHeight(row_num, col_num+1);
	float C =pHeightMap->getHeight(row_num+1, col_num);
	float D =pHeightMap->getHeight(row_num+1, col_num+1);
	
	if(dz <1.0f -dx)   //在上半个三角形
	{
		float uy = B - A;
		float vy = C - A;
		
		height =A + uy*dx + vy *dz; 
	}
	else	//在下半个三角形
	{
		float uy = C - D;
		float vy = B - D;
		
		height = D + uy*(1.0f-dx) + vy*(1.0f-dz);
	}
	return height;	
}


void cGround::init_ground_mesh(
	IDirect3DDevice9* device, 
	D3DXVECTOR3 Center /* =Zero */,
	bool bOptimize /*=true*/)
{
	int step =_Terrain.cell_space;

	int VertexCount =0;
	int VertsPerRow =_Terrain.num_vertex_per_row;
	int VertsPerCol =_Terrain.num_vertex_per_col;
	int NumVertex =_Terrain.num_vertex;
	int CellPerRow =_Terrain.num_cell_per_row;
	int CellPerCol =_Terrain.num_cell_per_col;
	int NumTriangle =_Terrain.num_triangle;

	int start_x = (int)-_Terrain.width/2;
	int start_z = (int)_Terrain.depth/2;
	int end_x	= (int)_Terrain.width/2;
	int end_z	= (int)-_Terrain.depth/2;

	int FaceNum =NumTriangle;

	cVertexDecl VertexDecl(device);
	VertexDecl.create(COLOR_FVF, sizeof(ColorVertex));

	// 创建地形网格
	int r =D3DXCreateMesh(FaceNum, NumVertex, D3DXMESH_MANAGED | D3DXMESH_32BIT,
		VertexDecl.get_vertex_element(), device,
		&_pMesh);

	// 加载顶点数据
	ColorVertex* pV;
	_pMesh->LockVertexBuffer(0, (void**)&pV);

	float h =0;
	for(int z=start_z;z>=end_z;z-=step) //
	{
		float k=0;
		for(int x=start_x;x<=end_x;x+=step) //
		{	
			pV[VertexCount].pos.x =x;
			pV[VertexCount].pos.y =0;
			pV[VertexCount].pos.z =z;

			pV[VertexCount].color =0;

			//--调整地形整体位置
			D3DXMATRIX t;
			D3DXMatrixTranslation(&t, Center.x, Center.y, Center.z);
			D3DXVec3TransformCoord(&pV[VertexCount].pos, &pV[VertexCount].pos, &t);
			//--

			VertexCount++;
			k++;
		}	
		h++;
	}

	_pMesh->UnlockVertexBuffer();


	DWORD* pI;
	DWORD* pAttr;

	// 加载索引数据
	// 先锁先解
	r =_pMesh->LockIndexBuffer(NULL, (void**)&pI);
	r =_pMesh->LockAttributeBuffer(NULL, &pAttr); //属性列表是以三角形为单元

	int AttrIndex =0;
	int index =0;
	for(int i=0;i<CellPerCol;i++)
	{
		for(int j=0;j<CellPerRow;j++)
		{//单元格中第一个三角形
			pI[index+0] = i * VertsPerRow + j;
			pI[index+1] = i * VertsPerRow  + j+1;
			pI[index+2] = (i+1) * VertsPerRow  + j;

			pAttr[AttrIndex++] =0;

			// 第二个三角形
			pI[index+3] = (i+1) * VertsPerRow  + j;
			pI[index+4] = i * VertsPerRow  +j+1;
			pI[index+5] = (i+1) * VertsPerRow  + j+1;

			pAttr[AttrIndex++] =0;

			index+=6;

		}
	}
	r =_pMesh->UnlockIndexBuffer();
	r =_pMesh->UnlockAttributeBuffer();


	// 单位化网格中法线
	D3DXComputeNormals(_pMesh, NULL);

	// 优化会改变顶点的存储顺序
	if(bOptimize)
	{
		//  生成邻接信息
		DWORD*  pAdjacency =new DWORD[FaceNum * 3];
		_pMesh->GenerateAdjacency(0.001f, pAdjacency); //epsilon 多近看成一个点

		// 优化
		_pMesh->OptimizeInplace(D3DXMESHOPT_VERTEXCACHE|D3DXMESHOPT_ATTRSORT, 
			pAdjacency, NULL, NULL, NULL);

		delete[] pAdjacency;
	}

}