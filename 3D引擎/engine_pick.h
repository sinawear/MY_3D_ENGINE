
#ifndef	_RAY_
#define _RAY_

#include "engine_base.h"
#include "bound.h"

//  射线
typedef	struct Ray 
{
	D3DXVECTOR3	origin;
	D3DXVECTOR3	direction;
}Ray;

// 判断射线是否与球相交
bool	is_ray_cross_bound_sphere(IDirect3DDevice9* device,
						   sBoundSphere& bound_sphere, POINT& point);

// [6/19/2012 sinawear]

class EnginePick
{
public:

	///
	static void getWorldPickingRay(const POINT& ptMousePos, Ray& ray,
		HWND WinHandle, const D3DXMATRIX& matView, const D3DXMATRIX& matProj, const D3DVIEWPORT9& vp)
	{

		D3DXVECTOR3 MousePos(ptMousePos.x, ptMousePos.y, 0);
		D3DXVECTOR3& RayPos = ray.origin;
		D3DXVECTOR3& RayDir = ray.direction;

		// 变换到摄像机坐标
		float x = (2.0f*MousePos.x/vp.Width - 1.0f) / matProj(0,0);
		float y = (-2.0f*MousePos.y/vp.Height + 1.0f) / matProj(1,1);

		RayPos =Zero;
		RayDir.x =x;
		RayDir.y =y;
		RayDir.z =1.0f;

		// 变化到世界坐标
		D3DXMATRIX	matInvView;
		D3DXMatrixInverse(&matInvView, NULL, &matView);
		D3DXVec3TransformCoord(&RayPos, &RayPos, &matInvView); //变换点
		D3DXVec3TransformNormal(&RayDir, &RayDir, &matInvView); //变换向量
		D3DXVec3Normalize(&RayDir, &RayDir); //单位化射线方向

	}

	static BOOL rayIntersectMeshTri(LPD3DXBASEMESH pMesh, const Ray& ray, DWORD& FaceIndex)
	{
		FLOAT u;
		FLOAT v;
		FLOAT Dist;
		LPD3DXBUFFER pAllHits;
		DWORD CountOfHits;

		BOOL bHit =FALSE;
		HRESULT hr =D3DXIntersect(pMesh, &ray.origin, &ray.direction, 
			&bHit, &FaceIndex, &u, &v, &Dist, &pAllHits, &CountOfHits);


		SAFE_RELEASE(pAllHits); 

		return bHit;
	}


	
	static void getWorldPickingRay(D3DXVECTOR3& RayPos, D3DXVECTOR3& RayDir,
		HWND WinHandle, const D3DXMATRIX& matView, const D3DXMATRIX& matProj, const D3DVIEWPORT9& vp)
	{
		POINT MousePos ={0};
		::GetCursorPos(&MousePos); // 得到屏幕坐标的点
		// 转换到客户区中的坐标
		::ScreenToClient(WinHandle, &MousePos);

		// 变换到摄像机坐标
		float x = (2.0f*MousePos.x/vp.Width - 1.0f) / matProj(0,0);
		float y = (-2.0f*MousePos.y/vp.Height + 1.0f) / matProj(1,1);

		RayPos =Zero;
		RayDir.x =x;
		RayDir.y =y;
		RayDir.z =1.0f;

		// 变化到世界坐标
		D3DXMATRIX	matInvView;
		D3DXMatrixInverse(&matInvView, NULL, &matView);
		D3DXVec3TransformCoord(&RayPos, &RayPos, &matInvView); //变换点
		D3DXVec3TransformNormal(&RayDir, &RayDir, &matInvView); //变换向量
		D3DXVec3Normalize(&RayDir, &RayDir); //单位化射线方向

	}

	static BOOL rayIntersectMeshTri(LPD3DXBASEMESH pMesh, 
		const D3DXVECTOR3* pRayPos, const D3DXVECTOR3* pRayDir, DWORD& FaceIndex)
	{
		FLOAT u;
		FLOAT v;
		FLOAT Dist;
		LPD3DXBUFFER pAllHits;
		DWORD CountOfHits;

		BOOL bHit =FALSE;
		HRESULT hr =D3DXIntersect(pMesh, pRayPos, pRayDir, 
			&bHit, &FaceIndex, &u, &v, &Dist, &pAllHits, &CountOfHits);


		SAFE_RELEASE(pAllHits); 

		return bHit;
	}

};


#endif
