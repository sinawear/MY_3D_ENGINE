// [5/25/2012 sinawear]

#ifndef UV_GENERATE_H
#define UV_GENERATE_H

#include "cVertexDecl.h"

/**
  用来生成为球或圆柱网格的UV。
  由于，填充顶点缓冲的时候需要一个类型，因此只能显示写出，通过函数名区别，以应对
  以后的扩展。
 */
class f_cUVGenerate
{
public:
	/**
	 将球网格顶点格式升级为:D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1

	 @param
		pWantVertexEle要变换成，的顶点格式。
	 */
	static void sphere_LightTex(IDirect3DDevice9* pDevice, 
		ID3DXMesh*& pSphere, D3DVERTEXELEMENT9* pWantVertexELe)
	{
		// D3DXCreate* functions generate vertices with position
		// and normal data. But for texturing, we also need
		// tex-coords. So clone the mesh to change the vertex
		// format to a format with tex-coords.
		int r;
		ID3DXMesh* temp = NULL;
		r =pSphere->CloneMesh(D3DXMESH_SYSTEMMEM,
			pWantVertexELe, pDevice, &temp);
		Failed_Msg_Box("CloneMesh", r);

		// 释放掉原有的网格
		SAFE_RELEASE(pSphere);

		// Now generate texture coordinates for each vertex.
		LightTextureVertexV1* vertices = 0;  // 
		temp->LockVertexBuffer(0, (void**)&vertices);

		for(UINT i = 0; i < temp->GetNumVertices(); ++i)
		{
			// Convert to spherical coordinates.
			D3DXVECTOR3 p = vertices[i]._p;

			float theta = atan2f(p.z, p.x);
			float phi   = acosf(p.y / sqrtf(p.x*p.x+p.y*p.y+p.z*p.z));

			// Phi and theta give the texture coordinates, but are
			// not in the range [0, 1], so scale them into that range.

			float u = theta / (2.0f*D3DX_PI);
			float v = phi   / D3DX_PI;

			// Save texture coordinates.

			vertices[i]._uv.x = u;
			vertices[i]._uv.y = v;
		}
		temp->UnlockVertexBuffer();

		// Clone back to a hardware mesh.
		// 克隆回到原来的Mesh;
		temp->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY,
			pWantVertexELe, pDevice, &pSphere);

		SAFE_RELEASE(temp);
	}

	/**
	 将圆柱体网格顶点格式升级为:D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 

	 @param
	 axis =2;表示Z轴，圆柱体是沿着Z轴为长度的。
	 */
	static void Cylinder_LightTex(IDirect3DDevice9* pDevice, 
		ID3DXMesh*& pCylinder, D3DVERTEXELEMENT9* p, int axis =2)
	{
		// D3DXCreate* functions generate vertices with position
		// and normal data. But for texturing, we also need
		// tex-coords. So clone the mesh to change the vertex
		// format to a format with tex-coords.

		ID3DXMesh* temp = 0;
		pCylinder->CloneMesh(D3DXMESH_SYSTEMMEM,
			p, pDevice, &temp);

		SAFE_RELEASE(pCylinder);

		// Now generate texture coordinates for each vertex.
		LightTextureVertexV1* vertices = 0;
		temp->LockVertexBuffer(0, (void**)&vertices);

		// We need to get the height of the cylinder onto which we are
		// projecting the vertices. That height depends on the axis on
		// which the client has specified that the cylinder lies. The
		// height is determined by finding the height of the bounding
		// cylinder on the specified axis.

		D3DXVECTOR3 maxPoint(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		D3DXVECTOR3 minPoint(FLT_MAX, FLT_MAX, FLT_MAX);

		for(UINT i = 0; i < temp->GetNumVertices(); ++i)
		{

			D3DXVec3Maximize(&maxPoint, &maxPoint, &vertices[i]._p);
			D3DXVec3Minimize(&minPoint, &minPoint, &vertices[i]._p);
		}

		float a = 0.0f;
		float b = 0.0f;
		float h = 0.0f;
		switch( axis )
		{
		case 0: //x
			a = minPoint.x;
			b = maxPoint.x;
			h = b-a;
			break;
		case 1: //y
			a = minPoint.y;
			b = maxPoint.y;
			h = b-a;
			break;
		case 2: //z
			a = minPoint.z;
			b = maxPoint.z;
			h = b-a;
			break;
		}

		// Iterate over each vertex and compute its texture coordinate.
		for(UINT i = 0; i < temp->GetNumVertices(); ++i)
		{
			// Get the coordinates along the axes orthogonal to the
			// axis with which the cylinder is aligned.

			float x = 0.0f;
			float y = 0.0f;
			float z = 0.0f;
			switch( axis )
			{
			case 0: //x
				x = vertices[i]._p.y;
				z = vertices[i]._p.z;
				y = vertices[i]._p.x;
				break;
			case 1: //y
				x = vertices[i]._p.x;
				z = vertices[i]._p.z;
				y = vertices[i]._p.y;
				break;
			case 2: //z
				x = vertices[i]._p.x;
				z = vertices[i]._p.y;
				y = vertices[i]._p.z;
				break;
			}

			// Convert to cylindrical coordinates.

			float theta = atan2f(z, x);
			float y2    = y - b; // Transform [a, b]-->[-h, 0]

			// Transform theta from [0, 2*pi] to [0, 1] range and
			// transform y2 from [-h, 0] to [0, 1].

			float u = theta / (2.0f*D3DX_PI);
			float v = y2 / -h;

			// Save texture coordinates.
			vertices[i]._uv.x = u;
			vertices[i]._uv.y = v;
		}

		temp->UnlockVertexBuffer();

		// Clone back to a hardware mesh.
		HRESULT hr =temp->CloneMesh(D3DXMESH_MANAGED | D3DXMESH_WRITEONLY,
			p, pDevice, &pCylinder);

		SAFE_RELEASE(temp);
	}

};

#endif