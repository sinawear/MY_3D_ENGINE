#ifndef	_RENDER_
#define _RENDER_

#include "engine_base.h"
#include "engine_fvf.h"
#include "engine_GUI.h"
#include "engine_color.h"

// 截取数据结构到该头文件
#include "render_struct.h"

//  
//  
class cRender
{
public:
	cRender();
	~cRender();

	// D9设备系列
	IDirect3DDevice9* init_d3d9_device(HWND window_handle, int width, int height, BOOL windowed =TRUE);
	IDirect3DDevice9* getDevice9(){
		return m_pDevice;
	}

	// 渲染系列
	void setClearColor(D3DCOLOR	temp);
	void startRender(bool bColor, bool bDepth, bool bStencil);
	void clearBuff(bool bColor, bool bDepth, bool bStencil);
	void endRender();
	void init_render_state();
	void drawGeometry(sD3DStaticBuffer&	 buffer_dsc);
//--增强

	// 
	void drawGeometry(IDirect3DVertexBuffer9* pVB, 
					IDirect3DIndexBuffer9* pIB,
					DWORD Stride, DWORD NumVertices,
					DWORD PrimCount, 
					D3DPRIMITIVETYPE PrimType =D3DPT_TRIANGLELIST
		);

	// 材质系列
	void setMaterial(const sMaterial& m);
	void disableMaterial();

	// 灯光系列
	void setLight(sLight& l, int index);
	void disableLight();

	// 纹理系列
	void addTexture(const TCHAR* name, int& texID);
	void setTextureFilter(int index, int filter, int type);
	void setMultiTexture();
	void setDetailMapping();
	void applyTexture(int index, int texID);
	void saveScreenShot();
	IDirect3DTexture9* getTexture(int TexID);
	IDirect3DSurface9* getTextureSuface(int TexID);
	sTexture* getDIYTexStruct(int TexStructID);

	// 
	D3DSURFACE_DESC& getTextureInfo(int TexID);
	
	int addTexture(const TCHAR* Name); // Update!

	// 字体系列
	void createFont(TCHAR* name, int width, int height, int& ID, BOOL italic =FALSE);
//--未来应设计成内部控制x，y
	void displayText(int id, long x, long y, D3DCOLOR color, TCHAR* text,...);


	// 矩阵系列
	void setWorldMatrix(D3DXMATRIX* temp);
	void setViewMatrix(D3DXMATRIX* temp);
	void calculateProjMatrix(float fov, float n, float f);

	// 释放系列
	void ReleaseTex(int TexID);
	void ReleaseFont(int FontID);
	void ReleaseAllTex();
	void ReleaseAllFont();
	void shutDownALLXmodels();

	// 精灵系列
// 	void createSprite();
// 	void begine(DWORD Flags);
// 	void end();
// 	void draw();

	void shutDown();
	
// 增强
//	void debug_text(int FontID, char* Message, ...);

private:
	void generate_matrial(D3DMATERIAL9& mtrl, const sMaterial& m);

	void generate_direction_light(D3DLIGHT9& dir_light, D3DVECTOR&	dir, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

	void generate_point_light(D3DLIGHT9& point_light, D3DVECTOR& pos, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

	void generate_spot_ligth(D3DLIGHT9&	spot_light, D3DVECTOR&	pos, D3DVECTOR& dir, 
		D3DXCOLOR&	ambient_color, D3DXCOLOR&  diffuse_color, D3DXCOLOR&	specular_color);

private:
	D3DCOLOR			m_clearColor;
	HWND				m_handle;
	BOOL				m_windowed;
	bool				m_rendering;
	int					m_windowWidth;
	int					m_windowHeight;
	IDirect3DDevice9*	m_pDevice;

	sTexture*			m_pTexList;
	int					m_totalTextures;

	LPD3DXFONT*			m_ppFont;
	int					m_totalFonts;

	D3DXMATRIX			m_projection;

#define MAX_SPRITE_NUM 1024
	ID3DXSprite*		m_pSprite[MAX_SPRITE_NUM];
	int					m_totalSprites;

};


bool	createRender(cRender** _render);

#endif


