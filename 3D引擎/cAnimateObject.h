#pragma once

#include "engine_base.h"
#include "skinnedMesh.h"

#include "D9_debug.h"

#define SALUTE 0
#define AIM 1
#define STILL 2
#define WALK 3

class cAnimateObject
{
public:
	cAnimateObject(){ 
		m_isEndLineAnima =true;
	
	}	
	~cAnimateObject(){ SAFE_DELETE(m_pLightFX);}

	void load(IDirect3DDevice9* pDevice, char* xFileName, char* FXName, D3DXVECTOR3 InitPos)
	{
		ID3DXEffect* pEffect;
		m_pLightFX =new cFx(pDevice);
		m_pLightFX->load_fx(FXName);
		pEffect =m_pLightFX->get_effect();

		m_SkinnedMesh.Load(pDevice, pEffect, xFileName);
		m_SkinnedMesh.GetAnimations(m_Animation); 
		m_SkinnedMesh.SetAnimation(m_Animation[SALUTE]);
//////////////////////////////////////////////////////////////////////////
		
		m_StartPos =InitPos;
		m_NewPos =InitPos;
	}
	
	SkinnedMesh* getSkinnedMesh(){return &m_SkinnedMesh;}

	void setAnimation(int i){m_SkinnedMesh.SetAnimation(m_Animation[i]);}

	void setPos(D3DXMATRIX& world, float time){m_SkinnedMesh.SetPose(world, time);}

	void draw(){m_SkinnedMesh.Render(NULL);}

	D3DXMATRIX rotaXFile( D3DXVECTOR3& ClickPos, D3DXVECTOR3& CurrentPos)
	{
		D3DXMATRIX rota;
		D3DXVECTOR3 vDis = ClickPos - CurrentPos;

		float anti_tan =atan2f(vDis.z, vDis.x);
		anti_tan =D3DX_PI + D3DX_PI/2 - anti_tan;
		D3DXMatrixRotationY(&rota, anti_tan);

		return rota;

	}

	void compute(BOOL bClicked, D3DXVECTOR3& ClickPos, float delt_time,D3DXMATRIX& ViewPorj)
	{
		//////////////////////////////////////////////////////////////////////////
		// light position
		ID3DXEffect* pEffect;
		pEffect =m_pLightFX->get_effect();

		D3DXVECTOR4 lightPos(-20.0f, 75.0f, -120.0f, 0.0f);

		pEffect->SetVector("lightPos", &lightPos);
		pEffect->SetMatrix("matVP", &ViewPorj);
//////////////////////////////////////////////////////////////////////////

		D3DXMATRIX SkinnedMeshWorld ;
		D3DXMATRIX SkinTrans =I_MATRIX;
		D3DXMATRIX SkinRe;
		static D3DXMATRIX SkinRota =I_MATRIX;

		// 根据目标点旋转物体
		if(bClicked)
		{
			SkinRota =rotaXFile( ClickPos, m_NewPos);
			setAnimation(WALK);
		}
		

		if(ClickPos != Zero)
		{
			m_NewPos =render_line_animation(m_StartPos, ClickPos, delt_time/1000);
		}

		if(m_isEndLineAnima == false) //没有到第一次点击处，又发生了点击，就改变起始点。
		{
			static int i =0;

			if(i == 1)
				m_StartPos =m_NewPos;

			if(bClicked)
				i =1;
		}	

		D3DXMatrixTranslation(&SkinnedMeshWorld, m_NewPos.x, m_NewPos.y, m_NewPos.z);

		SkinRe =SkinRota * SkinTrans * SkinnedMeshWorld;
		setPos(SkinRe, delt_time/1000);

		if(m_isEndLineAnima)
		{
			D3DXMatrixTranslation(&SkinTrans, m_NewPos.x, m_NewPos.y, m_NewPos.z);
			m_StartPos =m_NewPos;
			setAnimation(AIM);

		}

		
		render.displayText(0, 0, 120, RED, "m_NewPos:%f, %f, %f", m_NewPos.x, m_NewPos.y, m_NewPos.z);
	}

	/// 有点小BUG，以后再看吧
	D3DXVECTOR3&	render_line_animation(D3DXVECTOR3& start,/*[in]*/ D3DXVECTOR3&	end,/*[in]*/
		float EplasedTime/*[in]*/ )
	{
		static bool	onward =true;
		static bool same =false;
		static float deltTime=0;
		static float scale;
		static D3DXVECTOR3 BeforeEnd =Zero;
		static D3DXVECTOR3	pos =Zero;
		static D3DXVECTOR3 BeforeStart =start;

		if(BeforeStart != start) //改变了起始点。要重新计时
		{
			deltTime =0;
		}

		if(BeforeEnd != end) //是否和上一次的end点相同，
			same =false;
		else
			same =true;

		deltTime +=EplasedTime; //秒为单位


		if(onward || same == false)
		{
			D3DXVECTOR3		temp =end - start;
			float length =D3DXVec3Length(&temp);

			scale =deltTime/length;
			// 线性插值
			pos = (end - start) * scale + start;
		}

		if(scale >=1) //到了100%
		{
			onward =false;
			BeforeEnd =end;
			BeforeStart =start;
			deltTime =0;
			m_isEndLineAnima =true;
		}
		else
			m_isEndLineAnima =false;
/*
		//偶尔乱点击会悬停的原因是start，end和上一次的start，end相同。
		render.displayText(font_ID, 0,160, RED, "onward:%d | SAME:%d",  onward, same);
		render.displayText(font_ID, 0,180, RED, "deltTime:%f | scale:%f", deltTime, scale);
		render.displayText(font_ID, 0,40, RED, "BEFORE_START:%f,%f,%f ", BeforeStart.x, BeforeStart.y, BeforeStart.z);
		render.displayText(font_ID, 0,60, RED, "START:%f,%f,%f", start.x, start.y, start.z);
		render.displayText(font_ID, 0,120, RED, "BEFORE_END:%f,%f,%f ", BeforeEnd.x, BeforeEnd.y, BeforeEnd.z);
		render.displayText(font_ID, 0,140, RED, "END:%f,%f,%f", end.x, end.y, end.z);
*/
		return pos;
	}


private:
	D3DXVECTOR3 m_NewPos ;
	D3DXVECTOR3 m_StartPos;
	bool m_isEndLineAnima;
	SkinnedMesh m_SkinnedMesh;
	vector<string> m_Animation;
	cFx* m_pLightFX;
};
