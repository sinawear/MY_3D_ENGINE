// 通过测试 [5/27/2012 sinawear]
#pragma once
#include "engine_base.h"

#ifdef _DEBUG
#define SHADER_DEBUG D3DXSHADER_DEBUG
#else
//没有Release，标签。。
#define SHADER_DEBUG D3DXSHADER_DEBUG //不确定
#endif


class cTerrain;
class cTerrainV1;
/**
  特效类，用于方便使用Shader.
  需要前导词：
  class cTerrain;
  class cTerrainV1;
 */
class cFx
{
public:
	cFx(IDirect3DDevice9* pDevice)
	{
		_pDevice =pDevice;
	}
	~cFx()
	{
		SAFE_RELEASE(_pEffect);
	}

	void load_fx(const char* FileName)
	{
		// 加载编译SHADE 
		int result =0;
		ID3DXBuffer*	pErrorBuff =0;

		// 注意性能，这里的SHADER是DeBUG，应接受标志
		result = D3DXCreateEffectFromFile(_pDevice, FileName, NULL, NULL, \
			SHADER_DEBUG, 
			NULL, &_pEffect, &pErrorBuff);
	
		if( FAILED(result) && pErrorBuff )
		{
			MessageBox(0, (char*)pErrorBuff->GetBufferPointer(), 0, 0);
			exit(0);
		}

			Failed_Msg_Box("D3DXCreateEffectFromFile", result);
		SAFE_RELEASE(pErrorBuff);
	}

	void apply(char* TechniqueName, void (*fuc)())
	{
		D3DXHANDLE hTechnique = _pEffect->GetTechniqueByName( TechniqueName );
		_pEffect->SetTechnique( hTechnique );

		UINT nPasses;
		UINT iPass;
		
		_pEffect->Begin( &nPasses, 0 );
			for( iPass = 0; iPass < nPasses; iPass ++ )
			{
				_pEffect->BeginPass( iPass );
				fuc();
				_pEffect->EndPass();
			}
		_pEffect->End();
	}

	// 针对地形的渲染, -_-汗
	/// (pTerrain->*成员函数指针)(); 忘了是不是自己发明的。
	void apply(char* TechniqueName, cTerrain* pTerrain, void(cTerrain::* fuc)())
	{
		D3DXHANDLE hTechnique = _pEffect->GetTechniqueByName( TechniqueName );
		_pEffect->SetTechnique( hTechnique );
		
		UINT nPasses;
		UINT iPass;
		
		_pEffect->Begin( &nPasses, 0 );
		for( iPass = 0; iPass < nPasses; iPass ++ )
		{
			_pEffect->BeginPass( iPass );
				(pTerrain->*fuc)();  //!
			_pEffect->EndPass();
		}
		_pEffect->End();
	}

	// 。。更新一次多一个呀。
	/// (pTerrain->*成员函数指针)();
	void apply(char* TechniqueName, cTerrainV1* pTerrain, void(cTerrainV1::* fuc)())
	{
		D3DXHANDLE hTechnique = _pEffect->GetTechniqueByName( TechniqueName );
		_pEffect->SetTechnique( hTechnique );
		
		UINT nPasses;
		UINT iPass;
		
		_pEffect->Begin( &nPasses, 0 );
		for( iPass = 0; iPass < nPasses; iPass ++ )
		{
			_pEffect->BeginPass( iPass );
			(pTerrain->*fuc)();  //!
			_pEffect->EndPass();
		}
		_pEffect->End();
	}

	ID3DXEffect* get_effect(){
		return _pEffect;
	}

	void modify_matrix(char* Name, D3DXMATRIX& t)
	{// 由于VS设置不是用总接口设置的保持一致，就写了该函数
		 _r =_pEffect->SetMatrix(Name, &t);
			Failed_Msg_Box("_pEffect->SetMatrix(Name, &t).", _r);
	}

	void modify_float(char* Name, float t)
	{
		 _r =_pEffect->SetFloat(Name, t);
			Failed_Msg_Box("modify_float(char* Name, float t).", _r);
	}

	void modify_vector(char* Name, D3DXVECTOR4& v)
	{
		_r =_pEffect->SetVector(Name, &v);
			Failed_Msg_Box("_pEffect->SetVector(Name, &v)",_r);
	}

	void modify_value(char* Name, const void* p, int Size)
	{
		_r =_pEffect->SetValue(Name, p, Size);
			Failed_Msg_Box("_pEffec->SetValue(Name, p, Size)",_r);
	}

	void set_texture(char* Name, IDirect3DTexture9* pTexture)
	{
		_r =_pEffect->SetTexture(Name, pTexture);
			Failed_Msg_Box("_pEffect->SetTexture",_r);
	}

	void set_float4(char* Name, void* p)
	{
		_r =_pEffect->SetValue(Name, p, sizeof(D3DXCOLOR));
			Failed_Msg_Box("_pEffect->SetValue(Name, p, sizeof(D3DXCOLOR)",_r);
	}

	void set_float_array(char* Name, float* p, int num)
	{
		_r =_pEffect->SetFloatArray(Name, p, num);
			Failed_Msg_Box("_pEffect->SetFloatArray(Name, p, num)",_r);	
	}

	void commitChanges()
	{
		_r =_pEffect->CommitChanges();
	}

	/**
	  @remark
	   如果将shader里的各主要矩阵名固定就能减轻不少负担
	   世界矩阵变了，其他的都要改
	 */
	void updateShaderMatrix(
		D3DXMATRIX& World, D3DXMATRIX& View, D3DXMATRIX Proj,
		char* matWorldName ="g_WorldMatrix", 
		char* matWorldInverseTransposeName ="g_WorldInverseTransposeMatrix", 
		char* matWVPName ="g_WorldViewProjMatrix")
	{
		modify_matrix(matWorldName, World);
		modify_matrix(matWVPName, World*View*Proj);
		
		D3DXMATRIX worldInverseTranspose;
		D3DXMatrixInverse(&worldInverseTranspose, 0, &World);
		D3DXMatrixTranspose(&worldInverseTranspose, &worldInverseTranspose);
		modify_matrix(matWorldInverseTransposeName, worldInverseTranspose);	
	}

	void updateShaderMaterialLight(
		D3DXCOLOR& AmbientLight, D3DXCOLOR& AmbientMatrial,
		D3DXCOLOR& DiffuseLight, D3DXCOLOR& DiffuseMatrial,
		D3DXCOLOR& SpecLight, D3DXCOLOR& SpecMaterial,
		float SpecPow,
		char* AmbientLightName ="g_AmbientLight", 
		char* AmbientMatrialName ="g_AmbientMaterial",
		char* DiffuseLightName ="g_DiffuseLight", 
		char* DiffuseMatrialName ="g_DiffuseMatrial",
		char* SpecLightName ="g_SpecLight", 
		char* SpecMaterialName ="g_SpecMaterial",
		char* SpecPowName ="g_SpecPow")
	{

		modify_vector(AmbientLightName, (D3DXVECTOR4)AmbientLight);
		modify_vector(AmbientMatrialName, (D3DXVECTOR4)AmbientMatrial);
		modify_vector(DiffuseLightName, (D3DXVECTOR4)DiffuseLight);
		modify_vector(DiffuseMatrialName, (D3DXVECTOR4)DiffuseMatrial);
		modify_vector(SpecLightName, (D3DXVECTOR4)SpecLight);
		modify_vector(SpecMaterialName, (D3DXVECTOR4)SpecMaterial);			
		modify_float(SpecPowName, SpecPow);
	
	}

	void setShaderMaterial(
		D3DXCOLOR& AmbientMatrial,
		D3DXCOLOR& DiffuseMatrial,
		D3DXCOLOR& SpecMaterial,
		float SpecPow,
		char* AmbientMatrialName ="g_AmbientMaterial",
		char* DiffuseMatrialName ="g_DiffuseMatrial",
		char* SpecMaterialName ="g_SpecMaterial",
		char* SpecPowName ="g_SpecPow")
	{
		modify_vector(AmbientMatrialName, (D3DXVECTOR4)AmbientMatrial);
		modify_vector(DiffuseMatrialName, (D3DXVECTOR4)DiffuseMatrial);
		modify_vector(SpecMaterialName, (D3DXVECTOR4)SpecMaterial);			
		modify_float(SpecPowName, SpecPow);
	}


protected:
	IDirect3DDevice9* _pDevice;
	ID3DXEffect* _pEffect;

	int _r;
};