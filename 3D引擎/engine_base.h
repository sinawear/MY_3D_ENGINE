
#if _MSC_VER <= 1200
// VC6专用,让for在另一个作用域中。
	#define for if(false){}else for
#endif

#ifdef _DEBUG 
#define D3D_DEBUG_INFO	
#endif

#define DIRECTINPUT_VERSION	0x0800


#include <windows.h>
#include <windowsx.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dX9math.h>
#include <d3dx9core.h>
#include <d3dx9mesh.h>

//DX2010
#include <dxerr.h>

//DX2004
/*
#include <dxerr9.h>
#include<dmusicc.h>
#include<dmusici.h>
*/

#include<cguid.h>


#include <dinput.h>
#include <dsound.h>


#include "comm_base.h"
#include "comm_macro.h"
#include "comm_Error.h"



#include "MAC_CHANGE.h" //模仿微软ANSI和UNICODE函数的转换



#include "utility_F.inl"

#include "D9_comm.h"
#include "D9_macro.INL"
#include "DX9_ERROR.INL"


#include "my_math.h"

/*
//可以正常使用的。不过很畸形。不应该设计在这。
#include "system_log.h"

就像
1.h{
#include "2.h"
}
2.h{
#include "1.h"
}

*/

//................GLOBAL.OBJECT..........................

// 全局对象
class cCameraUVNV1;
extern cCameraUVNV1 g_CameraV1;

//...........................................