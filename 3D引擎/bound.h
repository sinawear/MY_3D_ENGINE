
#ifndef	_BOUND_
#define _BOUND_

#include "engine_base.h"

// 包围球
struct sBoundSphere 
{
	D3DXVECTOR3	center;
	float		radius;
};

// 包围盒
struct  sBoundBox
{
	D3DXVECTOR3		min_pos;
	D3DXVECTOR3		max_pos;

	// center的求法不应该是：
	// min_pos + (max_pos-min_pos)/2 吗？-_-! => min+max/2-min/2=>(max+min)/2;
	D3DXVECTOR3 center() const 
	{
		return (max_pos + min_pos) / 2;
	}

	//
	D3DXVECTOR3 extent() const 
	{
		return (max_pos - min_pos) /2;
	}

	// 不知道哪给的公式
	sBoundBox trans(const D3DXMATRIX& M)
	{
		// Convert to center/extent representation.
		D3DXVECTOR3 c = center();
		D3DXVECTOR3 e = extent();
		
		// Transform center in usual way.
		D3DXVec3TransformCoord(&c, &c, &M);
		
		// Transform extent.
		D3DXMATRIX absM;
		D3DXMatrixIdentity(&absM);
		absM(0,0) = fabsf(M(0,0)); absM(0,1) = fabsf(M(0,1)); absM(0,2) = fabsf(M(0,2));
		absM(1,0) = fabsf(M(1,0)); absM(1,1) = fabsf(M(1,1)); absM(1,2) = fabsf(M(1,2));
		absM(2,0) = fabsf(M(2,0)); absM(2,1) = fabsf(M(2,1)); absM(2,2) = fabsf(M(2,2));
		D3DXVec3TransformNormal(&e, &e, &absM);
		
		sBoundBox out;
		// Convert back to AABB representation.
		out.min_pos = c - e;
		out.max_pos = c + e;

		return out;
	}
};

// cObjectBound构造函数还是要写成指针的形式，在内部对空指针做相应操作
//
class cObjectBound
{
public:
	// 设置是可选的，可以只设置盒子，或球
	cObjectBound(D3DXVECTOR3& min, D3DXVECTOR3& max, D3DXVECTOR3& center, float radius);
	cObjectBound(sBoundBox* box, sBoundSphere* sphere);
	cObjectBound();
	~cObjectBound();
	void	shut_down();

	// 创建包围盒
	void	generate_bound_box( ID3DXMesh* pMeshObject);//生成网格的包围盒	
	void	generate_bound_box(D3DXVECTOR3* pointList, UINT numPoints);//根据顶点列表生成包围盒

	// 创建包围球
	void	generate_bound_sphere( ID3DXMesh* pMeshObject);//生成网格的包围球	

	// 生成包围的网格,可以用于显示的输出网格的样子
	void	bound_box_mesh(IDirect3DDevice9* pDevice);
	void	bound_shpere_mesh(IDirect3DDevice9* pDevice);

	// 设置包围体
	void	set_bound_box(sBoundBox& box);
	void	set_bound_sphere(sBoundSphere&	sphere);

	// 得到包围体
	sBoundBox*	get_bound_box();
	sBoundSphere*	get_bound_sphere();

	// 点是否在包围体内
	bool	isPointInsideBox(D3DXVECTOR3& v);
	bool	isPointInsideSphere(D3DXVECTOR3& v);

	// 判断盒子是否在盒子内
	bool	isBoxInsideBox(sBoundBox&	v);

	// 边界框碰撞检测
	bool	isCollision(sBoundBox& A);

	// 边界球碰撞检测
	bool	isCollision(sBoundSphere A, sBoundSphere B);

	// 变换包围体
	void	transBoundSphere(D3DXMATRIX*	t);
	void	transBoundBox(D3DXMATRIX*	t);

	// 渲染包围体
	void	renderBox(bool WireFrame =true); 
	void	renderSphere();

private:
	sBoundBox			m_boundBox;
	sBoundSphere		m_boundSphere;

	ID3DXMesh*			m_pBoundBoxMesh;
	ID3DXMesh*			m_pBoundSphereMesh;

	IDirect3DDevice9*	m_pDevice;
};

// 生成包围盒的网格,用于显示的输出网格的样子
//void	bound_box_mesh(IDirect3DDevice9* pDevice, sBoundBox& boundBox, ID3DXMesh** ppBoundBoxMesh);


// 生成包围球的网格,用于显示的输出网格的样子
//void	bound_shpere_mesh(IDirect3DDevice9* pDevice, sBoundSphere& boundSphere, ID3DXMesh** ppBoundSphereMesh);


#endif
