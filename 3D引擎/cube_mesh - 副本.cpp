

#include "cube_mesh.h"
#include "engine_fvf.h"

cCubeMesh::cCubeMesh(IDirect3DDevice9* device)
{
	// save a ptr to the device
	_pDevice = device;

	const int VertexNum =24;
	const int FaceNum =12; //Triangle Num
	const int IndexNum =36;

	HRESULT hr =D3DXCreateMeshFVF(FaceNum, VertexNum, D3DXMESH_MANAGED, 
		LIGHT_TEXTURE_FVF, device, &_pMesh);

//////////////////////////////////////////////////////////////////////////

	LightTextureVertex* v;
	_pMesh->LockVertexBuffer(NULL, (void**)&v);

	// build box
	//B C
	//A D
	// fill in the front face vertex data
	v[0] = LightTextureVertex(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f); //A
	v[1] = LightTextureVertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f); //B
	v[2] = LightTextureVertex( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f); //C
	v[3] = LightTextureVertex( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f); //D

	//I J
	//K L
	// fill in the back face vertex data
	v[4] = LightTextureVertex(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f); //K
	v[5] = LightTextureVertex( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f); //L
	v[6] = LightTextureVertex( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f); //J
	v[7] = LightTextureVertex(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f); //I

	//I J
	//B C
	// fill in the top face vertex data
	v[8]  = LightTextureVertex(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f); //B
	v[9]  = LightTextureVertex(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f); //I
	v[10] = LightTextureVertex( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f); //J
	v[11] = LightTextureVertex( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f); //C

	//K L
	//A D
	// fill in the bottom face vertex data
	v[12] = LightTextureVertex(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f); //A
	v[13] = LightTextureVertex( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f); //D
	v[14] = LightTextureVertex( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f); //L
	v[15] = LightTextureVertex(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f); //K

	//E B
	//F A
	// fill in the left face vertex data
	v[16] = LightTextureVertex(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f); //F
	v[17] = LightTextureVertex(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f); //E
	v[18] = LightTextureVertex(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f); //B
	v[19] = LightTextureVertex(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f); //A

	//C G
	//D H
	// fill in the right face vertex data
	v[20] = LightTextureVertex( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f); //D
	v[21] = LightTextureVertex( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f); //C
	v[22] = LightTextureVertex( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f); //G
	v[23] = LightTextureVertex( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f); //H

	_pMesh->UnlockVertexBuffer();

	WORD* i = 0;
	_pMesh->LockIndexBuffer(NULL, (void**)&i);

	// fill in the front face index data
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	// fill in the back face index data
	i[6] = 4; i[7]  = 5; i[8]  = 6;
	i[9] = 4; i[10] = 6; i[11] = 7;

	// fill in the top face index data
	i[12] = 8; i[13] =  9; i[14] = 10;
	i[15] = 8; i[16] = 10; i[17] = 11;

	// fill in the bottom face index data
	i[18] = 12; i[19] = 13; i[20] = 14;
	i[21] = 12; i[22] = 14; i[23] = 15;

	// fill in the left face index data
	i[24] = 16; i[25] = 17; i[26] = 18;
	i[27] = 16; i[28] = 18; i[29] = 19;

	// fill in the right face index data
	i[30] = 20; i[31] = 21; i[32] = 22;
	i[33] = 20; i[34] = 22; i[35] = 23;

	_pMesh->UnlockIndexBuffer();
}

cCubeMesh::~cCubeMesh()
{
	SAFE_RELEASE(_pMesh);
}

bool cCubeMesh::draw(D3DXMATRIX* world, D3DMATERIAL9* mtrl, IDirect3DTexture9* tex)
{
	if( world )
		_pDevice->SetTransform(D3DTS_WORLD, world);

	if( mtrl )
		_pDevice->SetMaterial(mtrl);
	if( tex )
		_pDevice->SetTexture(0, tex);

	_pMesh->DrawSubset(0);


	return true;
}