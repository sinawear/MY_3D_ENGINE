// 通过实验。 [5/27/2012 sinawear]
#pragma once

#include "engine_base.h"
#include "engine_terrain.h"
#include "cVertexDecl.h"
#include "cHeightMap.h"
#include "bound.h"

/**
  地板类，升级版；
  和cGround的区别：
  1）是构造函数的参数。
  2) 可以剔除网格。
 @remark
  内部使用从cCameraV1来剔除不可见的网格块。
  需要的V1系列：cCameraV1。
 */
class cGroundV1
{
public:
	cGroundV1(cVertexDecl* pVertexDecl, const Terrain& t):_Terrain(t)
	{
		_pVertexDecl =pVertexDecl;
		_D_MeshInFrustum =0;		
	}
	
	~cGroundV1()
	{
		for(vector<SubMeshAndBox>::iterator i =_SubGrid.begin();
		i != _SubGrid.end(); i++)
		{
			SAFE_RELEASE(i->mesh);
		}
	}
		
	//--
	/// 初始化整个网格
	void	init_light_tex_ground_sub_mesh(
		cHeightMap* pHeightMap, D3DXVECTOR3 Center =Zero);
	
	/// 渲染子网格
	void	draw_light_tex_sub_mesh_ground();
	
	/// 给出位置得到所处的高度
	float	get_light_texGround_sub_mesh_Y(cHeightMap* pHeightMap,
		float Row, float Col);
	
protected:
	// 将网格分解。
	void	sub_mesh(RECT& Rect, vector<LightTextureVertexV1>& GobalVertex
					   );
	
private:
	cVertexDecl* _pVertexDecl;
	const Terrain& _Terrain; // 地形的结构体描述

	vector<LightTextureVertexV1> _GroundData; //整个地形数据

	vector<SubMeshAndBox> _SubGrid; // 被分解后的地形数据

public:
///用于射线相交使用；存储了经过cameraV1剔除后的子网格。
	vector<ID3DXMesh*> _RayTestMesh;

	///在平截头的网格数。
	int _D_MeshInFrustum;
	///被分割行的块数。
	int _D_NumSubRows;
	///被分割列的块数。
	int _D_NumSubCols;
};



