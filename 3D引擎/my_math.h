

#ifndef		_MY_MATH_
#define		_MY_MATH_

#include <stdio.h>


//百分比宏(percentage)
#define	PERCENTAGE(v, p)	(int)v*p

namespace MYMATH{

/// 将浮点数控制在【0，1】
inline void clamp(float& d)
{

	if( d < 0)
		d =0;
	if( d > 1.0f)
		d =1.0f;
}



/// 判断是否为偶数
inline	bool isEvenNumber(int t)
{
	if(t%2 == 0)
		return true;

	return false;
}



}
#endif