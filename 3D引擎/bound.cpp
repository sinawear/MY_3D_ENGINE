
#include "bound.h"

cObjectBound::cObjectBound(D3DXVECTOR3& min, D3DXVECTOR3& max, D3DXVECTOR3& center, float radius)
{
	m_boundBox.min_pos =min;
	m_boundBox.max_pos =max;

	m_boundSphere.center =center;
	m_boundSphere.radius =radius;
}

cObjectBound::cObjectBound(sBoundBox* box, sBoundSphere* sphere)
{
	m_boundBox =*box;
	m_boundSphere =*sphere;
}

cObjectBound::cObjectBound()
{
	m_pDevice =NULL;
}

cObjectBound::~cObjectBound()
{

}


void	cObjectBound::shut_down()
{
}


void	cObjectBound::generate_bound_box(ID3DXMesh* pMeshObject)
{
	BYTE*	v;

	// 锁定物体
	pMeshObject->LockVertexBuffer(0,(void**)&v);

	D3DXComputeBoundingBox((D3DXVECTOR3*)v,pMeshObject->GetNumVertices(), \
		D3DXGetFVFVertexSize(pMeshObject->GetFVF()),
		&m_boundBox.min_pos,&m_boundBox.max_pos);

	pMeshObject->UnlockVertexBuffer();

}

void	cObjectBound::generate_bound_box(D3DXVECTOR3* pointList, UINT numPoints)
{
	// Loop through all of the points to find the min/max values.
	// ..~
	// ...
	// @..  @:为包围盒最小点； ～：为包围盒最大点
	m_boundBox.min_pos =pointList[0];
	m_boundBox.max_pos =pointList[0];
	
	for(UINT i = 0; i < numPoints; i++)
	{
		if(pointList[i].x < m_boundBox.min_pos.x) m_boundBox.min_pos.x = pointList[i].x;
		if(pointList[i].x > m_boundBox.max_pos.x) m_boundBox.max_pos.x = pointList[i].x;

		if(pointList[i].y < m_boundBox.min_pos.y) m_boundBox.min_pos.y = pointList[i].y;
		if(pointList[i].y > m_boundBox.max_pos.y) m_boundBox.max_pos.y = pointList[i].y;

		if(pointList[i].z < m_boundBox.min_pos.z) m_boundBox.min_pos.z = pointList[i].z;
		if(pointList[i].z > m_boundBox.max_pos.z) m_boundBox.max_pos.z = pointList[i].z;
	}
}

void	cObjectBound::generate_bound_sphere(ID3DXMesh* pMeshObject)
{
	BYTE*	v;

	pMeshObject->LockVertexBuffer(0, (void**)&v);

	D3DXComputeBoundingSphere((D3DXVECTOR3*)v, pMeshObject->GetNumVertices(),
		D3DXGetFVFVertexSize(pMeshObject->GetFVF()),
		&m_boundSphere.center, &m_boundSphere.radius);

	pMeshObject->UnlockVertexBuffer();

}

void	cObjectBound::bound_box_mesh(IDirect3DDevice9* pDevice)
{
	if(m_pDevice ==NULL)
		m_pDevice =pDevice;

	D3DXCreateBox(pDevice,m_boundBox.max_pos.x - m_boundBox.min_pos.x,
		m_boundBox.max_pos.y - m_boundBox.min_pos.y,
		m_boundBox.max_pos.z - m_boundBox.min_pos.z,
		&m_pBoundBoxMesh, NULL);
}

void	cObjectBound::bound_shpere_mesh(IDirect3DDevice9* pDevice)
{
	if(m_pDevice ==NULL)
		m_pDevice =pDevice;

	// 12段
	D3DXCreateSphere(pDevice, m_boundSphere.radius, 12, 12, &m_pBoundSphereMesh, NULL);
}

void	cObjectBound::set_bound_box(sBoundBox& box)
{
	m_boundBox =box;
}

void	cObjectBound::set_bound_sphere(sBoundSphere& sphere)
{
	m_boundSphere =sphere;
}

sBoundBox*	cObjectBound::get_bound_box()
{
	return &m_boundBox;
}

sBoundSphere*	cObjectBound::get_bound_sphere()
{
	return &m_boundSphere;
}

bool	cObjectBound::isPointInsideBox(D3DXVECTOR3& v)
{// 注意不要用<= 或 >=；等于说明还是在里面，用了等于逻辑就混乱了
	
	if(m_boundBox.max_pos.x < v.x) return false;  
	if(m_boundBox.min_pos.x > v.x) return false;
	if(m_boundBox.max_pos.y < v.y) return false;
	if(m_boundBox.min_pos.y > v.y) return false;
	if(m_boundBox.max_pos.z < v.z) return false;
	if(m_boundBox.min_pos.z > v.z) return false;

	return true;	
}

bool cObjectBound::isPointInsideSphere(D3DXVECTOR3& v)
{
	
   // The distance between the two spheres.
   D3DXVECTOR3 intersect = m_boundSphere.center - v;
	
   // Test for collision.
   if(sqrt(intersect.x * intersect.x + intersect.y * intersect.y +
           intersect.z * intersect.z) < m_boundSphere.radius)
      return true;
   
   return false;	
}

bool	cObjectBound::isBoxInsideBox(sBoundBox&	v)
{
	if(isPointInsideBox(v.min_pos) == false) 
		return false;

	if(isPointInsideBox(v.max_pos) == false)
		return false;

	return true;
}

// 边界框碰撞检测
bool	cObjectBound::isCollision(sBoundBox& A)
{

	if(A.min_pos.x > m_boundBox.max_pos.x || A.max_pos.x < m_boundBox.min_pos.x)
		return false;
	if(A.min_pos.y > m_boundBox.max_pos.y || A.max_pos.y < m_boundBox.min_pos.y)
		return false;
	if(A.min_pos.z > m_boundBox.max_pos.z || A.max_pos.z < m_boundBox.min_pos.z)
		return false;

	return true;
}

// 边界球碰撞检测
bool	cObjectBound::isCollision(sBoundSphere A, sBoundSphere B)
{

	D3DXVECTOR3	temp =B.center - A.center;

	if(A.radius + B.radius <
		sqrtf(temp.x *temp.x +temp.y * temp.y + temp.z *temp.z))
		return false;

	return true;
}


void	cObjectBound::transBoundSphere(D3DXMATRIX* t)
{	
	D3DXVec3TransformCoord(&m_boundSphere.center, &m_boundSphere.center, t);	
}


void	cObjectBound::transBoundBox(D3DXMATRIX* t)
{

// 	float temp =m_boundBox.min_pos.y;
// 	m_boundBox.min_pos.y =m_boundBox.min_pos.z;
// 	m_boundBox.min_pos.z =temp;
// 	
// 	temp =m_boundBox.max_pos.y;
// 	m_boundBox.max_pos.y =m_boundBox.max_pos.z;
// 	m_boundBox.max_pos.z = temp;

	// 这种方式对包围框进行变换在处理平移时还没什么
	// 当处理旋转时，就不能得到正确的结果了
	D3DXVec3TransformCoord(&m_boundBox.min_pos, &m_boundBox.min_pos, t);
	D3DXVec3TransformCoord(&m_boundBox.max_pos, &m_boundBox.max_pos, t);
}

// 注意线的颜色会受物体材质的影响，
// 即物体最后材质是什么颜色，线就是什么颜色的。
void	cObjectBound::renderBox(bool WireFrame )
{
	if(WireFrame)
		m_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	else //未完成
	{//alpha blend 可是，需要一张白色纹理和材质的参数
		// 这就需要额外的东西了。
//		m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
//		m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
//		m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);		
	}

	m_pBoundBoxMesh->DrawSubset(0);

	if(WireFrame)
		m_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	else
	{
//		m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	}
}

void	cObjectBound::renderSphere()
{
	m_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	
	m_pBoundSphereMesh->DrawSubset(0);

	m_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
}

// void	bound_shpere_mesh(IDirect3DDevice9* pDevice, sBoundSphere& boundSphere, ID3DXMesh** ppBoundSphereMesh)
// {
// 	// 12段
// 	D3DXCreateSphere(pDevice, boundSphere.radius, 12, 12, ppBoundSphereMesh, NULL);	
// }
// 
// void	bound_box_mesh(IDirect3DDevice9* pDevice, sBoundBox& boundBox, ID3DXMesh** ppBoundBoxMesh)
// {
// 	D3DXCreateBox(pDevice,boundBox.max_pos.x - boundBox.min_pos.x,
// 		boundBox.max_pos.y - boundBox.min_pos.y,
// 		boundBox.max_pos.z - boundBox.min_pos.z,
// 		ppBoundBoxMesh, NULL);
// }