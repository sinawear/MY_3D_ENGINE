
#include "engine_load_x.h"


cStaticModel::cStaticModel()
{
	_isVisable =true;
	_device =NULL;
}

cStaticModel::~cStaticModel()
{
	Shutdown();
}

void	cStaticModel::SetDevice(IDirect3DDevice9*	device)
{
	_device =device;
}

void	cStaticModel::LoadXFile( char* x_file_name, char* dir)
{
	if(_device == NULL)
		MyMsgBox(L"LoadXFile()还没有设置_device.");
//////////////////////////////////////////////////////////////////////////
//调整到正确的目录
	char* pXFileName =x_file_name;
		assert(strlen(pXFileName) < 128);
	char XFileName[128];
	strcpy_s(XFileName, 128, dir);
	strcat_s(XFileName, 128, pXFileName);
//////////////////////////////////////////////////////////////////////////
	ID3DXBuffer*	pAdja_buff,*pMaterial_buff;
	
	// 得到X文件的材质指针,材质个数,网格
	int r =D3DXLoadMeshFromX(XFileName, D3DXMESH_MANAGED, _device, &pAdja_buff, 
		&pMaterial_buff, NULL, &_numMaterial, &_mesh);
	Failed_Msg_Box(NoSay, r);

	// 材质数和纹理数是匹配的.
	// 开辟材质缓冲,纹理缓冲.
	D3DXMATERIAL*	pD3DX_Material =(D3DXMATERIAL*)pMaterial_buff->GetBufferPointer();	
	_material = new	D3DMATERIAL9[_numMaterial];
	_tex = new	LPDIRECT3DTEXTURE9[_numMaterial];
	
	int hr;
	for(DWORD i=0;i<_numMaterial;i++)
	{
	//////////////////////////////////////////////////////////////////////////
	//调整到正确的目录
		char* pTexName =pD3DX_Material[i].pTextureFilename;
		assert(strlen(pTexName) < 128);
		
		char TextureFileName[128];
		strcpy_s(TextureFileName, 128, dir);
		strcat_s(TextureFileName, 128, pTexName);
	//////////////////////////////////////////////////////////////////////////
		_material[i] =pD3DX_Material[i].MatD3D;
		// 从X文件加载的材质没有AMBIENT;
		// 当我们有范围光源时,可注释掉.
	//	_material[i].Ambient = _material[i].Diffuse ;
		
		hr =D3DXCreateTextureFromFile(_device, TextureFileName, &_tex[i]);
		
		if(FAILED(hr))
		{
			_tex[i] =0;
		}
	}
	
	
	SAFE_RELEASE(pAdja_buff);
	SAFE_RELEASE(pMaterial_buff);
	
}

void	cStaticModel::Render()
{
	int r ;
	for(int i=0; i<_numMaterial; i++)
	{
		_device->SetMaterial(&_material[i]);
		_device->SetTexture(0,_tex[i]);
		r =_mesh->DrawSubset(i);
		Failed_Msg_Box(NoSay, r);
	}
}

ID3DXMesh*	cStaticModel::getMesh()
{
	return _mesh;
}

bool	cStaticModel::isVisable()
{
	return _isVisable;
}

void	cStaticModel::setVisable(bool t)
{
	_isVisable =t;
}

void	cStaticModel::Shutdown()
{
	SAFE_RELEASE(_mesh);
	
	for(int i =0; i<_numMaterial; i++)
	{
		SAFE_RELEASE(_tex[i]);
	}

	SAFE_DELETE_ARRAY(_material);
}


D3DXMATRIX*	cStaticModel::getMatrix()
{
	return &_modulate;
}

void	cStaticModel::setMatrix(D3DXMATRIX t)
{
	_modulate =t;
}


//...........................................
// 需要Light结构体里4个变量
void cStaticModel::shader_draw(D3DLIGHT9* pLight, cFx* pFx)
{
	for(int i=0; i<_numMaterial; i++)
	{
		// 设置网格的材质
		pFx->updateShaderMaterialLight(
			D3DXCOLOR(_material[i].Ambient), D3DXCOLOR(pLight->Ambient),
			D3DXCOLOR(_material[i].Diffuse), D3DXCOLOR(pLight->Diffuse),
			D3DXCOLOR(_material[i].Specular), D3DXCOLOR(pLight->Specular),
			_material[i].Power);
	
		// 设置方向光
		pFx->modify_value("g_LightDir", 
			&pLight->Direction, sizeof(D3DXVECTOR3));

		// 设置纹理
		pFx->set_texture("gTex", _tex[i]);

		pFx->commitChanges();

		// 渲染子集
		int r =_mesh->DrawSubset(i);
			Failed_Msg_Box("渲染子集",r);
	}
	
}