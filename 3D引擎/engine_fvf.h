#ifndef _ENGINE_FVF_
#define _ENGINE_FVF_


#include "engine_base.h"

//
//---------------------------------
// 宏区

// 灵活顶点格式宏
//
#define COLOR_FVF		 D3DFVF_XYZ | D3DFVF_DIFFUSE
#define LIGHT_TEXTURE_FVF		 D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1
#define TEXTURE_FVF		 D3DFVF_XYZ | D3DFVF_TEX1
#define SPRITE_FVF		 D3DFVF_XYZW	| D3DFVF_TEX1
#define RHW_TEXTURE_FVF		D3DFVF_XYZRHW | D3DFVF_TEX1
#define RHW_COLOR_FVF		D3DFVF_XYZRHW | D3DFVF_DIFFUSE
#define PARTICLE_FVF	D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_PSIZE

#define LIGHT_FVF	D3DFVF_XYZ | D3DFVF_NORMAL
#define LIGHT_TEXTURE_FVF_V1 D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1
#define TWO_TEXTURE_FVF D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEX2

#define CUBE_TEX_FVF D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE3(0)




//
//---------------------------------
// 数据结构区

struct CubeTexFvf
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 tex;
};

struct TwoTextureVertex
{
	TwoTextureVertex(){}
	TwoTextureVertex(D3DXVECTOR3 p, D3DXVECTOR2 u, D3DXVECTOR2 v)
	{
		_p =p;
		_u =u;
		_v =v;
	}

	D3DXVECTOR3 _p;
	D3DXVECTOR2 _u;
	D3DXVECTOR2 _v;
};


struct LightVertex
{
	LightVertex(){}

	LightVertex(D3DXVECTOR3 p, D3DXVECTOR3 n)
	{
		_p =p;
		_n =n;
	}	

	D3DXVECTOR3 _p;
	D3DXVECTOR3 _n;
};


// 坐标加颜色
typedef struct ColorVertex
{
	ColorVertex(){}

	ColorVertex(float x, float y, float z, D3DXCOLOR c)
	{
		pos.x =x;
		pos.y =y;
		pos.z =z;
		
		color =c;
	}

	ColorVertex(D3DXVECTOR3	_pos, D3DCOLOR _color)
	{
		pos =_pos;
		color =_color;
	}
	
	D3DXVECTOR3		pos;	
	D3DCOLOR		color;
	
}ColorVertex;


// 坐标加光照纹理
typedef struct LightTextureVertex
{
	LightTextureVertex(){}
	LightTextureVertex(float _x,float _y, float _z,float _nx,float _ny, \
		float _nz,float _u,float _v)
	{
		x=_x; y=_y; z=_z;  // vertex
		nx=_nx; ny=_ny; nz=_nz; // normal vertex
		u=_u; v=_v;  // UV;
	}

	float x,y,z;
	float nx,ny,nz;
	float u,v;

}LightTextureVertex;

// 
struct LightTextureVertexV1
{
	LightTextureVertexV1(){}
	LightTextureVertexV1(D3DXVECTOR3 p, D3DXVECTOR3 n, D3DXVECTOR2 uv)
	{
		_p =p;
		_n =n;
		_uv =uv;
	}

	D3DXVECTOR3 _p ;
	D3DXVECTOR3 _n ;
	D3DXVECTOR2 _uv;

};

// 坐标加纹理
typedef struct TextureVertex 
{
	TextureVertex(){}
	TextureVertex(float _x,float _y,float _z,float _u,float _v)
	{
		x=_x;y=_y;z=_z;
		u=_u;v=_v;
	}
	TextureVertex(D3DXVECTOR3 temp, D3DXVECTOR2 uv)
	{
		x =temp.x;
		y =temp.y;
		z =temp.z;

		u =uv.x;
		v =uv.y;
	}

	float x,y,z;
	float u,v;
}TextureVertex;

// 屏幕坐标颜色
struct	RhwColorVertex
{
	RhwColorVertex(){}
	RhwColorVertex(D3DXVECTOR4&	 vertex, D3DCOLOR	color)
	{
		_vertex =vertex;
		_color  =color;
	}

	D3DXVECTOR4	_vertex;
	D3DCOLOR	_color;
};

// 屏幕坐标加纹理
typedef	struct RhwTextureVertex 
{
	RhwTextureVertex(){}
	RhwTextureVertex(float _x,float _y,float _z,float _w, float _u,float _v)
	{
		x=_x;y=_y;z=_z;w=_w;
		u=_u;v=_v;
	}

	float x,y,z,w;
	float u,v;
}RhwTextureVertex;

// 粒子
struct ParticleVertex 
{
	ParticleVertex(){}
	ParticleVertex(D3DXVECTOR3& _pos, D3DCOLOR& _color, float _size)
	{
		position =_pos;
		color =_color;
		size =_size;
	}
	D3DXVECTOR3		position;
	D3DCOLOR		color;
	float			size;
};
//=========================================================
//顶点元素声明区




#endif	