
#include "engine_gui.h"

cGUIsystem::cGUIsystem()
{

}

cGUIsystem::cGUIsystem(IDirect3DDevice9* _device, int _width, int _heigh)
{
	m_pDevice =_device;
	m_width =_width;
	m_heigh =_heigh;

	m_ppFont =0;
	m_pControl =0;
	m_ppVb =0;
	m_useBackground =0;
	m_totalControl =0;
	m_totalBuff =0;
	m_totalFont =0;
	m_pBackVb =0;
}

cGUIsystem::~cGUIsystem()
{
	shut_down();
}

void	cGUIsystem::setGUIsystem(IDirect3DDevice9* _device, int _width, int _heigh)
{
	m_pDevice =_device;
	m_width =_width;
	m_heigh =_heigh;
}

void	cGUIsystem::shut_down()
{
	// 卸载

	if(m_useBackground)
	{
		SAFE_RELEASE(m_BackControl.back);
		SAFE_RELEASE(m_pBackVb);
	}

	// 先根据控件的类型,释放对应类型的内容
	for(int i =0; i<m_totalControl; i++)
	{
		switch(m_pControl[i].type)
		{
		case CONTROL_TEXT:
			SAFE_DELETE_ARRAY(m_pControl[i].text);
			break;

		case CONTROL_BUTTON:
			SAFE_RELEASE(m_pControl[i].up);
			SAFE_RELEASE(m_pControl[i].down);
			SAFE_RELEASE(m_pControl[i].up);
			break;
		}
	}

	//根据总的字体数,释放
	for(int k =0; k<m_totalFont; k++)
	{
		SAFE_RELEASE(m_ppFont[k]);
	}

	//根据总的缓存数,释放,
	for(int j=0 ;j<m_totalBuff; j++)
	{
		SAFE_RELEASE(m_ppVb[j]);
	}

	SAFE_DELETE(m_pControl);
}

IDirect3DVertexBuffer9*	cGUIsystem::get_vertex_buff(int listID)
{
	return	m_ppVb[listID];
}

bool	cGUIsystem::add_button_control(int ID, TCHAR* up, TCHAR* down, TCHAR* over, int x, int y)
{
	if(!up || !down || !over) return false;

	IDirect3DTexture9 *upTex, *downTex, *overTex;
	D3DXCreateTextureFromFile(m_pDevice, up, &upTex);
	D3DXCreateTextureFromFile(m_pDevice, down, &downTex);
	D3DXCreateTextureFromFile(m_pDevice, over, &overTex);

	D3DSURFACE_DESC	surfaceDesc;	
	upTex->GetLevelDesc(0, &surfaceDesc);

	int	surfaceX =surfaceDesc.Width;
	int surfaceY =surfaceDesc.Height;

	if(!m_pControl)
	{
		m_pControl =new GUIcontrol[1];
		ZeroMemory(m_pControl,sizeof(m_pControl));
	}
	else
	{
		GUIcontrol*	temp =new GUIcontrol[m_totalControl + 1];
		memcpy(temp, m_pControl, m_totalControl * sizeof(GUIcontrol));
		delete[] m_pControl;
		m_pControl =temp;	
	}

	m_pControl[m_totalControl].type =CONTROL_BUTTON;
	m_pControl[m_totalControl].ID =ID;
	m_pControl[m_totalControl].up =upTex;
	m_pControl[m_totalControl].down =downTex;
	m_pControl[m_totalControl].over =overTex;
	m_pControl[m_totalControl].m_listID =m_totalBuff;
	m_pControl[m_totalControl].x =x;
	m_pControl[m_totalControl].y =y;

	if(!m_ppVb)
	{
		m_ppVb =new IDirect3DVertexBuffer9*[1];
	}
	else
	{
		IDirect3DVertexBuffer9** temp;
		temp =new IDirect3DVertexBuffer9*[m_totalBuff + 1];
		memcpy(temp, m_ppVb, sizeof(IDirect3DVertexBuffer9*) * m_totalBuff);
		delete[] m_ppVb;
		m_ppVb =temp;
	}

	m_pDevice->CreateVertexBuffer(4 * sizeof(RhwTextureVertex), NULL, RHW_TEXTURE_FVF, D3DPOOL_MANAGED,
		&m_ppVb[m_totalBuff], NULL);

	RhwTextureVertex* p;
	m_ppVb[m_totalBuff]->Lock(0, 4 * sizeof(RhwTextureVertex), (void**)&p, NULL);

//！！！估计9600GT以下的设置为0，1都行。
// 但是以上的，必须设置1。Clear(NULL, NULL, buffers, m_clearColor, 1, 0)
// 注意我clear的Z值始终是1
// 	p[0] =RhwTextureVertex(x, y, 0, 0, 0,0);
// 	p[1] =RhwTextureVertex(x+surfaceX, y, 0, 0, 1, 0);
// 	p[2] =RhwTextureVertex(x, y+surfaceY, 0, 0, 0, 1);
// 	p[3] =RhwTextureVertex(x+surfaceX, y+surfaceY, 0, 0, 1, 1);
	p[0] =RhwTextureVertex(x, y, 0, 1, 0,0);
	p[1] =RhwTextureVertex(x+surfaceX, y, 0, 1, 1, 0);
	p[2] =RhwTextureVertex(x, y+surfaceY, 0, 1, 0, 1);
	p[3] =RhwTextureVertex(x+surfaceX, y+surfaceY, 0, 1, 1, 1);
	m_ppVb[m_totalBuff]->Unlock();

	m_pControl[m_totalControl].width =surfaceX;
	m_pControl[m_totalControl].heigh =surfaceY;


	m_totalBuff++;
	m_totalControl++;

	return true;
}

void	cGUIsystem::get_width_heigh(int& width, int& height)
{
	width =m_width;
	height =m_heigh;
}

ID3DXFont*	cGUIsystem::get_font(int listID)
{
	return m_ppFont[listID];
}

GUIcontrol*	cGUIsystem::get_control(int	listID)
{
	return	&m_pControl[listID];
}

int	cGUIsystem::get_total_buff()
{
	return m_totalBuff;
}

int	cGUIsystem::get_total_font()
{
	return m_totalFont;
}

int	cGUIsystem::get_total_control()
{
	return m_totalControl;
}

bool	cGUIsystem::create_font(TCHAR*	name, int& fontID, int width, int heigh)
{
	if(!name)	return false;

	if(!m_ppFont)
	{// 初始化
		m_ppFont =new LPD3DXFONT[1];
	}
	else
	{	
		LPD3DXFONT*	temp;
		temp =new LPD3DXFONT[m_totalFont + 1];
		memcpy(temp, m_ppFont, sizeof(LPD3DXFONT) * m_totalFont);
		delete[] m_ppFont;
		m_ppFont =temp;

	}

	// 创建字体
	D3DXFONT_DESC	font_desc;

	ZeroMemory(&font_desc,sizeof(font_desc));
	font_desc.Height =heigh;
	font_desc.Width =width;
	_tcscpy(font_desc.FaceName,name);

	D3DXCreateFontIndirect(m_pDevice, &font_desc, &m_ppFont[m_totalFont]); // indirect :间接的.

	// 当前字体索引
	fontID =m_totalFont;

	m_totalFont++;

	return true;
}

bool	cGUIsystem::add_background_control(int ID, TCHAR* texName)
{
	if(!texName) return false;

	//背景类型
	m_BackControl.type =CONTROL_BACK;

	//控件编号
	m_BackControl.ID =ID;

	//基于屏幕的顶点缓冲	
	m_pDevice->CreateVertexBuffer( 4 * sizeof(RhwTextureVertex), 0, RHW_TEXTURE_FVF, 
		D3DPOOL_DEFAULT, &m_pBackVb, 0);

	RhwTextureVertex*	p;
	m_pBackVb->Lock(0, 4 * sizeof(RhwTextureVertex), (void**)&p, NULL);
	p[0] =RhwTextureVertex(0,0,0,1,0,0);
	p[1] =RhwTextureVertex(m_width,0,0,1,1,0);
	p[2] =RhwTextureVertex(0, m_heigh, 0, 1,0, 1);
	p[3] =RhwTextureVertex(m_width, m_heigh, 0, 1,1,1);
	m_pBackVb->Unlock();

	//背景纹理
	D3DXCreateTextureFromFile(m_pDevice,texName,&m_BackControl.back);

	//使用背景纹理
	m_useBackground =true;

	return true;
}

bool	cGUIsystem::add_text_control(int ID, int fontID, TCHAR* text, int x, int y, DWORD color)
{
	if(!text) return false;

	//得到字符串长度
	int		len =_tcslen(text);

	if(!m_pControl)
	{
		m_pControl =new GUIcontrol[1];
	}
	else
	{// memcpy拷贝的是字节数哦..
		GUIcontrol* temp =new GUIcontrol[m_totalControl + 1];
		memcpy(temp, m_pControl, m_totalControl * sizeof(GUIcontrol));
		delete[] m_pControl;
		m_pControl =temp;
	}

	m_pControl[m_totalControl].type =CONTROL_TEXT;
	m_pControl[m_totalControl].ID =ID;
	m_pControl[m_totalControl].m_listID =fontID;
	m_pControl[m_totalControl].x =x;
	m_pControl[m_totalControl].y =y;
	m_pControl[m_totalControl].color =color;

	m_pControl[m_totalControl].text =new TCHAR[len + 1];

	_tcscpy(m_pControl[m_totalControl].text, text);

	m_totalControl++;

	return true;
}
GUIcontrol*	cGUIsystem::get_back_control()
{
	return	&m_BackControl;
}

IDirect3DVertexBuffer9*	cGUIsystem::get_back_control_vb()
{
	return m_pBackVb;
}

bool	cGUIsystem::isUseBackControl()
{
	return m_useBackground;
}

IDirect3DDevice9*	cGUIsystem::get_device()
{
	return	m_pDevice;
}

//-----------------------------------------
void	process_GUI(cGUIsystem*	gui,	//GUI系统指针
					bool	LButtonDown,//左键是否按下
					POINT	mousePoint,	//鼠标位置
					void (*fun)(int ID, int state)) // 外部的一条线
{
	//
	//
	//设计变量
	GUIcontrol*		tempControl =gui->get_back_control();
	IDirect3DVertexBuffer9*	tempVb =gui->get_back_control_vb();
	IDirect3DDevice9*	tempDevice =gui->get_device();
	int		numControl =gui->get_total_control();
	int		numFont		=gui->get_total_font();
	int		numBuff		=gui->get_total_buff();
	int		windowHeight, windowWidth;

	gui->get_width_heigh(windowWidth, windowHeight);
	RECT	rect ={0, 0, windowWidth, windowHeight};

	// 判断是否有背景控件
	if(gui->isUseBackControl())
	{// 渲染背景控件
		tempDevice->SetFVF(RHW_TEXTURE_FVF);
		tempDevice->SetTexture(0, tempControl->back);
		tempDevice->SetStreamSource(0, tempVb, 0, sizeof(RhwTextureVertex));
		tempDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
	}

	for(int i =0; i<numControl; i++)
	{
		int		state =BUTTON_UP;
		ID3DXFont*	temp;
		IDirect3DTexture9* currentTex;

		tempControl =gui->get_control(i);

		switch(tempControl->type)
		{
		case CONTROL_TEXT:
			temp =gui->get_font(tempControl->m_listID);

			rect.left =tempControl->x;
			rect.top =tempControl->y;

			temp->DrawText(NULL, tempControl->text, _tcslen(tempControl->text), 
				&rect, NULL, tempControl->color);
			break;

		case CONTROL_BUTTON:
			currentTex =tempControl->up;

			IDirect3DVertexBuffer9* vb =gui->get_vertex_buff(tempControl->m_listID);

			if(mousePoint.x >tempControl->x && 
				mousePoint.x <tempControl->x + tempControl->width &&
				mousePoint.y >tempControl->y && 
				mousePoint.y <tempControl->y + tempControl->heigh)
			{
				//	MessageBox(NULL,_T("!"),_T("鼠标在区域位置"),0);
				if(LButtonDown)
				{
					currentTex =tempControl->down;
					state =BUTTON_DOWN;
				}
				else
				{
					currentTex =tempControl->over;
					state =BUTTON_OVER;
				}
			}
/*
			D3DBLEND_SRCALPHA 
				Blend factor is (As, As, As, As). 

			D3DBLEND_INVSRCALPHA 
				Blend factor is ( 1 - As, 1 - As, 1 - As, 1 - As). 
*/

			tempDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
			tempDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			tempDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			tempDevice->SetFVF(RHW_TEXTURE_FVF);
			tempDevice->SetTexture(0, currentTex);
			tempDevice->SetStreamSource(0, vb, 0, sizeof(RhwTextureVertex));
			tempDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
			tempDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
			break;
		}
		// 处理各控件对应的消息
		fun(tempControl->ID, state);
	}

}

