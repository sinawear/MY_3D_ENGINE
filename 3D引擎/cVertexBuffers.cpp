

#include "cVertexBuffers.h"


// 放回类的顶点缓冲索引
DWORD cVertexBuffers::create_vertex_buff(DWORD Length, DWORD FVF, D3DPOOL Pool)
{
	// 创建顶点缓冲
	_pDevice->CreateVertexBuffer(Length, NULL, FVF, Pool, 
		&_pVertexBuff[_TotalVertexBuff], NULL);
	
	return _TotalVertexBuff;
}

void cVertexBuffers::release_Vertex_buffer_array()
{
	for(int i =0; i<_TotalVertexBuff; i++)
		SAFE_RELEASE(_pVertexBuff[i]);
}