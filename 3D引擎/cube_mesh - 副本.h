

#ifndef __cubeMeshH__
#define __cubeMeshH__

#include "engine_base.h"

/**
  标准矩形；MinPos【-1，-1，-1】MaxPos【1,1,1】。
  使用网格。
 */
class cCubeMesh
{
public:
	/**
	  24个顶点，36个索引，12个面（三角形）
	 */
	cCubeMesh(IDirect3DDevice9* device);
	~cCubeMesh();

	bool draw(D3DXMATRIX* world, D3DMATERIAL9* mtrl, IDirect3DTexture9* tex);

private:
	ID3DXMesh* _pMesh;
	IDirect3DDevice9* _pDevice;
};
#endif //__cubeMeshH__