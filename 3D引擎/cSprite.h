//  [12/24/2011 O.O]

#pragma once

#include "engine_base.h"
#include "render_struct.h"

#define MAX_SPRITE_NUM 1024

class cSprite
{
public:
	cSprite(IDirect3DDevice9* pDevice)
	{
		_pDevice =pDevice;
		_Total =0;
	}
	~cSprite()
	{
		for(int i =0; i<_Total; i++)
		{
			SAFE_RELEASE(_pSprite[i]);
		}
	}
	
	int create()
	{
		D3DXCreateSprite(_pDevice, &_pSprite[_Total]);
		
		return _Total++;
	}

	void begin(int Index, DWORD Flags =D3DXSPRITE_OBJECTSPACE | 
		D3DXSPRITE_DONOTMODIFY_RENDERSTATE | D3DXSPRITE_ALPHABLEND
		)
	{
		_pSprite[Index]->Begin(Flags);
	}

	void draw(int Index, sTexture* pTexStruct, D3DXMATRIX* pWorld, float z =0)
	{
// 		RECT Rect;
// 		Rect.left =0;
// 		Rect.top =0;
// 		Rect.right =pTexStruct->width;
// 		Rect.bottom =pTexStruct->height;

		if(pWorld != NULL)
			_pSprite[Index]->SetTransform(pWorld);

		D3DXVECTOR3 Center(pTexStruct->width/2, pTexStruct->height/2, z);

		// ��������
		_pSprite[Index]->Draw(pTexStruct->image, NULL, &Center, NULL, -1);
	}

	void end(int Index)
	{
		_pSprite[Index]->End();
	}
protected:
	IDirect3DDevice9* _pDevice;
	ID3DXSprite* _pSprite[MAX_SPRITE_NUM];
	int _Total;
};