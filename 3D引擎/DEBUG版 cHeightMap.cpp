#include "cHeightMap.h"





//////////////////////////////////////////////////////////////////////////


cHeightMap::cHeightMap(int NumRow, int NumCol, string FileName, 
		   float HeightScal, float HeightOffset)
{
	m_bIsLoad =false;
	
	load_row(NumRow, NumCol, FileName, HeightScal, HeightOffset);
	
	m_bIsLoad =true;
}

void cHeightMap::load_row(int NumRow, int NumCol, const string FileName, 
		float HeightScal, float HeightOffset)
	{
		if(m_bIsLoad == true)
			MyMsgBox(L" 高度图已经被加载。", true);

		FILE* pFile;
		vector<UCHAR> FileMemory(NumRow * NumCol);
		pFile =fopen(FileName.c_str(), "rb");
		int ReadByte =fread((void*)&FileMemory[0], sizeof(UCHAR), NumRow*NumCol, pFile);
		fclose(pFile);
/*
		// 高度图文件和内存
		ifstream FileRead;
		vector<UCHAR> FileMemory(NumRow * NumCol);
		
// 打开文件读取到内存
		FileRead.open(FileName.c_str(), ios::binary);
			if(FileRead.is_open())
			{
				FileRead>>(UCHAR*)&FileMemory[0];
			}
// 关闭文件
		FileRead.close();
	*/
		
		// 调整高度图大小
		m_HeightMap.resize(NumRow, NumCol);

		for(int i =0; i< NumRow; i++) //行数
		{
			for(int j =0; j< m_HeightMap.getVertexesPerRow(); j++) //每行多少顶点
			{
				float t =FileMemory[i*m_HeightMap.getVertexesPerRow()+j] * HeightScal + HeightOffset;
				m_HeightMap(i, j) = t;
//				Log.appendLog("二进制打开高度图数据.txt", "%f\n", t);
			}
		}
/*
		cSystemLog Log;
		for(int i =0; i<257; i++)
		{
			for(int j =0; j<257; j++)
			{
				float t =m_HeightMap(i, j);
				Log.appendLog("未经优化高度图中数据.txt", "%f\n", t);
			}
		}
*/
		// 平滑地图的高度值
		sampleHeight3X3();

/*
		cSystemLog Log;
		for(int i =0; i<257; i++)
		{
			for(int j =0; j<257; j++)
			{
				float t =m_HeightMap(i, j);
				Log.appendLog("优化高度图中数据.txt", "%f\n", t);
			}
		}
*/

				
}