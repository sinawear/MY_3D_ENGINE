//测试通过

#ifndef __cubeMeshH__
#define __cubeMeshH__

#include "engine_base.h"

/**
 有了UV坐标。
  使用网格。
 */
class cCubeMesh
{
public:
	/**
	  24个顶点，36个索引，12个面（三角形）
	 */
	~cCubeMesh();

	ID3DXMesh* create(IDirect3DDevice9* device, float width =1.0f, float height =1.0f, float depth =1.0f);

	bool draw(D3DXMATRIX* world, D3DMATERIAL9* mtrl, IDirect3DTexture9* tex);

private:
	ID3DXMesh* _pMesh;
	IDirect3DDevice9* _pDevice;
};
#endif //__cubeMeshH__