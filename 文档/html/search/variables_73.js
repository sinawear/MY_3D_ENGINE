var searchData=
[
  ['s',['s',['../structs_material.html#a555186212db2e6708fd525826e1f819a',1,'sMaterial']]],
  ['s_5fsubterrain',['s_SubTerrain',['../c_ground_8cpp.html#a7807417b4f32ae8bb5afb517354448a5',1,'cGround.cpp']]],
  ['size',['size',['../struct_particle_vertex.html#a38162d12885686dad08db2971bae018b',1,'ParticleVertex']]],
  ['specular_5fcolor',['specular_color',['../structs_light.html#a37b911f710f2e99cc2eeee2d8eb12920',1,'sLight']]],
  ['sprite_5ffile_5fname',['sprite_file_name',['../struct_sprite.html#af742f9d3ab8550dcae0b2b4a0fe3505a',1,'Sprite']]],
  ['stride',['stride',['../structs_d3_d_static_buffer.html#a4577e8158778a39fddaf2b56116d034e',1,'sD3DStaticBuffer']]],
  ['submesh_5fnumcol',['SUBMESH_NUMCOL',['../c_gournd_v1_8cpp.html#a6b5d654ae08a10c420e1ff69fc549bab',1,'SUBMESH_NUMCOL():&#160;cGourndV1.cpp'],['../c_ground_8cpp.html#a6b5d654ae08a10c420e1ff69fc549bab',1,'SUBMESH_NUMCOL():&#160;cGround.cpp']]],
  ['submesh_5fnumrow',['SUBMESH_NUMROW',['../c_gournd_v1_8cpp.html#a5350cfe6123ecf14775ccd640e8479d3',1,'SUBMESH_NUMROW():&#160;cGourndV1.cpp'],['../c_ground_8cpp.html#a5350cfe6123ecf14775ccd640e8479d3',1,'SUBMESH_NUMROW():&#160;cGround.cpp']]]
];
